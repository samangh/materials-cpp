/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "multifit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	MultiFit::MultiFit(){

	}

	// factory
	ShMultiFitPr MultiFit::create(){
		return std::make_shared<MultiFit>();
	}

	// copy constructor
	cmn::ShNodePr MultiFit::copy() const{
		// create new multi-fit object
		ShMultiFitPr multi_fit = MultiFit::create();
		
		// copy fits
		for(auto it=fit_list_.begin();it!=fit_list_.end();it++)
			multi_fit->add_fit(std::get<0>(*it), std::get<1>(*it), std::get<2>(*it)->copy_covariant<Fit>());

		// return
		return multi_fit;
	}

	// add fit
	void MultiFit::add_fit(const double t1, const double t2, ShFitPr fit){
		fit_list_.push_back(std::make_tuple(t1,t2,fit));
	}

	// fit functions
	double MultiFit::calc_property(const double temperature) const{
		// allocate output value
		double value = 0;

		// walk over list of fits
		for(auto it=fit_list_.begin();it!=fit_list_.end();it++){
			// get temperature range
			const double t1 = std::get<0>(*it); const double t2 = std::get<1>(*it);

			// check if temperature is in range
			if(temperature<t2 && temperature>=t1){
				value = std::get<2>(*it)->calc_property(temperature); break;
			}
		}

		// return calculated value
		return value;
	}
	
	// specific heat calculation
	arma::Col<double> MultiFit::calc_property(const arma::Col<double> &temperature) const{
		// allocate output values
		arma::Col<double> values(temperature.n_elem,arma::fill::zeros);

		// walk over list of fits
		for(auto it=fit_list_.begin();it!=fit_list_.end();it++){
			// get temperature range
			const double t1 = std::get<0>(*it); const double t2 = std::get<1>(*it);

			// find indexes within this range
			const arma::Col<arma::uword> idx = arma::find(temperature>=t1 && temperature<t2);

			// calculate using the corresponding fit
			values(idx) = std::get<2>(*it)->calc_property(temperature(idx));
		}

		// return calculated values
		return values;
	}

	// get type
	std::string MultiFit::get_type(){
		return "rat::mat::multifit";
	}

	// method for serialization into json
	void MultiFit::serialize(Json::Value &js, cmn::SList &list) const{
		js["type"] = get_type();
		
		for(auto it=fit_list_.begin();it!=fit_list_.end();it++){
			// create fit object
			Json::Value jsfit;

			// serialize the temperature range in which the fit is used
			jsfit["t1"] = std::get<0>(*it);
			jsfit["t2"] = std::get<1>(*it);

			// serialize the fit itself
			jsfit["fit"] = cmn::Node::serialize_node(std::get<2>(*it), list);

			// add the fit to the list
			js["fit_list"].append(jsfit);
		}
	}

	// method for deserialisation from json
	void MultiFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// deserialize fits
		for(auto it=js["fit_list"].begin(); it!=js["fit_list"].end(); ++it){
			const double t1 = (*it)["t1"].asDouble();
			const double t2 = (*it)["t2"].asDouble();
			ShFitPr fit = cmn::Node::deserialize_node<Fit>((*it)["fit"], list, factory_list, pth);
			add_fit(t1,t2,fit);
		}
	}

}}