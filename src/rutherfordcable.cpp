/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "rutherfordcable.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	RutherfordCable::RutherfordCable(){

	}

	// factory
	ShRutherfordCablePr RutherfordCable::create(){
		return std::make_shared<RutherfordCable>();
	}

	// copy factory
	cmn::ShNodePr RutherfordCable::copy() const{
		ShRutherfordCablePr new_wire = RutherfordCable::create();
		new_wire->width_ = width_;
		new_wire->thickness_ = thickness_;
		new_wire->keystone_ = keystone_;
		new_wire->pitch_ = pitch_;
		new_wire->dinsu_ = dinsu_;
		new_wire->num_strands_ = num_strands_;
		new_wire->fcabling_ = fcabling_;
		new_wire->tol_bisection_ = tol_bisection_;
		new_wire->strand_ = strand_->copy_covariant<LTSWire>();
		if(voids_!=NULL)new_wire->voids_ = voids_->copy_covariant<Conductor>();
		if(insu_!=NULL)new_wire->insu_ = insu_->copy_covariant<Conductor>();
		new_wire->setup();
		return new_wire;
	}

	// set number of strands
	void RutherfordCable::set_num_strands(const arma::uword num_strands){
		if(num_strands<=0)rat_throw_line("number of strands must be positive");
		num_strands_ = num_strands;
	}

	// set dimensions
	void RutherfordCable::set_width(const double width){
		if(width<0)rat_throw_line("width must be larger than zero");
		width_ = width;
	}

	// set dimensions
	void RutherfordCable::set_thickness(const double thickness){
		if(thickness<0)rat_throw_line("thickness must be larger than zero");
		thickness_ = thickness;
	}

	// set keystone angle in [rad]
	void RutherfordCable::set_keystone(const double keystone){
		keystone_ = keystone;
	}

	// set pitch angle in [rad]
	void RutherfordCable::set_pitch(const double pitch){
		if(pitch<0)rat_throw_line("pitch must be larger than zero");
		pitch_ = pitch;
	}

	// set insulation thickness
	void RutherfordCable::set_dinsu(const double dinsu){
		if(dinsu<0)rat_throw_line("dinsu must be larger than zero");
		dinsu_ = dinsu;
	}

	// set degradation factor due to cabling
	void RutherfordCable::set_fcabling(const double fcabling){
		if(fcabling<0)rat_throw_line("fcabling must be larger than zero");
		if(fcabling>1.0)rat_throw_line("fcabling must be less than one");
		fcabling_ = fcabling;
	}

	// get superconductor
	ShLTSWirePr RutherfordCable::get_strand() const{
		return strand_;
	}

	// set superconductor
	void RutherfordCable::set_strand(ShLTSWirePr strand){
		strand_ = strand;
	}

	// set insulation material
	void RutherfordCable::set_insu(ShConductorPr insu){
		insu_ = insu;
	}

	// set void material
	void RutherfordCable::set_voids(ShConductorPr voids){
		voids_ = voids;
	}

	// setup function
	void RutherfordCable::setup(){
		// calculate bare and total area
		const double bare_area = thickness_*width_;
		const double total_area = (thickness_+2*dinsu_)*(width_+2*dinsu_);
		
		// get strand diameter from strand
		const double strand_radius = strand_->get_diameter()/2;

		// calculate filling fraction
		const double strand_area = num_strands_*arma::datum::pi*strand_radius*strand_radius;

		// check sizes
		if(strand_area>bare_area)rat_throw_line("strands do not fit in the bare cable");

		// calculate filling of strands in cable
		const double strand_fraction = strand_area/total_area;
		const double void_fraction = (bare_area - strand_area)/total_area;
		const double insu_fraction = (total_area - bare_area)/total_area;

		// add conductors by fraction
		add_conductor(strand_fraction, strand_);
		if(void_fraction>0 && voids_!=NULL)add_conductor(void_fraction, voids_);
		if(insu_fraction>0 && insu_!=NULL)add_conductor(insu_fraction, insu_);
	}

	// critical current density [J m^-2]
	double RutherfordCable::calc_critical_current_density(
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{

		// calculate scaling
		double fpitch = 1.0; 
		if(pitch_!=0){
			const double pitch_angle = std::atan(2*(width_ + thickness_)/pitch_);
			fpitch = std::cos(pitch_angle);
		}

		// forward calculation and scale
		return fcabling_*fpitch*ParallelConductor::calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle);
	}

	// critical current density [J m^-2]
	arma::Col<double> RutherfordCable::calc_critical_current_density(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle) const{

		// calculate scaling
		double fpitch = 1.0; 
		if(pitch_!=0){
			const double pitch_angle = std::atan(2*(width_ + thickness_)/pitch_);
			fpitch = std::cos(pitch_angle);
		}

		// forward calculation and scale
		return fcabling_*fpitch*ParallelConductor::calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle);
	}


	// serialization type
	std::string RutherfordCable::get_type(){
		return "rat::mat::rutherfordcable";
	}

	// serialization
	void RutherfordCable::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// serialization type
		js["type"] = get_type();

		// name
		rat::cmn::Nameable::serialize(js,list);

		// dimensions
		js["num_strands"] = (int)num_strands_;
		js["width"] = width_;
		js["thickness"] = thickness_;
		js["keystone"] = keystone_;
		js["pitch"] = pitch_;
		js["dinsu"] = dinsu_;
		js["fcabling"] = fcabling_;

		// layers
		js["strand"] = cmn::Node::serialize_node(strand_, list);
		js["insu"] = cmn::Node::serialize_node(insu_, list);
		js["voids"] = cmn::Node::serialize_node(voids_, list);

		// bisection tolerance
		js["tol_bisection"] = tol_bisection_;
	}

	// deserialization
	void RutherfordCable::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// name
		rat::cmn::Nameable::deserialize(js,list,factory_list, pth);

		// dimensions
		set_width(js["width"].asDouble());
		set_thickness(js["thickness"].asDouble());
		set_keystone(js["keystone"].asDouble());
		set_pitch(js["pitch"].asDouble());
		set_dinsu(js["dinsu"].asDouble());
		set_num_strands(js["num_strands"].asUInt64());
		set_fcabling(js["fcabling"].asDouble());

		// layers
		set_strand(cmn::Node::deserialize_node<LTSWire>(js["strand"], list, factory_list, pth));
		set_insu(cmn::Node::deserialize_node<Conductor>(js["insu"], list, factory_list, pth));
		set_voids(cmn::Node::deserialize_node<Conductor>(js["voids"], list, factory_list, pth));

		// bisection tolerance
		if(js["tol_bisection"].isNumeric())
			set_tol_bisection(js["tol_bisection"].asDouble());

		// setup as parallel conductor
		setup();
	}

}}