/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "baseconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	BaseConductor::BaseConductor(){

	}

	// constructor with input
	BaseConductor::BaseConductor(
		ShFitPr cp_fit, 
		ShFitPr k_fit, 
		ShFitPr rho_fit, 
		ShFitPr d_fit){
		
		// use set functions to set fits
		set_thermal_conductivity_fit(k_fit);
		set_electrical_resistivity_fit(rho_fit);
		set_specific_heat_fit(cp_fit);
		set_density_fit(d_fit);
	}

	// constructor with input
	BaseConductor::BaseConductor(
		ShFitPr jc_fit, 
		ShFitPr nv_fit, 
		ShFitPr cp_fit, 
		ShFitPr k_fit, 
		ShFitPr rho_fit, 
		ShFitPr d_fit){
		
		// use set functions to set fits
		set_critical_current_density_fit(jc_fit);
		set_nvalue_fit(nv_fit);
		set_thermal_conductivity_fit(k_fit);
		set_electrical_resistivity_fit(rho_fit);
		set_specific_heat_fit(cp_fit);
		set_density_fit(d_fit);
	}

	// factory
	ShBaseConductorPr BaseConductor::create(){
		return std::make_shared<BaseConductor>();
	}

	// normal conductor factory
	ShBaseConductorPr BaseConductor::create(
		ShFitPr cp_fit, ShFitPr k_fit, 
		ShFitPr rho_fit, ShFitPr d_fit){
		return std::make_shared<BaseConductor>(
			cp_fit,k_fit,rho_fit,d_fit);
	}

	// supercondutcor factory
	ShBaseConductorPr BaseConductor::create(
		ShFitPr jc_fit, ShFitPr nv_fit, 
		ShFitPr cp_fit, ShFitPr k_fit, 
		ShFitPr rho_fit, ShFitPr d_fit){
		return std::make_shared<BaseConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
	}

	// copy constructor
	cmn::ShNodePr BaseConductor::copy() const{
		// create base conductor
		ShBaseConductorPr new_base = BaseConductor::create();
		
		// copy if not NULL
		if(k_fit_!=NULL)new_base->set_thermal_conductivity_fit(k_fit_->copy_covariant<Fit>());
		if(rho_fit_!=NULL)new_base->set_electrical_resistivity_fit(rho_fit_->copy_covariant<Fit>());
		if(cp_fit_!=NULL)new_base->set_specific_heat_fit(cp_fit_->copy_covariant<Fit>());
		if(d_fit_!=NULL)new_base->set_density_fit(d_fit_->copy_covariant<Fit>());
		if(jc_fit_!=NULL)new_base->set_critical_current_density_fit(jc_fit_->copy_covariant<Fit>());
		if(nv_fit_!=NULL)new_base->set_nvalue_fit(nv_fit_->copy_covariant<Fit>());
			
		// copy electric field criterion
		new_base->electric_field_criterion_ = electric_field_criterion_;

		// return copy of base
		return new_base;
	}

	// supercondutcor factory
	ShBaseConductorPr BaseConductor::copper(const double RRR){
		// create fits
		ShFitPr cp_fit = Nist8SumFit::create(4,300,arma::Col<double>::fixed<9>{
			-1.91844,-0.15973,8.61013,-18.996,21.9661,-12.7328,3.54322,-0.3797,0.0});
		ShFitPr rho_fit = CudiFit::create(RRR);
		ShFitPr k_fit = WFLFit::create(rho_fit);
		ShFitPr d_fit = ConstFit::create(8960);

		// create conductor and return
		ShBaseConductorPr copper = std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);
		copper->set_name("Copper OFHC RRR" + std::to_string(RRR));
		return copper;
	}

	// set electric field criterion
	void BaseConductor::set_electric_field_criterion(
		const double electric_field_criterion){
		electric_field_criterion_ = electric_field_criterion;
	}


	// set thermal conductivity fit
	void BaseConductor::set_thermal_conductivity_fit(ShFitPr k_fit){
		k_fit_ = k_fit;
	}

	// set electrical resistivity fit
	void BaseConductor::set_electrical_resistivity_fit(ShFitPr rho_fit){
		rho_fit_ = rho_fit;
	}

	// set specific heat fit
	void BaseConductor::set_specific_heat_fit(ShFitPr cp_fit){
		cp_fit_ = cp_fit;
	}

	// set density fit
	void BaseConductor::set_density_fit(ShFitPr d_fit){
		d_fit_ = d_fit;
	}

	// set critical current density fit
	void BaseConductor::set_critical_current_density_fit(ShFitPr jc_fit){
		jc_fit_ = jc_fit;
	}

	// set fit for determining the n-value
	void BaseConductor::set_nvalue_fit(ShFitPr nv_fit){
		nv_fit_ = nv_fit;
	}


	// material properties for scalar input
	// calculate specific heat [J kg^-1 K^-1]
	double BaseConductor::calc_specific_heat(
		const double temperature) const{
		if(cp_fit_!=NULL)return cp_fit_->calc_property(temperature); 
		else return 0;
	}

	// thermal conductivity [W m^-1 K^-1]
	double BaseConductor::calc_thermal_conductivity(
		const double temperature,
		const double magnetic_field_magnitude) const{
		if(k_fit_!=NULL)return k_fit_->calc_property(
			temperature, magnetic_field_magnitude);
		else return 0;
	}

	// electrical resistivity [Ohm m^2]
	double BaseConductor::calc_electrical_resistivity(
		const double temperature,
		const double magnetic_field_magnitude) const{
		if(rho_fit_!=NULL)return rho_fit_->calc_property(
			temperature,magnetic_field_magnitude);
		else return 1e99;
	}

	// material density [kg m^-3]
	double BaseConductor::calc_density(
		const double temperature) const{
		if(d_fit_!=NULL)return d_fit_->calc_property(temperature); 
		else return 0;
	}

	// critical current density [J m^-2]
	double BaseConductor::calc_critical_current_density(
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{
		if(jc_fit_!=NULL)return jc_fit_->calc_property(
			temperature, magnetic_field_magnitude, magnetic_field_angle);
		else return 0;
	}

	// calculate power law nvalue
	double BaseConductor::calc_nvalue(
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{
		if(nv_fit_!=NULL)return nv_fit_->calc_property(
			temperature, magnetic_field_magnitude, magnetic_field_angle);
		else return 0;
	}



	// electric field [V/m]
	double BaseConductor::calc_electric_field(
		const double current_density,
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{

		// calculate electrical resistivity 
		const double rho_scalar  = calc_electrical_resistivity(
			temperature, magnetic_field_magnitude);

		// calculate critical current 
		const double jc_scalar = calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle);

		// calculate nvalue 
		const double nvalue_scalar = calc_nvalue(
			temperature, magnetic_field_magnitude, magnetic_field_angle);

		// convert to vector
		const arma::Row<double> rho_vec = {rho_scalar};
		const arma::Row<double> jc_vec = {jc_scalar};
		const arma::Row<double> nvalue_vec = {nvalue_scalar};

		// calculate using regular method
		return calc_electric_field_fast(
			current_density, rho_vec, jc_vec, nvalue_vec);
	}

	// electric field using pre-calculated electrical 
	// resistivity en critical current density [V/m]
	double BaseConductor::calc_electric_field_fast(
		const double current_density,
		const arma::Row<double> &electrical_resistivity_vec,
		const arma::Row<double> &critical_current_density_vec,
		const arma::Row<double> &nvalue_vec) const{

		// check input sizes
		assert(electrical_resistivity_vec.n_elem==1);
		assert(critical_current_density_vec.n_elem==1);
		assert(nvalue_vec.n_elem==1);

		// convert to scalars
		const double electrical_resistivity = arma::as_scalar(electrical_resistivity_vec);
		const double critical_current_density = arma::as_scalar(critical_current_density_vec);
		const double nvalue = arma::as_scalar(nvalue_vec);

		// get electric field criterion used for fit
		// const double electric_field_criterion = jc_fit_->get_electric_field_criterion();

		// electric field based on resistivity
		const double electric_field_rho = electrical_resistivity*std::abs(current_density);

		// allocate 
		double electric_field_jc;

		// superconducting
		if(critical_current_density>0){
			assert(electric_field_criterion_>0);
			electric_field_jc = electric_field_criterion_*std::pow(
				std::abs(current_density)/critical_current_density,nvalue); 
		}

		// normal conducting
		else{
			electric_field_jc = arma::datum::inf;
		}

		// check sign
		assert(electric_field_jc>=0);
		assert(electric_field_rho>=0);

		// return smallest of the two
		return rat::cmn::Extra::sign(current_density)*
			std::min(electric_field_rho, electric_field_jc);
	}

	// calculate current density [A m^-2]
	double BaseConductor::calc_current_density(
		const double electric_field,
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{

		// calculate electrical resistivity 
		const double rho_scalar  = calc_electrical_resistivity(
			temperature, magnetic_field_magnitude);

		// calculate critical current 
		const double jc_scalar = calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle);

		// calculate nvalue 
		const double nvalue_scalar = calc_nvalue(
			temperature, magnetic_field_magnitude, magnetic_field_angle);

		// convert to vector
		const arma::Row<double> rho_vec = {rho_scalar};
		const arma::Row<double> jc_vec = {jc_scalar};
		const arma::Row<double> nvalue_vec = {nvalue_scalar};

		// calculate using regular method
		return calc_current_density_fast(
			electric_field, rho_vec, jc_vec, nvalue_vec);
	}

	// calculate current density [A m^-2]
	double BaseConductor::calc_current_density_fast(
		const double electric_field,
		const arma::Row<double> &electrical_resistivity_vec,
		const arma::Row<double> &critical_current_density_vec,
		const arma::Row<double> &nvalue_vec) const{

		// check input sizes
		assert(electrical_resistivity_vec.n_elem==1);
		assert(critical_current_density_vec.n_elem==1);
		assert(nvalue_vec.n_elem==1);

		// convert to scalars
		const double electrical_resistivity = arma::as_scalar(electrical_resistivity_vec);
		const double critical_current_density = arma::as_scalar(critical_current_density_vec);
		const double nvalue = arma::as_scalar(nvalue_vec);

		// get electric field criterion used for fit
		// const double electric_field_criterion = jc_fit_->get_electric_field_criterion();

		// allocate 
		double current_density_sc = 0;

		// superconducting
		if(critical_current_density>0){
			assert(electric_field_criterion_>0);
			current_density_sc = critical_current_density*
				std::pow(std::abs(electric_field)/electric_field_criterion_,1.0/nvalue);
		}

		// normal conducting current 
		const double current_density_nc = std::abs(electric_field)/electrical_resistivity;

		// check sign
		assert(current_density_sc>=0);
		assert(current_density_nc>=0);

		// take the highest of teh two
		return rat::cmn::Extra::sign(electric_field)*
			std::max(current_density_nc, current_density_sc);
	}


	// material properties for vector input
	// calculate specific heat [J kg^-1 K^-1]
	arma::Col<double> BaseConductor::calc_specific_heat(
		const arma::Col<double> &temperature) const{
		if(cp_fit_!=NULL)return cp_fit_->calc_property(temperature);
		else return arma::Col<double>(temperature.n_elem, arma::fill::zeros);
	}

	// thermal conductivity [W m^-1 K^-1]
	arma::Col<double> BaseConductor::calc_thermal_conductivity(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude) const{
		if(k_fit_!=NULL)return k_fit_->calc_property(temperature, magnetic_field_magnitude);
		else return arma::Col<double>(temperature.n_elem, arma::fill::zeros);
	}

	// electrical resistivity [Ohm m^2]
	arma::Col<double> BaseConductor::calc_electrical_resistivity(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude) const{
		if(rho_fit_!=NULL)return rho_fit_->calc_property(temperature,magnetic_field_magnitude);
		else return arma::Col<double>(temperature.n_elem, arma::fill::ones)*1e99;
	}

	// material density [kg m^-3]
	arma::Col<double> BaseConductor::calc_density(
		const arma::Col<double> &temperature) const{
		if(d_fit_!=NULL)return d_fit_->calc_property(temperature);
		else return arma::Col<double>(temperature.n_elem, arma::fill::zeros);
	}

	// critical current density [J m^-2]
	arma::Col<double> BaseConductor::calc_critical_current_density(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle) const{
		if(jc_fit_!=NULL)return jc_fit_->calc_property(
			temperature, magnetic_field_magnitude, magnetic_field_angle);
		else return arma::Col<double>(temperature.n_elem, arma::fill::zeros);
	}

	// critical current density [J m^-2]
	arma::Col<double> BaseConductor::calc_nvalue(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle) const{
		if(nv_fit_!=NULL)return nv_fit_->calc_property(
			temperature, magnetic_field_magnitude, magnetic_field_angle);
		else return arma::Col<double>(temperature.n_elem, arma::fill::zeros);
	}

	// electric field calculation [V/m]
	arma::Col<double> BaseConductor::calc_electric_field(
		const arma::Col<double> &current_density,
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle) const{
		
		// calculate electrical resistivity 
		const arma::Col<double> rho  = calc_electrical_resistivity(
			temperature, magnetic_field_magnitude);

		// calculate critical current 
		const arma::Col<double> jc = calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle);

		// calculate nvalue
		const arma::Col<double> nvalue = calc_nvalue(
			temperature, magnetic_field_magnitude, magnetic_field_angle);

		// calculate using regular method
		return calc_electric_field_fast(current_density, rho, jc, nvalue);
	}


	// electric field [V m^-1] section averaged
	arma::Col<double> BaseConductor::calc_electric_field_av(
		const arma::Col<double> &current_density,
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle,
		const arma::Mat<arma::uword> &nelements,
		const arma::Row<double> &cross_areas) const{

		// calculate electrical resistivity at nodes
		const arma::Col<double> rho  = calc_electrical_resistivity(
			temperature, magnetic_field_magnitude);

		// calculate critical current at nodes
		const arma::Col<double> jc = calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle);

		// calculate nvalue at nodes
		const arma::Col<double> nvalue = calc_nvalue(
			temperature, magnetic_field_magnitude, magnetic_field_angle);

		// calculate cross section averaged values
		const arma::Col<double> rho_section = 
			Conductor::nodes2section(rho, nelements, cross_areas);
		const arma::Col<double> jc_section = 
			Conductor::nodes2section(jc, nelements, cross_areas);
		const arma::Col<double> nvalue_section = 
			Conductor::nodes2section(nvalue, nelements, cross_areas);
		const arma::Col<double> j_section = 
			Conductor::nodes2section(current_density, nelements, cross_areas);

		// calculate electric field for each section
		const arma::Col<double> electric_field_section = 
			calc_electric_field_fast(j_section, rho_section, jc_section, nvalue_section);

		// back to nodes
		return Conductor::section2nodes(electric_field_section, nelements, cross_areas);
	}


	// electric field using pre-calculated electrical 
	// resistivity en critical current density [V/m]
	arma::Col<double> BaseConductor::calc_electric_field_fast(
		const arma::Col<double> &current_density,
		const arma::Mat<double> &electrical_resistivity,
		const arma::Mat<double> &critical_current_density, 
		const arma::Mat<double> &nvalue) const{

		// check if single column
		assert(electrical_resistivity.n_cols==1);
		assert(critical_current_density.n_cols==1);
		assert(nvalue.n_cols==1);

		// check sizes
		assert(electrical_resistivity.n_rows==current_density.n_elem);
		assert(critical_current_density.n_rows==current_density.n_elem);
		assert(nvalue.n_rows==current_density.n_elem);

		// get electric field criterion used for fit
		// const double electric_field_criterion = jc_fit_->get_electric_field_criterion();

		// electric field based on resistivity
		arma::Col<double> electric_field_nc = 
			electrical_resistivity%arma::abs(current_density);

		// electirc field due to critical current density
		// this is the classical power law
		arma::Col<double> electric_field_sc(current_density.n_elem);
		for(arma::uword i=0;i<current_density.n_elem;i++){
			// superconducting
			if(critical_current_density(i)>0){
				assert(electric_field_criterion_>0);
				electric_field_sc(i) = electric_field_criterion_*
					std::pow(std::abs(current_density(i))/
					critical_current_density(i),nvalue(i));
			}

			// normal conducting
			else{
				electric_field_sc(i) = arma::datum::inf;
			}
		}

		// check sign
		assert(arma::all(electric_field_nc>=0));
		assert(arma::all(electric_field_sc>=0));

		// return lowest of the two
		return arma::sign(current_density)%
			arma::min(electric_field_nc, electric_field_sc);
	}

	// calculate current density [A m^-2]
	arma::Col<double> BaseConductor::calc_current_density(
		const arma::Col<double> &electric_field,
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle) const{

		// calculate electrical resistivity 
		const arma::Col<double> rho  = calc_electrical_resistivity(
			temperature, magnetic_field_magnitude);

		// calculate critical current 
		const arma::Col<double> jc = calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle);

		// calculate nvalue
		const arma::Col<double> nvalue = calc_nvalue(
			temperature, magnetic_field_magnitude, magnetic_field_angle);

		// calculate using regular method
		return calc_current_density_fast(electric_field, rho, jc, nvalue);
	}

	// current density from electric field 
	// output in [A m^-2]
	arma::Col<double> BaseConductor::calc_current_density_fast(
		const arma::Col<double> &electric_field,
		const arma::Mat<double> &electrical_resistivity,
		const arma::Mat<double> &critical_current_density, 
		const arma::Mat<double> &nvalue) const{

		// check if single column
		assert(electrical_resistivity.n_cols==1);
		assert(critical_current_density.n_cols==1);
		assert(nvalue.n_cols==1);

		// get electric field criterion used for fit
		// const double electric_field_criterion = jc_fit_->get_electric_field_criterion();

		// allocate 
		arma::Col<double> current_density_sc(electric_field.n_elem, arma::fill::zeros);

		// superconducting current
		for(arma::uword i=0;i<electric_field.n_elem;i++){
			// superconducting
			if(critical_current_density(i)>0){
				assert(electric_field_criterion_>0);
				current_density_sc(i) = critical_current_density(i)*
					std::pow(std::abs(electric_field(i))/electric_field_criterion_,1.0/nvalue(i));
			}
		}

		// normal conducting current
		const arma::Col<double> current_density_nc = 
			arma::abs(electric_field)/electrical_resistivity;

		// check sign
		assert(arma::all(current_density_nc>=0));
		assert(arma::all(current_density_sc>=0));

		// take the highest of teh two and return
		const arma::Col<double> current_density = 
			arma::sign(electric_field)%arma::max(
			current_density_nc, current_density_sc);
		return current_density;
	}



	// serialization type
	std::string BaseConductor::get_type(){
		return "rat::mat::baseconductor";
	}

	// serialization
	void BaseConductor::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parent
		cmn::Nameable::serialize(js,list);

		// serialize fits
		js["type"] = get_type();
		js["k_fit"] = cmn::Node::serialize_node(k_fit_, list);
		js["rho_fit"] = cmn::Node::serialize_node(rho_fit_, list);
		js["cp_fit"] = cmn::Node::serialize_node(cp_fit_, list);
		js["d_fit"] = cmn::Node::serialize_node(d_fit_, list);
		js["jc_fit"] = cmn::Node::serialize_node(jc_fit_, list);
		js["nv_fit"] = cmn::Node::serialize_node(nv_fit_, list);

		// electric field criterion
		if(electric_field_criterion_!=0)
			js["E0"] = electric_field_criterion_;
	}

	// deserialization
	void BaseConductor::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// parent
		cmn::Nameable::deserialize(js,list,factory_list,pth);

		// deserialize fits
		set_thermal_conductivity_fit(cmn::Node::deserialize_node<Fit>(js["k_fit"], list, factory_list, pth));
		set_electrical_resistivity_fit(cmn::Node::deserialize_node<Fit>(js["rho_fit"], list, factory_list, pth));
		set_specific_heat_fit(cmn::Node::deserialize_node<Fit>(js["cp_fit"], list, factory_list, pth));
		set_density_fit(cmn::Node::deserialize_node<Fit>(js["d_fit"], list, factory_list, pth));
		set_critical_current_density_fit(cmn::Node::deserialize_node<Fit>(js["jc_fit"], list, factory_list, pth));
		set_nvalue_fit(cmn::Node::deserialize_node<Fit>(js["nv_fit"], list, factory_list, pth));

		// electric field criterion
		if(js["E0"].isNumeric()){
			electric_field_criterion_ = js["E0"].asDouble();
			assert(electric_field_criterion_>0);
		}

	}

}}