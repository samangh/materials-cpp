/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "dualfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	DualFit::DualFit(){

	}

	// factory
	ShDualFitPr DualFit::create(){
		return std::make_shared<DualFit>();
	}

	// copy constructor
	cmn::ShNodePr DualFit::copy() const{
		// create new multi-fit object
		ShDualFitPr dual_fit = DualFit::create();
		
		// copy fits
		for(auto it=fit_list_.begin();it!=fit_list_.end();it++)
			dual_fit->add_fit(std::get<0>(*it), std::get<1>(*it), std::get<2>(*it)->copy_covariant<Fit>());

		// copy temperature
		dual_fit->t12_ = t12_;

		// return
		return dual_fit;
	}

	// acces fit 1
	ShFitPr DualFit::get_fit1() const{
		return (std::get<2>(*(fit_list_.begin())));
	}

	// acces fit 2
	ShFitPr DualFit::get_fit2() const{
		return (std::get<2>(*(++fit_list_.begin())));
	}

	// get type
	std::string DualFit::get_type(){
		return "rat::mat::dualfit";
	}

	// method for serialization into json
	void DualFit::serialize(Json::Value &js, cmn::SList &list) const{
		js["type"] = get_type();
		js["fit1"] = cmn::Node::serialize_node(get_fit1(),list);
		js["fit2"] = cmn::Node::serialize_node(get_fit2(),list);
		js["t12"] = t12_;
	}

	// method for deserialisation from json
	void DualFit::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		t12_ = js["t12"].asDouble();
		add_fit(-1e99, t12_, cmn::Node::deserialize_node<Fit>(js["fit1"], list, factory_list, pth));
		add_fit(t12_, 1e99, cmn::Node::deserialize_node<Fit>(js["fit2"], list, factory_list, pth));
	}

}}