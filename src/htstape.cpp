/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "htstape.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	HTSTape::HTSTape(){

	}

	// constructor
	HTSTape::HTSTape(const double width, const double thickness, ShHTSCorePr core, ShConductorPr stab){
		width_ = width; thickness_ = thickness; core_ = core; stab_ = stab; setup();
	}

	// copy function
	cmn::ShNodePr HTSTape::copy() const{
		ShHTSTapePr new_tape = HTSTape::create();
		new_tape->width_ = width_;
		new_tape->thickness_ = thickness_;
		new_tape->core_ = core_->copy_covariant<HTSCore>();
		new_tape->stab_ = stab_->copy_covariant<Conductor>();
		new_tape->tol_bisection_ = tol_bisection_;
		new_tape->setup();
		return new_tape;
	}

	// factory
	ShHTSTapePr HTSTape::create(){
		return std::make_shared<HTSTape>();
	}

	// factory with input
	ShHTSTapePr HTSTape::create(const double width, const double thickness, ShHTSCorePr core, ShConductorPr stab){
		return std::make_shared<HTSTape>(width, thickness, core, stab);
	}

	// set width
	void HTSTape::set_width(const double width){
		if(width<=0)rat_throw_line("width must be positive");
		width_ = width;
	}

	// set thickness
	void HTSTape::set_thickness(const double thickness){
		if(thickness<=0)rat_throw_line("width must be positive");
		thickness_ = thickness;
	}

	// get width
	double HTSTape::get_width() const{
		return width_;
	}

	// get thickness
	double HTSTape::get_thickness() const{
		return thickness_;
	}

	// get core thickness
	double HTSTape::get_area() const{
		return width_*thickness_;
	}

	// get superconductor
	ShHTSCorePr HTSTape::get_core() const{
		return core_;
	}

	// get substrate
	ShConductorPr HTSTape::get_stab() const{
		return stab_;
	}

	// set superconductor
	void HTSTape::set_core(ShHTSCorePr core){
		assert(core!=NULL);
		core_ = core;
	}

	// set superconductor
	void HTSTape::set_stab(ShConductorPr stab){
		assert(stab!=NULL);
		stab_ = stab;
	}

	// setup function
	void HTSTape::setup(){
		// get core dimensions
		const double core_width = core_->get_width();
		const double core_thickness = core_->get_thickness();
		const double core_area = core_width*core_thickness;

		// get tape area
		const double tape_area = width_*thickness_;

		// stab area
		const double stab_area = tape_area-core_area;

		// check
		if(core_area>tape_area)
			rat_throw_line("core does not fit in tape dimensions");

		// add conductor by fraction
		add_conductor(core_area/tape_area, core_);
		add_conductor(stab_area/tape_area, stab_);
	}

	// serialization type
	std::string HTSTape::get_type(){
		return "rat::mat::htstape";
	}

	// serialization
	void HTSTape::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// serialization type
		js["type"] = get_type();

		// name
		rat::cmn::Nameable::serialize(js,list);

		// dimensions
		js["width"] = width_;
		js["thickness"] = thickness_;

		// layers
		js["core"] = cmn::Node::serialize_node(core_, list);
		js["stab"] = cmn::Node::serialize_node(stab_, list);

		// bisection tolerance
		js["tol_bisection"] = tol_bisection_;
	}

	// deserialization
	void HTSTape::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// name
		rat::cmn::Nameable::deserialize(js,list,factory_list,pth);

		// dimensions
		width_ = js["width"].asDouble();
		thickness_ = js["thickness"].asDouble();

		// layers
		set_core(cmn::Node::deserialize_node<HTSCore>(js["core"], list, factory_list, pth));
		set_stab(cmn::Node::deserialize_node<Conductor>(js["stab"], list, factory_list, pth));

		// bisection tolerance
		if(js["tol_bisection"].isNumeric())
			set_tol_bisection(js["tol_bisection"].asDouble());

		// setup as parallel conductor
		setup();
	}

}}