/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "cudifit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	CudiFit::CudiFit(){

	}

	// default copper resistivity fit based on RRR
	CudiFit::CudiFit(const double RRR){
		C0_ = 1.7; C1_ = 2.33e9; C2_ = 9.57e5; C3_ = 163.0; RRR_ = RRR; 
		fit_temperature_range_(0) = 4.0; 
		fit_temperature_range_(1) = 300.0;
	}

	// factory
	ShCudiFitPr CudiFit::create(){
		return std::make_shared<CudiFit>();
	}

	// factory
	ShCudiFitPr CudiFit::create(const double RRR){
		return std::make_shared<CudiFit>(RRR);
	}

	// copy constructor
	cmn::ShNodePr CudiFit::copy() const{
		return std::make_shared<CudiFit>(*this);
	}

	// vector fit function
	arma::Col<double> CudiFit::calc_property(
		const arma::Col<double> &temperature, 
		const arma::Col<double> &magnetic_field_magnitude) const{

		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);

		// clamp temperature as to avoid negative values in fit
		const arma::Col<double> T = arma::clamp(temperature,fit_temperature_range_(0),arma::datum::inf);
		const arma::Col<double> B = magnetic_field_magnitude;

		// Equation [Ohm.m]
		const arma::Col<double> TTT = T%T%T; 
		const arma::Col<double> TTTTT = TTT%T%T;
		arma::Col<double> rho = (C0_/RRR_ + 1.0/(C1_/TTTTT + C2_/TTT + C3_/T))*1e-8 + B*(0.37 + 0.0005*RRR_)*1e-10;

		// return calculated conductivity
		return rho; // [Ohm m]
	}

	// scalar fit function
	double CudiFit::calc_property(
		const double temperature, 
		const double magnetic_field_magnitude) const{

		// clamp temperature as to avoid negative values in fit
		const double T = std::max(temperature,fit_temperature_range_(0));
		const double B = magnetic_field_magnitude;

		// Equation [Ohm.m]
		const double TTT = T*T*T; 
		const double TTTTT = TTT*T*T;
		const double value = (C0_/RRR_ + 1.0/(C1_/TTTTT + C2_/TTT + C3_/T))*1e-8 + B*(0.37 + 0.0005*RRR_)*1e-10;

		// return calculated conductivity
		return value; // [Ohm m]
	}

	// get type
	std::string CudiFit::get_type(){
		return "rat::mat::cudifit";
	}

	// method for serialization into json
	void CudiFit::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();

		// fit parameters
		js["C0"] = C0_;
		js["C1"] = C1_;
		js["C2"] = C2_;
		js["C3"] = C3_;
		js["RRR"] = RRR_;
		js["t1"] = fit_temperature_range_(0);
		js["t2"] = fit_temperature_range_(1);
	}

	// method for deserialisation from json
	void CudiFit::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		// fit parameters
		C0_ = js["C0"].asDouble();
		C1_ = js["C1"].asDouble();
		C2_ = js["C2"].asDouble();
		C3_ = js["C3"].asDouble();
		RRR_ = js["RRR"].asDouble();
		fit_temperature_range_(0) = js["t1"].asDouble();
		fit_temperature_range_(1) = js["t2"].asDouble();
	}

}}