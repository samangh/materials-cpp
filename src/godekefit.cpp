/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "godekefit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	GodekeFit::GodekeFit(){

	}

	// factory
	ShGodekeFitPr GodekeFit::create(){
		return std::make_shared<GodekeFit>();
	}

	// copy constructor
	cmn::ShNodePr GodekeFit::copy() const{
		return std::make_shared<GodekeFit>(*this);
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	double GodekeFit::calc_property(
		const double temperature,
		const double magnetic_field_magnitude) const{

		// prevent zero field at which a anomalous behaviour occurs
		const double B = std::max(magnetic_field_magnitude,0.1);
		const double T = std::max(temperature,1.0);

		// strain dependence
		const double eps_shift = Ca2_*eps_0a_/std::sqrt(Ca1_*Ca1_ - Ca2_*Ca2_);
		const double S = (Ca1_*(std::sqrt(eps_shift*eps_shift + eps_0a_*eps_0a_) - 
			std::sqrt(std::pow(eps_ax_ - eps_shift,2) + eps_0a_*eps_0a_)) - 
			Ca2_*eps_ax_)/(1.0 - Ca1_*eps_0a_) + 1.0;

		// scaled values
		const double Tc = Tcm_*std::pow(S,(1.0/3));
		const double t = T/Tc;
		const double Bc2 = Bc2m_*S*(1.0-std::pow(t,1.52));
		const double h = B/Bc2;

		// Godeke Equation
		double critical_current = 
			C1_*S*(1.0-std::pow(t,1.52))*
			(1.0-std::pow(t,2))*std::pow(h,p_)*
			std::pow(1.0-h,q_)/B;

		// prevent formula from rising again after reaching Bc
		if(B>Bc2 || t>1.0)critical_current = 0;
		if(B==0)critical_current = arma::datum::inf;

		// return critical current
		return 1e6*critical_current;
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<double> GodekeFit::calc_property(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude) const{

		// prevent zero field at which a anomalous behaviour occurs
		const arma::Col<double> B = arma::clamp(magnetic_field_magnitude,0.01,arma::datum::inf);
		const arma::Col<double> T = arma::clamp(temperature,1.0,arma::datum::inf);

		// strain dependence
		const double eps_shift = Ca2_*eps_0a_/std::sqrt(Ca1_*Ca1_ - Ca2_*Ca2_);
		const double S = (Ca1_*(std::sqrt(eps_shift*eps_shift + eps_0a_*eps_0a_) - 
			std::sqrt(std::pow(eps_ax_ - eps_shift,2) + eps_0a_*eps_0a_)) - 
			Ca2_*eps_ax_)/(1.0 - Ca1_*eps_0a_) + 1.0;

		// scaled values
		const double Tc = Tcm_*std::pow(S,(1.0/3));
		const arma::Col<double> t = T/Tc;
		const arma::Col<double> Bc2 = Bc2m_*S*(1.0 - arma::pow(t,1.52));
		const arma::Col<double> h = B/Bc2;

		// Godeke Equation
		arma::Col<double> critical_current = 
			C1_*S*(1.0-arma::pow(t,1.52))%
			(1.0-arma::pow(t,2))%arma::pow(h,p_)%
			arma::pow(1.0-h,q_)/B;

		// prevent formula from rising again after reaching Bc
		critical_current(arma::find(B>Bc2)).fill(0);
		critical_current(arma::find(B==0)).fill(arma::datum::inf);

		// return critical current
		return 1e6*critical_current;
	}


	

	// get type
	std::string GodekeFit::get_type(){
		return "rat::mat::godekefit";
	}

	// method for serialization into json
	void GodekeFit::serialize(Json::Value &js, cmn::SList &) const{
		// serialization type
		js["type"] = get_type();
		
		// fit parameters
		js["Ca1"] = Ca1_;
		js["Ca2"] = Ca2_;
		js["eps_0a"] = eps_0a_;
		js["C1"] = C1_;

		// superconducting parameters
		js["Bc2m"] = Bc2m_;
		js["Tcm"] = Tcm_;

		// magnetic field dependence parameters
		js["p"] = p_;
		js["q"] = q_;

		// environmental parameters
		js["eps_ax"] = eps_ax_; // axial strain
	}

	// method for deserialisation from json
	void GodekeFit::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){

		// fit parameters
		Ca1_ = js["Ca1"].asDouble();
		Ca2_ = js["Ca2"].asDouble();
		eps_0a_ = js["eps_0a"].asDouble(); // normalized
		C1_ = js["C1"].asDouble(); // [kAT mm^-2]

		// superconducting parameters
		Bc2m_ = js["Bc2m"].asDouble(); // [T]
		Tcm_ = js["Tcm"].asDouble(); // [K]

		// magnetic field dependence parameters
		p_ = js["p"].asDouble();
		q_ = js["q"].asDouble();

		// environmental parameters
		eps_ax_ = js["eps_ax"].asDouble();
	}

}}