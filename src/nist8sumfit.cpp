/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "nist8sumfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	Nist8SumFit::Nist8SumFit(){

	}

	// default constructor
	Nist8SumFit::Nist8SumFit(const double t1, const double t2, const arma::Col<double>::fixed<9> &fit_parameters){
		set_fit_parameters(fit_parameters); set_temperature_range(t1,t2);
	}

	// factory
	ShNist8SumFitPr Nist8SumFit::create(){
		return std::make_shared<Nist8SumFit>();
	}

	// factory
	ShNist8SumFitPr Nist8SumFit::create(const double t1, const double t2, const arma::Col<double>::fixed<9> &fit_parameters){
		return std::make_shared<Nist8SumFit>(t1,t2,fit_parameters);
	}

	// create by copy
	cmn::ShNodePr Nist8SumFit::copy() const{
		return std::make_shared<Nist8SumFit>(*this);
	}


	// set fit parameters
	void Nist8SumFit::set_fit_parameters(const arma::Col<double>::fixed<9> &fit_parameters){
		if(fit_parameters.is_empty())rat_throw_line("parameter list is empty");
		fit_parameters_ = fit_parameters;
	}

	// set fit parameters
	void Nist8SumFit::set_temperature_range(const double t1, const double t2){
		if(t2<t1)rat_throw_line("upper temperature must be larger than lower temperature");
		fit_temperature_range_(0) = t1; fit_temperature_range_(1) = t2;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Col<double> Nist8SumFit::calc_property(const arma::Col<double> &temperature)const{

		// check input
		assert(temperature.is_finite());

		// limit lower temperature
		const arma::Col<double> T = arma::clamp(temperature,fit_temperature_range_(0),arma::datum::inf);

		// check if fit parameters set
		if(fit_parameters_.is_empty())rat_throw_line("parameter list is not set");

		// calculate nist fit using for loop to avoid power
		arma::Col<double> A(T.n_elem,arma::fill::zeros);
		arma::Col<double> log10T = arma::log10(T);
		arma::Col<double> plt(T); plt.fill(1.0);
		for(arma::uword i=0;i<fit_parameters_.n_elem;i++){
			A += fit_parameters_(i)*plt; plt%=log10T;
		}

		// 10^A and multiply with density
		arma::Col<double> values = arma::exp10(A);

		// extrapolation beyond end
		const arma::Col<arma::uword> extrap = arma::find(T>fit_temperature_range_(1));
		if(!extrap.is_empty()){
			const double dt = (fit_temperature_range_(1)-fit_temperature_range_(0))/1000;
			const arma::Col<double>::fixed<2> Te = {fit_temperature_range_(1)-dt, fit_temperature_range_(1)-1e-10};
			const arma::Col<double>::fixed<2> ve = calc_property(Te);
			values(extrap) = ve(1) + (T(extrap)-Te(1))*((ve(1)-ve(0))/(Te(1)-Te(0)));
		}

		// check output
		assert(values.n_elem==T.n_elem);
		assert(values.is_finite());
		
		// check with scalar version
		#ifndef NDEBUG
		for(arma::uword i=0;i<T.n_elem;i++)
			assert(std::abs(values(i) - calc_property(T(i)))/values(i)<1e-6);
		#endif

		// return answer
		return values;
	}

	// specific heat output in [J m^-3 K^-1]
	double Nist8SumFit::calc_property(const double temperature)const{
		// check input
		if(fit_parameters_.is_empty())rat_throw_line("parameter list is not set");

		// allocate
		double value = 0;

		// extrapolation beyond end
		if(temperature>fit_temperature_range_(1)){
			const double dt = (fit_temperature_range_(1)-fit_temperature_range_(0))/1000;
			const double t1 = fit_temperature_range_(1)-dt;
			const double t2 = fit_temperature_range_(1);
			const double s1 = calc_property(t1);
			const double s2 = calc_property(t2);
			value = s2 + (temperature - fit_temperature_range_(1))*((s2-s1)/(t2-t1));
		}

		// fix value when below range
		else if(temperature<fit_temperature_range_(0)){
			value = calc_property(fit_temperature_range_(0));
		}

		// calculate normally
		else{
			double A = 0, plt = 1.0;
			double log10T = std::log10(temperature);
			for(arma::uword i=0;i<fit_parameters_.n_elem;i++){
				A += fit_parameters_(i)*plt; plt*=log10T;
			}

			// 10^A and multiply with density
			value = std::pow(10.0,A);
		}

		// return answer
		return value;
	}


	// get type
	std::string Nist8SumFit::get_type(){
		return "rat::mat::nist8sumfit";
	}

	// method for serialization into json
	void Nist8SumFit::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();

		// fit parameters
		js["a"] = fit_parameters_(0);
		js["b"] = fit_parameters_(1);
		js["c"] = fit_parameters_(2);
		js["d"] = fit_parameters_(3);
		js["e"] = fit_parameters_(4);
		js["f"] = fit_parameters_(5);
		js["g"] = fit_parameters_(6);
		js["h"] = fit_parameters_(7);
		js["i"] = fit_parameters_(8);

		// temperature range
		js["t1"] = fit_temperature_range_(0);
		js["t2"] = fit_temperature_range_(1);
	}

	// method for deserialisation from json
	void Nist8SumFit::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		// fit parameters
		fit_parameters_(0) = js["a"].asDouble();
		fit_parameters_(1) = js["b"].asDouble();
		fit_parameters_(2) = js["c"].asDouble();
		fit_parameters_(3) = js["d"].asDouble();
		fit_parameters_(4) = js["e"].asDouble();
		fit_parameters_(5) = js["f"].asDouble();
		fit_parameters_(6) = js["g"].asDouble();
		fit_parameters_(7) = js["h"].asDouble();
		fit_parameters_(8) = js["i"].asDouble();

		// temperature range
		fit_temperature_range_(0) = js["t1"].asDouble();
		fit_temperature_range_(1) = js["t2"].asDouble();
	}

}}