/* Raccoon - An Electro-Magnetic and Thermal network 
solver accelerated with the Multi-Level Fast Multipole Method
Copyright (C) 2020  Jeroen van Nugteren*/

// include header
#include "serializer.hh"

// serializable objects
#include "conductor.hh"
#include "baseconductor.hh"
#include "parallelconductor.hh"

// wrappers
#include "htscore.hh"
#include "htstape.hh"
#include "ltswire.hh"

// fits
#include "nist8sumfit.hh"
#include "nist8fracfit.hh"
#include "polyfit.hh"
#include "lubellkramerfit.hh"
#include "constfit.hh"
#include "multifit.hh"
#include "dualfit.hh"
#include "godekefit.hh"
#include "fleiterfit.hh"
#include "cudifit.hh"
#include "wflfit.hh"
#include "rutherfordcable.hh"

// code specific to Raccoon
namespace rat{namespace mat{
	
	// constructor
	Serializer::Serializer(){
		register_constructors();
	}

	// constructor
	Serializer::Serializer(
		const boost::filesystem::path &fname){
		register_constructors();
		import_json(fname);
	}

	// factory
	ShSerializerPr Serializer::create(){
		return std::make_shared<Serializer>();
	}

	// factory
	ShSerializerPr Serializer::create(
		const boost::filesystem::path &fname){
		return std::make_shared<Serializer>(fname);
	}

	// registry
	void Serializer::register_constructors(){
		// conductors
		register_factory<BaseConductor>();
		register_factory<ParallelConductor>();

		// HTS core and tape wrappers
		register_factory<HTSCore>();
		register_factory<HTSTape>();

		// LTS wire wrappers
		register_factory<LTSWire>();

		// Fits
		register_factory<Nist8SumFit>();
		register_factory<Nist8FracFit>();
		register_factory<PolyFit>();
		register_factory<LubellKramerFit>();
		register_factory<ConstFit>();
		register_factory<MultiFit>();
		register_factory<DualFit>();
		register_factory<GodekeFit>();
		register_factory<FleiterFit>();
		register_factory<CudiFit>();
		register_factory<WFLFit>();
		register_factory<RutherfordCable>();
	}

}}