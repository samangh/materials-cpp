/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "constfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	ConstFit::ConstFit(){

	}

	// default constructor
	ConstFit::ConstFit(const double value){
		set_value(value);
	}

	// factory
	ShConstFitPr ConstFit::create(){
		return std::make_shared<ConstFit>();
	}

	// factory
	ShConstFitPr ConstFit::create(const double value){
		return std::make_shared<ConstFit>(value);
	}

	// copy constructor
	cmn::ShNodePr ConstFit::copy() const{
		return std::make_shared<ConstFit>(*this);
	}

	// set fit parameters
	void ConstFit::set_value(const double value){
		value_ = value;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Col<double> ConstFit::calc_property(const arma::Col<double> &temperature)const{
		return arma::Col<double>(temperature.n_elem,arma::fill::ones)*value_;
	}

	// specific heat output in [J m^-3 K^-1]
	double ConstFit::calc_property(const double /*temperature*/)const{
		return value_;
	}

	// get type
	std::string ConstFit::get_type(){
		return "rat::mat::constfit";
	}

	// method for serialization into json
	void ConstFit::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();

		// fit parameters
		js["value"] = value_;
	}

	// method for deserialisation from json
	void ConstFit::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/, const boost::filesystem::path &/*pth*/){
		value_ = js["value"].asDouble();
	}

}}