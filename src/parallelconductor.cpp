/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "parallelconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	ParallelConductor::ParallelConductor(){

	}

	// default constructor
	ParallelConductor::ParallelConductor(
		const double fraction, ShConductorPr conductor){
		add_conductor(fraction, conductor);
	}

	// default constructor
	ParallelConductor::ParallelConductor(
		const double fraction1, ShConductorPr conductor1,
		const double fraction2, ShConductorPr conductor2){
		add_conductor(fraction1, conductor1);
		add_conductor(fraction2, conductor2);
	}

	// factory
	ShParallelConductorPr ParallelConductor::create(){
		return std::make_shared<ParallelConductor>();
	}

	// factory
	ShParallelConductorPr ParallelConductor::create(
		const double fraction, ShConductorPr conductor){
		return std::make_shared<ParallelConductor>(fraction, conductor);
	}

	// factory
	ShParallelConductorPr ParallelConductor::create(
		const double fraction1, ShConductorPr conductor1,
		const double fraction2, ShConductorPr conductor2){
		return std::make_shared<ParallelConductor>(fraction1, conductor1, fraction2, conductor2);
	}

	// copy factory
	cmn::ShNodePr ParallelConductor::copy() const{
		// create new conductor
		ShParallelConductorPr mycopy = ParallelConductor::create();

		// walk over conductors and copy one by one
		arma::uword i=0;
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++,i++){
			// get conductor
			const double fraction = (*it).first; assert(fraction>0);
			const ShConductorPr conductor = (*it).second;
			mycopy->add_conductor(fraction, conductor->copy_covariant<Conductor>());
		}
		
		// copy settings
		mycopy->tol_bisection_ = tol_bisection_;

		// return copy
		return mycopy;
	}

	// set bisection tolerance
	void ParallelConductor::set_tol_bisection(const double tol_bisection){
		if(tol_bisection<=0)rat_throw_line("tolerance must be positive");
		tol_bisection_ = tol_bisection;
	}

	// add material to list
	void ParallelConductor::add_conductor(const double fraction, ShConductorPr conductor){
		conductor_list_.push_back(FracShConPr(fraction,conductor));
	}

	// accumulate individual conductors in a list
	// if a series conductor is encountered it is added as whole
	void ParallelConductor::accu_parallel_conductors(
		FracConPrList &parallel_conductors) const{

		// this conductor is parallel so we take our children
		for(auto it = conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// allocate a new list
			FracConPrList parallel_conductors_child;

			// add conductors to the list
			conductor->accu_parallel_conductors(parallel_conductors_child);

			// multiply factions and add conductor to the general list
			for(auto it2 = parallel_conductors_child.begin();
				it2!=parallel_conductors_child.end();it2++)(*it2).first *= fraction;

			// combine lists
			parallel_conductors.insert(
				parallel_conductors.end(), 
				parallel_conductors_child.begin(), 
				parallel_conductors_child.end());
		}
	}

	// material properties for scalar input
	// calculate specific heat [J kg^-1 K^-1]
	double ParallelConductor::calc_specific_heat(
		const double temperature) const{
		
		// allocate
		double specific_heat = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional specific heat
			specific_heat += fraction*conductor->calc_specific_heat(temperature);
		}

		// return values
		return specific_heat;
	}

	// thermal conductivity [W m^-1 K^-1]
	double ParallelConductor::calc_thermal_conductivity(
		const double temperature,
		const double magnetic_field_magnitude) const{
		
		// allocate
		double thermal_conductivity = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional thermal conductivities
			thermal_conductivity += fraction*conductor->calc_thermal_conductivity(
				temperature, magnetic_field_magnitude);
		}

		// return values
		return thermal_conductivity;
	}

	// electrical resistivity [Ohm m^2]
	double ParallelConductor::calc_electrical_resistivity(
		const double temperature,
		const double magnetic_field_magnitude) const{
		
		// allocate
		double electrical_conductivity = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional electrical conductivities
			electrical_conductivity += fraction/conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude);
		}

		// take inverse to get resistivity and return
		return 1.0/electrical_conductivity;
	}

	// material density [kg m^-3]
	double ParallelConductor::calc_density(
		const double temperature) const{

		// allocate
		double density = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional densities
			density += fraction*conductor->calc_density(temperature);
		}

		// return values
		return density;
	}

	// critical current density [J m^-2]
	double ParallelConductor::calc_critical_current_density(
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{
		
		// allocate
		double critical_current_density = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional critical current densities
			critical_current_density += fraction*conductor->calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle);
		}

		// return values
		return critical_current_density;
	}

	// calculate power law nvalue
	double ParallelConductor::calc_nvalue(
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{
		
		// allocate
		double nvalue = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const ShConductorPr &conductor = (*it).second;

			// take highest n-value (no fraction)
			nvalue = std::max(nvalue, conductor->calc_nvalue(
				temperature, magnetic_field_magnitude, magnetic_field_angle));
		}

		// return values
		return nvalue;
	}


	// electric field [V/m]
	double ParallelConductor::calc_electric_field(
		const double current_density,
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{
		
		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors);

		// get number of conductors
		const arma::uword num_conductors = parallel_conductors.size();

		// allocate
		arma::Row<double> rho_vec(num_conductors);
		arma::Row<double> jc_vec(num_conductors);
		arma::Row<double> nvalue_vec(num_conductors);

		// walk over conductors
		arma::uword i = 0;
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
			// get conductor
			const Conductor* conductor = (*it).second;

			// calculate electrical resistivity 
			rho_vec(i)  = conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude);

			// calculate critical current 
			jc_vec(i) = conductor->calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle);

			// calculate nvalue 
			nvalue_vec(i) = conductor->calc_nvalue(
				temperature, magnetic_field_magnitude, magnetic_field_angle);
		}

		// calculate using regular method
		return calc_electric_field_fast(current_density, rho_vec, jc_vec, nvalue_vec);
	}

	// electric field using pre-calculated electrical 
	// resistivity en critical current density [V/m]
	// this uses bisection method
	double ParallelConductor::calc_electric_field_fast(
		const double current_density,
		const arma::Row<double> &electrical_resistivity,
		const arma::Row<double> &critical_current_density,
		const arma::Row<double> &nvalue) const{

		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors);

		// estimate lower bound
		double lower_electric_field = 0; 
		double higher_electric_field = arma::datum::inf;

		// walk over conductors
		arma::uword i = 0;
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
			// get conductor
			const double fraction = (*it).first;
			const Conductor* conductor = (*it).second;

			// we assume all current runs through this material
			const double con_current_density = std::abs(current_density)/fraction;
			
			// calculate electric field
			const double con_electric_field = conductor->calc_electric_field_fast(
				con_current_density, electrical_resistivity.col(i), critical_current_density.col(i), nvalue.col(i));
			
			// find lowest electric field value
			higher_electric_field = std::min(higher_electric_field, con_electric_field);
		}

		// allocate midpoint output electric field
		double mid_electric_field;;

		// start bisection algorithm
		// find an electric field where all the 
		// supplied current density is soaked by the materials
		for(;;){
			// update mid point
			mid_electric_field = (lower_electric_field + higher_electric_field)/2;

			// calculate intermeditae value
			const double vmid = calc_current_density_fast(
				mid_electric_field, electrical_resistivity,
				critical_current_density, nvalue) - 
				std::abs(current_density);

			// update mid-point
			if(vmid<0)lower_electric_field = mid_electric_field;
			else higher_electric_field = mid_electric_field;
			
			// check convergence
			if((higher_electric_field-lower_electric_field)<tol_bisection_)break;
		}

		// return signed electric field
		return rat::cmn::Extra::sign(current_density)*mid_electric_field;
	}

	// electric field [V/m]
	double ParallelConductor::calc_current_density(
		const double electric_field,
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{
		
		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors);

		// get number of conductors
		const arma::uword num_conductors = parallel_conductors.size();

		// allocate
		arma::Row<double> rho_vec(num_conductors);
		arma::Row<double> jc_vec(num_conductors);
		arma::Row<double> nvalue_vec(num_conductors);

		// walk over conductors
		arma::uword i = 0;
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
			// get conductor
			const Conductor* conductor = (*it).second;

			// calculate electrical resistivity 
			rho_vec(i)  = conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude);

			// calculate critical current 
			jc_vec(i) = conductor->calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle);

			// calculate nvalue 
			nvalue_vec(i) = conductor->calc_nvalue(
				temperature, magnetic_field_magnitude, magnetic_field_angle);
		}

		// calculate using regular method
		return calc_current_density_fast(electric_field, rho_vec, jc_vec, nvalue_vec);
	}

	// calculate current density [A m^-2]
	double ParallelConductor::calc_current_density_fast(
		const double electric_field,
		const arma::Row<double> &electrical_resistivity,
		const arma::Row<double> &critical_current_density,
		const arma::Row<double> &nvalue) const{

		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors);

		// allocate
		double current_density = 0;

		// walk over conductors
		arma::uword i=0;
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
			// get conductor
			const double fraction = (*it).first;
			const Conductor* conductor = (*it).second;

			// accumulate current density
			current_density += fraction*conductor->calc_current_density_fast(
				electric_field, electrical_resistivity.col(i), 
				critical_current_density.col(i), nvalue.col(i));
		}

		// return values
		return current_density;
	}


	// material properties for vector input
	// calculate specific heat [J kg^-1 K^-1]
	arma::Col<double> ParallelConductor::calc_specific_heat(
		const arma::Col<double> &temperature) const{
		
		// allocate
		arma::Col<double> specific_heat(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional specific heat
			specific_heat += fraction*conductor->calc_specific_heat(temperature);
		}

		// return values
		return specific_heat;
	}

	// thermal conductivity [W m^-1 K^-1]
	arma::Col<double> ParallelConductor::calc_thermal_conductivity(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude) const{
		
		// allocate
		arma::Col<double> thermal_conductivity(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional thermal conductivities
			thermal_conductivity += fraction*conductor->calc_thermal_conductivity(
				temperature, magnetic_field_magnitude);
		}

		// return values
		return thermal_conductivity;
	}

	// electrical resistivity [Ohm m^2]
	arma::Col<double> ParallelConductor::calc_electrical_resistivity(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude) const{
		
		// allocate
		arma::Col<double> electrical_conductivity(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional electrical conductivities
			electrical_conductivity += fraction/conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude);
		}

		// take inverse to get resistivity and return
		return 1.0/electrical_conductivity;
	}

	// material density [kg m^-3]
	arma::Col<double> ParallelConductor::calc_density(
		const arma::Col<double> &temperature) const{
		
		// allocate
		arma::Col<double> density(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional densities
			density += fraction*conductor->calc_density(temperature);
		}

		// return values
		return density;
	}

	// critical current density [J m^-2]
	arma::Col<double> ParallelConductor::calc_critical_current_density(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle) const{
		
		// allocate
		arma::Col<double> critical_current_density(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional critical current densities
			critical_current_density += fraction*conductor->calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle);
		}

		// return values
		return critical_current_density;
	}

	// critical current density [J m^-2]
	arma::Col<double> ParallelConductor::calc_nvalue(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle) const{
		
		// allocate
		arma::Col<double> nvalue(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const ShConductorPr &conductor = (*it).second;

			// take highest n-value (no fraction)
			nvalue = arma::max(nvalue, conductor->calc_nvalue(
				temperature, magnetic_field_magnitude, magnetic_field_angle));
		}

		// return values
		return nvalue;
	}


	// electric field calculation [V/m]
	arma::Col<double> ParallelConductor::calc_electric_field(
		const arma::Col<double> &current_density,
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle) const{

		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors);

		// get number of conductors
		const arma::uword num_conductors = parallel_conductors.size();

		// allocate
		arma::Mat<double> rho_mat(temperature.n_elem,num_conductors);
		arma::Mat<double> jc_mat(temperature.n_elem,num_conductors);
		arma::Mat<double> nvalue_mat(temperature.n_elem,num_conductors);

		// walk over conductors
		arma::uword i = 0;
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
			// get conductor
			const Conductor* conductor = (*it).second;

			// calculate electrical resistivity 
			rho_mat.col(i)  = conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude);

			// calculate critical current 
			jc_mat.col(i) = conductor->calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle);

			// calculate nvalue 
			nvalue_mat.col(i) = conductor->calc_nvalue(
				temperature, magnetic_field_magnitude, magnetic_field_angle);
		}

		// calculate using regular method
		return calc_electric_field_fast(current_density, rho_mat, jc_mat, nvalue_mat);
	}

	// electric field [V m^-1] section averaged
	arma::Col<double> ParallelConductor::calc_electric_field_av(
		const arma::Col<double> &current_density,
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle,
		const arma::Mat<arma::uword> &nelements,
		const arma::Row<double> &cross_areas) const{

		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors);

		// get number of conductors
		const arma::uword num_conductors = parallel_conductors.size();

		// allocate
		arma::Mat<double> rho_mat(temperature.n_elem,num_conductors);
		arma::Mat<double> jc_mat(temperature.n_elem,num_conductors);
		arma::Mat<double> nvalue_mat(temperature.n_elem,num_conductors);

		// walk over conductors
		arma::uword i = 0;
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
			// get conductor
			const Conductor* conductor = (*it).second;

			// calculate electrical resistivity 
			rho_mat.col(i)  = conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude);

			// calculate critical current 
			jc_mat.col(i) = conductor->calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle);

			// calculate nvalue 
			nvalue_mat.col(i) = conductor->calc_nvalue(
				temperature, magnetic_field_magnitude, magnetic_field_angle);
		}

		// calculate cross section averaged values
		const arma::Mat<double> rho_section = 
			Conductor::nodes2section(rho_mat, nelements, cross_areas);
		const arma::Mat<double> jc_section = 
			Conductor::nodes2section(jc_mat, nelements, cross_areas);
		const arma::Mat<double> nvalue_section = 
			Conductor::nodes2section(nvalue_mat, nelements, cross_areas);
		const arma::Col<double> j_section = 
			Conductor::nodes2section(current_density, nelements, cross_areas);

		// calculate electric field for each section
		const arma::Col<double> electric_field_section = 
			calc_electric_field_fast(j_section, rho_section, jc_section, nvalue_section);

		// back to nodes
		return Conductor::section2nodes(
			electric_field_section, nelements, cross_areas);
	}

	// electric field using pre-calculated electrical 
	// resistivity en critical current density [V/m]
	arma::Col<double> ParallelConductor::calc_electric_field_fast(
		const arma::Col<double> &current_density,
		const arma::Mat<double> &electrical_resistivity,
		const arma::Mat<double> &critical_current_density, 
		const arma::Mat<double> &nvalue) const{

		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors);

		// check sizes
		assert(electrical_resistivity.n_cols==parallel_conductors.size());
		assert(critical_current_density.n_cols==parallel_conductors.size());
		assert(nvalue.n_cols==parallel_conductors.size());
		assert(current_density.n_rows==electrical_resistivity.n_rows);
		assert(current_density.n_rows==critical_current_density.n_rows);
		assert(current_density.n_rows==nvalue.n_rows);

		// estimate lower bound
		arma::Col<double> lower_electric_field(current_density.n_elem,arma::fill::zeros);
		arma::Col<double> higher_electric_field(current_density.n_elem); 
		higher_electric_field.fill(arma::datum::inf);

		// walk over conductors
		arma::uword i = 0;
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
			// get conductor
			const double fraction  = (*it).first; assert(fraction>0);
			const Conductor* conductor = (*it).second;

			// we assume all current runs through this material
			const arma::Col<double> con_current_density = arma::abs(current_density)/fraction;
			
			// calculate electric field
			const arma::Col<double> con_electric_field = conductor->calc_electric_field_fast(
				con_current_density, electrical_resistivity.col(i), 
				critical_current_density.col(i), nvalue.col(i));
			
			// find lowest electric field value
			higher_electric_field = arma::min(higher_electric_field,con_electric_field);
		}

		// calculate current density at these bounds
		// arma::Row<double> vlow = calc_current_density_fast(Elow,Bm,T,alpha,Jc,sigma) - J;
		// arma::Row<double> vhigh = calc_current_density_fast(Ehigh,Bm,T,alpha,Jc,sigma) - J;

		// allocate midpoint output electric field
		arma::Col<double> mid_electric_field(current_density.n_elem);

		// start bisection algorithm
		// find an electric field where all the 
		// supplied current density is soaked by the materials
		for(;;){
			// update mid point
			mid_electric_field = (lower_electric_field + higher_electric_field)/2;

			// calculate intermeditae value
			const arma::Col<double> vmid = calc_current_density_fast(
				mid_electric_field,electrical_resistivity,
				critical_current_density,nvalue) - 
				arma::abs(current_density);

			// update lower and upper bounds
			const arma::Col<arma::uword> idx1 = arma::find(vmid<0);
			const arma::Col<arma::uword> idx2 = arma::find(vmid>=0);

			// update mid-point
			if(!idx1.is_empty())lower_electric_field(idx1) = mid_electric_field(idx1); 
			if(!idx2.is_empty())higher_electric_field(idx2) = mid_electric_field(idx2);

			// check convergence
			if(arma::max(higher_electric_field-lower_electric_field)<tol_bisection_)break;
		}

		// return signed electric field
		return arma::sign(current_density)%mid_electric_field;
	}

	// calculate current density [A m^-2]
	arma::Col<double> ParallelConductor::calc_current_density(
		const arma::Col<double> &electric_field,
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle) const{

		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors);

		// get number of conductors
		const arma::uword num_conductors = parallel_conductors.size();

		// allocate
		arma::Mat<double> rho_mat(temperature.n_elem,num_conductors);
		arma::Mat<double> jc_mat(temperature.n_elem,num_conductors);
		arma::Mat<double> nvalue_mat(temperature.n_elem,num_conductors);

		// walk over conductors
		arma::uword i = 0;
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
			// get conductor
			const Conductor* conductor = (*it).second;

			// calculate electrical resistivity 
			rho_mat.col(i)  = conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude);

			// calculate critical current 
			jc_mat.col(i) = conductor->calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle);

			// calculate nvalue 
			nvalue_mat.col(i) = conductor->calc_nvalue(
				temperature, magnetic_field_magnitude, magnetic_field_angle);
		}

		// calculate using regular method
		return calc_current_density_fast(electric_field, rho_mat, jc_mat, nvalue_mat);
	}

	// current density from electric field 
	// output in [A m^-2]
	arma::Col<double> ParallelConductor::calc_current_density_fast(
		const arma::Col<double> &electric_field,
		const arma::Mat<double> &electrical_resistivity,
		const arma::Mat<double> &critical_current_density, 
		const arma::Mat<double> &nvalue) const{

		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors);

		// check matrix sizes
		assert(electrical_resistivity.n_cols == parallel_conductors.size());
		assert(critical_current_density.n_cols == parallel_conductors.size());
		assert(nvalue.n_cols == parallel_conductors.size());
		assert(electric_field.n_rows == electrical_resistivity.n_rows);
		assert(electric_field.n_rows == critical_current_density.n_rows);
		assert(electric_field.n_rows == nvalue.n_rows);

		// allocate
		arma::Col<double> current_density(electric_field.n_elem,arma::fill::zeros);

		// walk over conductors
		arma::uword i=0;
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
			// get conductor
			const double fraction = (*it).first; assert(fraction>0);
			const Conductor* conductor = (*it).second;

			// accumulate current density
			current_density += fraction*conductor->calc_current_density_fast(
				electric_field, 
				electrical_resistivity.col(i), 
				critical_current_density.col(i), 
				nvalue.col(i));
		}

		// return values
		return current_density;
	}


	// serialization type
	std::string ParallelConductor::get_type(){
		return "rat::mat::parallelconductor";
	}

	// serialization
	void ParallelConductor::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// type
		js["type"] = get_type();

		// parent
		cmn::Nameable::serialize(js,list);

		// serialize conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first; const ShConductorPr &conductor = (*it).second;

			// create sub object to hold both fraction and conductor
			Json::Value js_con;
			js_con["conductor"] = cmn::Node::serialize_node(conductor, list);
			js_con["fraction"] = fraction;

			// serialize nodes
			js["conductor_list"].append(js_con);
		}

		// bisection tolerance
		js["tol_bisection"] = tol_bisection_;
	}

	// deserialization
	void ParallelConductor::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// parent
		cmn::Nameable::deserialize(js,list,factory_list,pth);

		// number of condutcors
		arma::uword num_conductors = js["conductor_list"].size();
		for(arma::uword i=0;i<num_conductors;i++){
			// get sub object
			Json::Value js_con = js["conductor_list"].get(i,0);

			// get fraction and conductor and add to self
			add_conductor(js_con["fraction"].asDouble(), 
				cmn::Node::deserialize_node<Conductor>(
				js_con["conductor"], list, factory_list, pth));
		}

		// bisection tolerance
		tol_bisection_ = js["tol_bisection"].asDouble();
	}

}}