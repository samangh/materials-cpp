/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "nist8fracfit.hh"

// code specific to Rat
namespace rat{namespace mat{


	// default constructor
	Nist8FracFit::Nist8FracFit(){

	}

	// factory
	ShNist8FracFitPr Nist8FracFit::create(){
		return std::make_shared<Nist8FracFit>();
	}

	// copy constructor
	cmn::ShNodePr Nist8FracFit::copy() const{
		return std::make_shared<Nist8FracFit>(*this);
	}


	// set fit parameters
	void Nist8FracFit::set_fit_parameters(const arma::Col<double> &fit_parameters){
		if(fit_parameters.is_empty())rat_throw_line("parameter list is empty");
		fit_parameters_ = fit_parameters;
	}

	// set fit parameters
	void Nist8FracFit::set_temperature_range(const double t1, const double t2){
		if(t2<t1)rat_throw_line("upper temperature must be larger than lower temperature");
		fit_temperature_range_(0) = t1;
		fit_temperature_range_(1) = t2;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Col<double> Nist8FracFit::calc_property(
		const arma::Col<double> &temperature)const{

		// limit lower temperature
		const arma::Col<double> temperature_lim = arma::clamp(
			temperature,fit_temperature_range_(0),arma::datum::inf);

		// calculate fit
		arma::Col<double> k1(temperature_lim.n_elem,arma::fill::zeros); 
		arma::Col<double> k2(temperature_lim.n_elem,arma::fill::ones);
		for(arma::uword i=0;i<5;i++){
			const arma::Col<double> tp = arma::pow(temperature_lim,i*0.5);
			const double f1 = fit_parameters_(2*i);
			k1 += f1*tp; 
			if(i!=0){
				const double f2 = fit_parameters_(2*i-1);
				k2 += f2*tp;
			}
		}

		// take exp10
		arma::Col<double> values = arma::exp10(k1/k2);

		// extrapolation beyond end
		const arma::Col<arma::uword> extrap = arma::find(temperature_lim>fit_temperature_range_(1));
		if(!extrap.is_empty()){
			const double dt = (fit_temperature_range_(1)-fit_temperature_range_(0))/1000;
			const arma::Col<double>::fixed<2> Te = {fit_temperature_range_(1)-dt, fit_temperature_range_(1)-1e-10};
			const arma::Col<double>::fixed<2> ve = calc_property(Te);
			values(extrap) = ve(1) + (temperature_lim(extrap)-Te(1))*((ve(1)-ve(0))/(Te(1)-Te(0)));
		}

		// check output
		assert(values.is_finite()); assert(arma::all(values>0));

		// return thermal conductivity
		return values;
	}

	// specific heat output in [J m^-3 K^-1]
	double Nist8FracFit::calc_property(
		const double temperature)const{

		// catch below range
		if(temperature<fit_temperature_range_(0))
			return calc_property(fit_temperature_range_(0));

		// calculate fit
		double k1 = 0, k2 = 1.0;
		for(arma::uword i=0;i<5;i++){
			const double tp = std::pow(temperature,i*0.5);
			const double f1 = fit_parameters_(2*i);
			k1 += f1*tp; 
			if(i!=0){
				const double f2 = fit_parameters_(2*i-1);
				if(i!=0)k2 += f2*tp;
			}
		}

		// take exp10
		const double k = std::pow(10.0,k1/k2);

		// return thermal conductivity
		return k;
	}

	
	// get type
	std::string Nist8FracFit::get_type(){
		return "rat::mat::nist8fracfit";
	}

	// method for serialization into json
	void Nist8FracFit::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();

		// fit parameters
		js["a"] = fit_parameters_(0);
		js["b"] = fit_parameters_(1);
		js["c"] = fit_parameters_(2);
		js["d"] = fit_parameters_(3);
		js["e"] = fit_parameters_(4);
		js["f"] = fit_parameters_(5);
		js["g"] = fit_parameters_(6);
		js["h"] = fit_parameters_(7);
		js["i"] = fit_parameters_(8);

		// temperature range
		js["t1"] = fit_temperature_range_(0);
		js["t2"] = fit_temperature_range_(1);
	}

	// method for deserialisation from json
	void Nist8FracFit::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		// fit parameters
		fit_parameters_(0) = js["a"].asDouble();
		fit_parameters_(1) = js["b"].asDouble();
		fit_parameters_(2) = js["c"].asDouble();
		fit_parameters_(3) = js["d"].asDouble();
		fit_parameters_(4) = js["e"].asDouble();
		fit_parameters_(5) = js["f"].asDouble();
		fit_parameters_(6) = js["g"].asDouble();
		fit_parameters_(7) = js["h"].asDouble();
		fit_parameters_(8) = js["i"].asDouble();

		// temperature range
		fit_temperature_range_(0) = js["t1"].asDouble();
		fit_temperature_range_(1) = js["t2"].asDouble();
	}

}}