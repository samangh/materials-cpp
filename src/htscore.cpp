/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "htscore.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	HTSCore::HTSCore(){

	}

	// constructor
	HTSCore::HTSCore(const double width, const double thickness, const double tsc, ShConductorPr sc, const double tsubstr, ShConductorPr substr){
		width_ = width; thickness_ = thickness; tsc_ = tsc; tsubstr_ = tsubstr;
		set_sc(sc); set_substr(substr); setup();
	}

	// factory
	ShHTSCorePr HTSCore::create(){
		return std::make_shared<HTSCore>();
	}

	// factory with input
	ShHTSCorePr HTSCore::create(const double width, const double thickness, const double tsc, ShConductorPr sc, const double tsubstr, ShConductorPr substr){
		return std::make_shared<HTSCore>(width, thickness, tsc, sc, tsubstr, substr);
	}

	// copy factory
	cmn::ShNodePr HTSCore::copy() const{
		ShHTSCorePr new_core = HTSCore::create();
		new_core->width_ = width_;
		new_core->thickness_ = thickness_;
		new_core->tsc_ = tsc_;
		new_core->tsubstr_ = tsubstr_;
		new_core->sc_ = sc_->copy_covariant<Conductor>();
		new_core->substr_ = substr_->copy_covariant<Conductor>();
		new_core->tol_bisection_ = tol_bisection_;
		new_core->setup();
		return new_core;
	}

	// set width
	void HTSCore::set_width(const double width){
		if(width<=0)rat_throw_line("width must be positive");
		width_ = width;
	}

	// set thickness
	void HTSCore::set_thickness(const double thickness){
		if(thickness<=0)rat_throw_line("width must be positive");
		thickness_ = thickness;
	}

	// get core width
	double HTSCore::get_width() const{
		return width_;
	}

	// get core thickness
	double HTSCore::get_thickness() const{
		return thickness_;
	}

	// get core area
	double HTSCore::get_area() const{
		return width_*thickness_;
	}


	// get superconductor thickness
	double HTSCore::get_tsc() const{
		return tsc_;
	}

	// get substrate thickness
	double HTSCore::get_tsubstr() const{
		return tsubstr_;
	}

	// get superconductor
	ShConductorPr HTSCore::get_sc() const{
		return sc_;
	}

	// get substrate
	ShConductorPr HTSCore::get_substr() const{
		return substr_;
	}

	// set superconductor
	void HTSCore::set_sc(ShConductorPr sc){
		assert(sc!=NULL);
		sc_ = sc;
	}

	// set superconductor
	void HTSCore::set_substr(ShConductorPr substr){
		assert(substr!=NULL);
		substr_ = substr;
	}

	// critical current [A/m]
	double HTSCore::calc_critical_current_per_width(
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{
		return thickness_*calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle);
	}

	// setup function
	void HTSCore::setup(){
		// add conductor by fraction
		add_conductor(tsc_/thickness_, sc_);
		add_conductor(tsubstr_/thickness_, substr_);
	}

	// serialization type
	std::string HTSCore::get_type(){
		return "rat::mat::htscore";
	}

	// serialization
	void HTSCore::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// serialization type
		js["type"] = get_type();

		// name
		rat::cmn::Nameable::serialize(js,list);

		// dimensions
		js["width"] = width_;
		js["thickness"] = thickness_;

		// layer thicknesses
		js["tsc"] = tsc_;
		js["tsubstr"] = tsubstr_;

		// layers
		js["sc"] = cmn::Node::serialize_node(sc_, list);
		js["substr"] = cmn::Node::serialize_node(substr_, list);

		// bisection tolerance
		js["tol_bisection"] = tol_bisection_;
	}

	// deserialization
	void HTSCore::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// name
		rat::cmn::Nameable::deserialize(js,list,factory_list,pth);

		// dimensions
		width_ = js["width"].asDouble();
		thickness_ = js["thickness"].asDouble();

		// layer thicknesses
		tsc_ = js["tsc"].asDouble();
		tsubstr_ = js["tsubstr"].asDouble();

		// layers
		set_sc(cmn::Node::deserialize_node<Conductor>(js["sc"], list, factory_list, pth));
		set_substr(cmn::Node::deserialize_node<Conductor>(js["substr"], list, factory_list, pth));

		// bisection tolerance
		if(js["tol_bisection"].isNumeric())
			set_tol_bisection(js["tol_bisection"].asDouble());

		// setup as parallel conductor
		setup();
	}

}}