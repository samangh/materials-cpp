/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "fleiterfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	FleiterFit::FleiterFit(){

	}

	// factory
	ShFleiterFitPr FleiterFit::create(){
		return std::make_shared<FleiterFit>();
	}

	// copy constructor
	cmn::ShNodePr FleiterFit::copy() const{
		return std::make_shared<FleiterFit>(*this);
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	double FleiterFit::calc_property(
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{

		// prevent zero field at which a anomalous behaviour occurs
		const double B = std::max(magnetic_field_magnitude,0.1);
		const double T = std::max(temperature,1.0);
		const double alpha = magnetic_field_angle;
		
		// calculate unitless temperature
		const double t = T/Tc0_;

		// irreversibility field for ab-plane
		const double Bi_ab = Bi0ab_*(
			std::pow(1.0-std::pow(t,n1_),n2_)+a_*(1.0-std::pow(t,n_)));

		// calculate unitless magnetic field for ab-plane
		const double bab = B/Bi_ab;

		// irreversibility field for c-plane
		const double Bi_c = Bi0c_*(1.0-std::pow(t,n_));

		// unitless field for c-plane
		const double bc = B/Bi_c;

		// critical current density c-plane
		double Jc_c = (alphac_/B)*std::pow(bc,pc_)*
			std::pow(1.0-bc,qc_)*std::pow(1.0-std::pow(t,n_),gammac_);

		// critical current density ab-plane
		double Jc_ab = (alphaab_/B)*std::pow(bab,pab_)*std::pow(1.0-bab,qab_)*
			std::pow(std::pow(1.0-std::pow(t,n1_),n2_)+a_*(1.0-std::pow(t,n_)),gammaab_);

		// temperature over Tcs
		if(t>=1.0){Jc_c = 0; Jc_ab = 0;};
		if(bc>=1.0)Jc_c = 0;
		if(bab>=1.0)Jc_ab = 0;
		
		// anisotropy factor
		const double g = g0_ + g1_*std::exp(-(g2_*std::exp(g3_*T))*B);

		// Sort out angular dependence
		const double pkoff_rad = 2*arma::datum::pi*pkoff_/360.0;

		// critical current density calculation
		double Jc = std::min(Jc_c,Jc_ab)+
			(std::max(Jc_ab-Jc_c,0.0)/
			(1.0+std::pow((std::abs(std::abs(alpha + 
			!ignore_pkoff_*pkoff_rad) - arma::datum::pi/2.0))/g, nu_)));

		// flipped type-0 pair
		if(type0_flip_){
			Jc += std::min(Jc_c,Jc_ab)+
			(std::max(Jc_ab-Jc_c,0.0)/
			(1.0+std::pow((std::abs(std::abs(alpha - 
			!ignore_pkoff_*pkoff_rad) - arma::datum::pi/2.0))/g, nu_)));
			Jc/=2;
		}

		// return critical current density
		return Jc;
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<double> FleiterFit::calc_property(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle) const{
		
		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);
		assert(temperature.n_elem==magnetic_field_angle.n_elem);
		assert(temperature.is_finite());
		assert(magnetic_field_magnitude.is_finite());
		assert(magnetic_field_angle.is_finite());

		// prevent zero field at which a anomalous behaviour occurs
		const arma::Col<double> B = arma::clamp(magnetic_field_magnitude,0.1,arma::datum::inf);
		const arma::Col<double> T = arma::clamp(temperature,1.0,arma::datum::inf);
		arma::Col<double> alpha = magnetic_field_angle;

		//.make sure alpha is between 0 and 2*pi
		for(arma::uword i=0;i<alpha.n_elem;i++)
			alpha(i) = std::fmod(alpha(i),2*arma::datum::pi);

		// calculate unitless temperature
		const arma::Col<double> t = T/Tc0_;

		// irreversibility field for ab-plane
		const arma::Col<double> Bi_ab = Bi0ab_*(
			arma::pow(1.0-arma::pow(t,n1_),n2_)+a_*(1.0-arma::pow(t,n_)));

		// calculate unitless magnetic field for ab-plane
		const arma::Col<double> bab = B/Bi_ab;

		// irreversibility field for c-plane
		const arma::Col<double> Bi_c = Bi0c_*(1.0-arma::pow(t,n_));

		// unitless field for c-plane
		const arma::Col<double> bc = B/Bi_c;

		// critical current density c-plane
		arma::Col<double> Jc_c = (alphac_/B)%arma::pow(bc,pc_)%
			arma::pow(1.0-bc,qc_)%arma::pow(1.0-arma::pow(t,n_),gammac_);

		// critical current density ab-plane
		arma::Col<double> Jc_ab = (alphaab_/B)%arma::pow(bab,pab_)%arma::pow(1.0-bab,qab_)%
			arma::pow(arma::pow(1.0-arma::pow(t,n1_),n2_)+a_*(1.0-arma::pow(t,n_)),gammaab_);

		// temperature over Tcs
		Jc_c(arma::find(t>=1.0)).fill(0.0); 
		Jc_ab(arma::find(t>=1.0)).fill(0.0);

		// field over Bi
		Jc_c(arma::find(bc>=1.0)).fill(0.0); 
		Jc_ab(arma::find(bab>=1.0)).fill(0.0);

		// anisotropy factor
		const arma::Col<double> g = g0_ + g1_*arma::exp(-(g2_*arma::exp(g3_*T))%B);

		// Sort out angular dependence
		const double pkoff_rad = 2*arma::datum::pi*pkoff_/360.0;

		// critical current density calculation
		arma::Col<double> Jc = arma::min(Jc_c,Jc_ab)+
			(arma::clamp(Jc_ab-Jc_c,0.0,arma::datum::inf)/
			(1.0+arma::pow((arma::abs(arma::abs(alpha + !ignore_pkoff_*pkoff_rad) - arma::datum::pi/2.0))/g,nu_)));

		// flipped type-0 pair
		if(type0_flip_){
			Jc += arma::min(Jc_c,Jc_ab)+
			(arma::clamp(Jc_ab-Jc_c,0.0,arma::datum::inf)/
			(1.0+arma::pow((arma::abs(arma::abs(alpha - !ignore_pkoff_*pkoff_rad) - arma::datum::pi/2.0))/g,nu_)));
			Jc/=2;
		}

		// check output
		assert(Jc.n_elem==temperature.n_elem);
		assert(Jc.is_finite());

		// return critical current density
		return Jc;
	}

	// serialization
	// get type
	std::string FleiterFit::get_type(){
		return "rat::mat::fleiterfit";
	}

	// method for serialization into json
	void FleiterFit::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();
		
		// temperature dependence
		js["Tc0"] = Tc0_;
		js["n"] = n_;
		js["n1"] = n1_;
		js["n2"] = n2_;

		// AB-plane
		js["pab"] = pab_;
		js["qab"] = qab_;
		js["Bi0ab"] = Bi0ab_;
		js["a"] = a_;
		js["gammaab"] = gammaab_;
		js["alphaab"] = alphaab_;

		// C-axis
		js["pc"] = pc_;
		js["qc"] = qc_;
		js["Bi0c"] = Bi0c_;
		js["gammac"] = gammac_;
		js["alphac"] = alphac_;

		// anisotropy
		js["g0"] = g0_;
		js["g1"] = g1_;
		js["g2"] = g2_;
		js["g3"] = g3_;
		js["nu"] = nu_;
		js["pkoff"] = pkoff_;

		// typical layer thicknesses and width
		js["tsc"] = tsc_;

		// type-0 pair flipping
		js["type0_flip"] = type0_flip_;
		js["ignore_pkoff"] = ignore_pkoff_;
	}

	// method for deserialisation from json
	void FleiterFit::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/, const boost::filesystem::path &/*pth*/){
		// general
		Tc0_ = js["Tc0"].asDouble();
		n_ = js["n"].asDouble();
		n1_ = js["n1"].asDouble();
		n2_ = js["n2"].asDouble();

		// AB-plane
		pab_ = js["pab"].asDouble();
		qab_ = js["qab"].asDouble();
		Bi0ab_ = js["Bi0ab"].asDouble();
		a_ = js["a"].asDouble();
		gammaab_ = js["gammaab"].asDouble();
		alphaab_ = js["alphaab"].asDouble();

		// C-axis
		pc_ = js["pc"].asDouble();
		qc_ = js["qc"].asDouble();
		Bi0c_ = js["Bi0c"].asDouble();
		gammac_ = js["gammac"].asDouble();
		alphac_ = js["alphac"].asDouble();

		// anisotropy
		g0_ = js["g0"].asDouble();
		g1_ = js["g1"].asDouble();
		g2_ = js["g2"].asDouble();
		g3_ = js["g3"].asDouble();
		nu_ = js["nu"].asDouble();
		pkoff_ = js["pkoff"].asDouble();

		// typical layer thickness
		tsc_ = js["tsc"].asDouble();

		// type-0 pair flipping
		type0_flip_ = js["type0_flip"].asBool();
		ignore_pkoff_ = js["ignore_pkoff"].asBool();
	}

}}