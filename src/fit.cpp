/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// calculate property as function of scalar temperature
	double Fit::calc_property(
		const double /*temperature*/) const{
		rat_throw_line("insufficient parameters supplied or scalar property functions not overriden");
		return 0;
	}

	// calculate property as function of scalar
	// temperature and magnetic field magnitude
	double Fit::calc_property(
		const double temperature,
		const double /*magnetic_field_magnitude*/) const{
		return calc_property(temperature);
	}

	// calculate property as function of scalar
	// temperature and magnetic field magnitude
	double Fit::calc_property(
		const double temperature,
		const double magnetic_field_magnitude,
		const double /*magnetic_field_angle*/) const{
		return calc_property(temperature, magnetic_field_magnitude);
	}

	// calculate property as function of vector temperature
	arma::Col<double> Fit::calc_property(
		const arma::Col<double> &/*temperature*/) const{
		rat_throw_line("insufficient parameters supplied or vector property functions not overriden");
		return arma::Col<double>{};
	}

	// calculate property as function of vector
	// temperature and magnetic field magnitude
	arma::Col<double> Fit::calc_property(
		const arma::Col<double> &temperature,
		const arma::Col<double> &/*magnetic_field_magnitude*/) const{
		return calc_property(temperature);
	}

	// calculate property as function of vector
	// temperature, magnetic field magnitude and angle
	arma::Col<double> Fit::calc_property(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &/*magnetic_field_angle*/) const{
		return calc_property(temperature, magnetic_field_magnitude);
	}

}}