/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "nzpmodel.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	NZPModel::NZPModel(){

	}

	// constructor
	NZPModel::NZPModel(ShConductorPr conductor){
		set_conductor(conductor);
	}

	// factory
	ShNZPModelPr NZPModel::create(){
		return std::make_shared<NZPModel>();
	}

	// factory
	ShNZPModelPr NZPModel::create(ShConductorPr conductor){
		return std::make_shared<NZPModel>(conductor);
	}

	// set the conductor
	void NZPModel::set_conductor(ShConductorPr conductor){
		if(conductor==NULL)rat_throw_line("Conductor points to NULL");
		conductor_ = conductor;
	}

	// set current density
	void NZPModel::set_current_density(const double current_density){
		current_density_ = current_density;
	}

	// set magnetic field magnitude
	void NZPModel::set_magnetic_field_magnitude(const double magnetic_field_magnitude){
		magnetic_field_magnitude_ = magnetic_field_magnitude;
	}

	// set magnetic field angle
	void NZPModel::set_magnetic_field_angle(const double magnetic_field_angle){
		magnetic_field_angle_ = magnetic_field_angle;
	}

	// set operating temperature
	void NZPModel::set_operating_temperature(const double operating_temperature){
		if(operating_temperature<0)rat_throw_line("negative temperature provided");
		operating_temperature_ = operating_temperature;
	}

	// set operating temperature
	void NZPModel::set_element_size(const double element_size){
		if(element_size<=0)rat_throw_line("element length must be larger than zero");
		element_size_ = element_size;
	}

	// set length
	void NZPModel::set_tape_length(const double tape_length){
		tape_length_ = tape_length;
	}

	// system function
	arma::Col<double> NZPModel::bdf_system_fun(
		const arma::Col<double> &temperature_prev,
		const arma::Col<double> &temperature_next,
		const double h) const{

		// if(arma::any(temperature_next<0))
		// rat_throw_line("temperature out of range");

		// number of nodes
		const arma::uword num_nodes = temperature_prev.n_elem;

		// calculate element length
		const arma::Col<double> element_length = arma::diff(position_vec_);
		arma::Col<double> node_length(num_nodes,arma::fill::zeros);
		node_length.head_rows(num_nodes-1) += element_length/2;
		node_length.tail_rows(num_nodes-1) += element_length/2;

		// allocate material properties at nodes
		arma::Col<double> electric_field(num_nodes);
		arma::Col<double> electric_field_dT(num_nodes);
		arma::Col<double> thermal_conductivity(num_nodes);
		arma::Col<double> specific_heat(num_nodes);

		// walk over nodes
		rat::cmn::parfor(0,num_nodes,use_parallel_,[&](int i, int){
			// electric field
			electric_field(i) = conductor_->calc_electric_field(
				current_density_, temperature_next(i), 
				magnetic_field_magnitude_, magnetic_field_angle_); // V/m

			// electric field derivative
			electric_field_dT(i) = conductor_->calc_electric_field_dT(
				current_density_, temperature_next(i), 
				magnetic_field_magnitude_, magnetic_field_angle_); // V/m/K

			// calculate thermal conductivity
			thermal_conductivity(i) = conductor_->calc_thermal_conductivity(
				temperature_next(i), magnetic_field_magnitude_); // [W m^-1 K^-1]

			// calculate heat capacity
			specific_heat(i) = conductor_->calc_volumetric_specific_heat(
				temperature_next(i)); // [J m^-3 K^-1]
		});

		// calculate temperature gradient 
		const arma::Col<double> temperature_gradient = 
			arma::diff(temperature_next)/element_length; // [K/m]

		// claculate thermal conductivity at elements (interpolate from nodes)
		const arma::Col<double> elemental_thermal_conductivity = 
			(thermal_conductivity.head_rows(num_nodes-1) + 
			thermal_conductivity.tail_rows(num_nodes-1))/2; // [W m^-1 K^-1]

		// calculate heat flux
		const arma::Col<double> heat_flux = 
			-elemental_thermal_conductivity%temperature_gradient; // [W m^-2]

		// calculate power density including electric heating and heat flux
		arma::Col<double> power_density = electric_field*current_density_; // [W m^-3]
		power_density.head_rows(num_nodes-1) -= heat_flux/element_length;
		power_density.tail_rows(num_nodes-1) += heat_flux/element_length;

		// calculate residuals
		arma::Col<double> rr = temperature_prev + h*(power_density/specific_heat) - temperature_next;

		// create jacobian linearisation
		arma::Col<double> keff = elemental_thermal_conductivity/element_length; // [W m^-2 K^-1] 
		arma::Col<double> kmid(num_nodes,arma::fill::zeros); 
		kmid.head_rows(num_nodes-1) += keff/element_length;
		kmid.tail_rows(num_nodes-1) += keff/element_length;

		// assemble jacobian matrix
		#ifdef ARMA_USE_SUPERLU
		arma::SpMat<double> J(num_nodes,num_nodes);
		#else
		arma::Mat<double> J(num_nodes,num_nodes,arma::fill::zeros);
		#endif

		// add diagonal and off-diagonal values (band-matrix)
		J.diag(-1) = h*keff/element_length/specific_heat.head_rows(num_nodes-1); 
		J.diag(0) = h*(electric_field_dT*current_density_ - kmid)/specific_heat - 1.0;
		J.diag(1) = h*keff/element_length/specific_heat.tail_rows(num_nodes-1);

		// fix temperature at end (this effectively grounds
		// the equation to avoid singularities)
		// J.row(num_nodes-1).fill(0); J.col(num_nodes-1).fill(0);
		// J(num_nodes-1,num_nodes-1) = 1.0; rr(num_nodes-1) = 0;

		// solve system and return
		arma::Col<double> v;
		#ifdef ARMA_USE_SUPERLU
		arma::superlu_opts opts;
		opts.pivot_thresh = 0.1;
		bool flag = arma::spsolve(v,J,rr,"superlu",opts);
		if(flag==false)rat_throw_line("Unable to solve sparse system with superlu");
		#else
		bool flag = arma::solve(v,J,rr);
		if(flag==false)rat_throw_line("Unable to solve dense system");
		#endif

		// return v
		return v;
	}

	// calculate the normal zone propagation velocity in [m s^-1]
	void NZPModel::solve(cmn::ShLogPr lg){

		// check if conductor set
		if(conductor_==NULL)rat_throw_line("conductor not set");

		// display warning
		#ifndef ARMA_USE_SUPERLU
		lg->msg("solving nzp without SuperLU\n");
		lg->msg("this may be slower\n");
		#endif

		// calculate number of nodes
		const arma::uword num_nodes = 2*std::max((int)(tape_length_/element_size_/2),2)+1;

		// position vector
		position_vec_ = arma::linspace<arma::Col<double> >(0, tape_length_, num_nodes);

		// set start condition
		arma::Col<double> temperature_vec(num_nodes); 
		temperature_vec.fill(operating_temperature_);

		// quench the tape on the left side
		temperature_vec(0) += 50.0;

		// set initial time and timestep
		double time = 0.0;
		double timestep_size = timestep_ini_;

		// new temperature array
		arma::Col<double> temperature_guess = temperature_vec;

		// create log
		arma::uword num_disp = std::min((arma::uword)15,num_nodes);
		lg->msg("%8s | %64s","time [s]","temperatures [K]\n");
		const arma::uword table_size = 73;
		lg->msg("="); for(arma::uword i=0;i<table_size;i++)lg->msg(0,"="); lg->msg(0,"\n");

		// position header
		lg->msg("%8s | " ,"x [mm]");
		for(arma::uword i=0;i<15;i++)lg->msg(0,"%3.0f ",1e3*position_vec_(i));
		lg->msg(0,"\n");


		// walk over timesteps
		while(time<max_time_){

			// set solver vars
			double err = arma::datum::inf;
			double alpha = 1.0;

			// solve timestep using newton-raphson iterations
			for(arma::uword i=0;i<max_iter_;i++){
				// calculate new temperature
				const arma::Col<double> temperature_new = temperature_guess - 
					alpha*bdf_system_fun(temperature_vec, temperature_guess, timestep_size);
				
				// calculate maximum absolute error
				double new_err = arma::max(arma::abs(temperature_new - temperature_guess));
				if(new_err>err){
					alpha=std::max(0.1,alpha/1.2);
				}

				// update guess
				err = new_err;
				temperature_guess = temperature_new;

				// stop condition
				if(err<abs_tol_)break;
			}

			// reduce timestep and try again
			if(err>abs_tol_){
				timestep_size /= 2;
				continue;
			}

			// update temperature vector
			double dTdtmax = arma::max(arma::abs(temperature_vec-temperature_guess));
			temperature_vec = temperature_guess; time += timestep_size;

			// update time step
			timestep_size = std::min(max_time_step_, timestep_size*max_temperature_step_/dTdtmax);

			// limit temperature drop
			temperature_vec(0) = std::max(temperature_vec(0),operating_temperature_+50);

			// store
			nzp_data_.push_back(std::pair<double, arma::Col<double> >(time, temperature_vec));

			// show time
			lg->msg("%8.3f | ",time);
			for(arma::uword i=0;i<num_disp;i++){
				if(temperature_vec(i)>300){
					lg->msg(0,"%s%03.0f%s ",KMAG,temperature_vec(i),KNRM);
				}else if(temperature_vec(i)>200){
					lg->msg(0,"%s%03.0f%s ",KRED,temperature_vec(i),KNRM);
				}else if(temperature_vec(i)>100){
					lg->msg(0,"%s%03.0f%s ",KYEL,temperature_vec(i),KNRM);
				}else if(temperature_vec(i)>50){
					lg->msg(0,"%s%03.0f%s ",KGRN,temperature_vec(i),KNRM);
				}else{
					lg->msg(0,"%s%03.0f%s ",KBLU,temperature_vec(i),KNRM);
				}
			}
			lg->msg(0,"...\n");

			// check stop conditions
			if(temperature_vec.max()>max_temperature_)break;
		}
	}

	// calculate normal zone velocity
	arma::Col<double> NZPModel::calc_velocity() const{
		// current sharing temperature
		const double cs_temperature = conductor_->calc_cs_temperature(
			current_density_, magnetic_field_magnitude_, magnetic_field_angle_);

		// critical temperature (at zero current)
		const double crit_temperature = conductor_->calc_cs_temperature(
			0, magnetic_field_magnitude_, magnetic_field_angle_);

		// transition temperature between Tc and Tcs
		const double transition_temperature = (cs_temperature + crit_temperature)/2;

		// post process data
		arma::Col<double> xnzf(nzp_data_.size());
		arma::Col<double> times(nzp_data_.size());
		arma::uword i = 0;
		for(auto it = nzp_data_.begin(); it!=nzp_data_.end(); it++,i++){
			times(i) = (*it).first;
			arma::Col<double> temperature = (*it).second;

			// use interpolation to find the normal zone front
			xnzf(i) = rat::cmn::Extra::interp1(
				temperature, position_vec_, transition_temperature);
		}

		// calculate velocity
		const arma::Col<double> vnzp = arma::diff(xnzf)/arma::diff(times);

		// return
		return arma::join_vert(arma::Col<double>{0},vnzp);
	}

	// get peak temperature
	// calculate normal zone velocity
	arma::Col<double> NZPModel::calc_peak_temperature() const{
		// post process data
		arma::Col<double> peak_temperature(nzp_data_.size());
		arma::uword i = 0;
		for(auto it = nzp_data_.begin(); it!=nzp_data_.end(); it++,i++)
			peak_temperature(i) = (*it).second.max();
		return peak_temperature;
	}

	// get voltage [V]
	arma::Col<double> NZPModel::calc_voltage() const{
		arma::uword i = 0;
		arma::Col<double> voltage(nzp_data_.size());
		for(auto it = nzp_data_.begin(); it!=nzp_data_.end(); it++,i++){
			// get temperature
			const arma::Col<double> temperature = (*it).second;
			const arma::uword num_nodes = temperature.n_elem;

			// calculate electric field
			arma::Col<double> electric_field(num_nodes);
			rat::cmn::parfor(0,num_nodes,use_parallel_,[&](int j, int){
				// electric field
				electric_field(j) = conductor_->calc_electric_field(
					current_density_, temperature(j), 
					magnetic_field_magnitude_, magnetic_field_angle_); // V/m
			});

			// electric field at elements
			const arma::Col<double> electric_field_elements = 
				(electric_field.head_rows(num_nodes-1) + 
				electric_field.tail_rows(num_nodes-1))/2;

			// calculate voltage 
			voltage(i) = arma::accu(arma::diff(position_vec_)%electric_field_elements);
		}

		// return voltage vector
		// times two for symmetry
		return 2*voltage;
	}

	// get resistance [Ohm m^2]	
	arma::Col<double> NZPModel::calc_resistance() const{
		return calc_voltage()/current_density_;
	}

	// get resistance [Ohm m^2]	
	double NZPModel::calc_resistance(
		const double peak_temperature) const{
		// get voltage and peak temperature
		const arma::Col<double> resistance = calc_resistance();
		const arma::Col<double> temperature = calc_peak_temperature();
		
		// find time of temperature limit break
		const arma::Col<arma::uword> idx_temperature = 
			arma::find(temperature>peak_temperature,1,"first");
		double normal_zone_resistance = arma::datum::nan;
		if(!idx_temperature.is_empty()){
			normal_zone_resistance = cmn::Extra::interp1(
				arma::join_vert(temperature.rows(idx_temperature-1),temperature.rows(idx_temperature)),
				arma::join_vert(resistance.rows(idx_temperature-1),resistance.rows(idx_temperature)),
				peak_temperature);
		}
		return normal_zone_resistance;
	}

	// get time vector
	arma::Col<double> NZPModel::get_times() const{
		arma::Col<double> times(nzp_data_.size()); arma::uword i = 0;
		for(auto it = nzp_data_.begin(); it!=nzp_data_.end(); it++,i++)
			times(i) = (*it).first;
		return times;
	}

	// calculate time between 100 mv and 300 K
	double NZPModel::calc_burn_time(
		const double voltage_treshold, 
		const double temperature_limit) const{

		// get arrays
		arma::Col<double> t = get_times();
		arma::Col<double> V = arma::abs(calc_voltage());
		arma::Col<double> Tp = calc_peak_temperature();

		// allocate times
		double t0 = arma::datum::nan;
		double t1 = arma::datum::nan;

		// find time of voltage treshold break
		const arma::Col<arma::uword> idx_voltage = 
			arma::find(V>voltage_treshold,1,"first");
		if(!idx_voltage.is_empty()){
			if(idx_voltage(0)>0){
				t0 = cmn::Extra::interp1(
					arma::join_vert(V.rows(idx_voltage-1),V.rows(idx_voltage)),
					arma::join_vert(t.rows(idx_voltage-1),t.rows(idx_voltage)),
					voltage_treshold);
			}
		}

		// find time of temperature limit break
		const arma::Col<arma::uword> idx_temperature = 
			arma::find(Tp>temperature_limit,1,"first");
		if(!idx_temperature.is_empty()){
			if(idx_temperature(0)>0){
				t1 = cmn::Extra::interp1(
					arma::join_vert(Tp.rows(idx_temperature-1),Tp.rows(idx_temperature)),
					arma::join_vert(t.rows(idx_temperature-1),t.rows(idx_temperature)),
					temperature_limit);
			}
		}

		// return time difference
		return t1-t0;
	}

}}