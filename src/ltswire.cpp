/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "ltswire.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	LTSWire::LTSWire(){

	}

	// factory
	ShLTSWirePr LTSWire::create(){
		return std::make_shared<LTSWire>();
	}

	// copy factory
	cmn::ShNodePr LTSWire::copy() const{
		ShLTSWirePr new_wire = LTSWire::create();
		new_wire->diameter_ = diameter_;
		new_wire->fnc2sc_ = fnc2sc_;
		new_wire->sc_ = sc_->copy_covariant<Conductor>();
		new_wire->nc_ = nc_->copy_covariant<Conductor>();
		new_wire->tol_bisection_ = tol_bisection_;
		new_wire->setup();
		return new_wire;
	}

	// set dimensions
	void LTSWire::set_diameter(const double diameter){
		if(diameter<0)rat_throw_line("diameter must be larger than zero");
		diameter_ = diameter;
	}

	// set normal conductor to superconductor fraction
	void LTSWire::set_fnc2sc(const double fnc2sc){
		if(fnc2sc<0)rat_throw_line("fnc2sc must be larger than zero");
		fnc2sc_ = fnc2sc;
	}

	// get core width
	double LTSWire::get_diameter() const{
		return diameter_;
	}

	// get core thickness
	double LTSWire::get_fnc2sc() const{
		return fnc2sc_;
	}

	// get superconductor
	ShConductorPr LTSWire::get_sc() const{
		return sc_;
	}

	// get substrate
	ShConductorPr LTSWire::get_nc() const{
		return nc_;
	}

	// set superconductor
	void LTSWire::set_sc(ShConductorPr sc){
		assert(sc!=NULL);
		sc_ = sc;
	}

	// set superconductor
	void LTSWire::set_nc(ShConductorPr nc){
		assert(nc!=NULL);
		nc_ = nc;
	}

	// setup function
	void LTSWire::setup(){
		// add conductor by fraction
		add_conductor(1.0/(fnc2sc_+1.0), sc_);
		add_conductor(fnc2sc_/(fnc2sc_+1.0), nc_);
	}

	// serialization type
	std::string LTSWire::get_type(){
		return "rat::mat::ltswire";
	}

	// serialization
	void LTSWire::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// serialization type
		js["type"] = get_type();

		// name
		rat::cmn::Nameable::serialize(js,list);

		// dimensions
		js["fnc2sc"] = fnc2sc_;
		js["diameter"] = diameter_;

		// layers
		js["sc"] = cmn::Node::serialize_node(sc_, list);
		js["nc"] = cmn::Node::serialize_node(nc_, list);

		// bisection tolerance
		js["tol_bisection"] = tol_bisection_;
	}

	// deserialization
	void LTSWire::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// name
		rat::cmn::Nameable::deserialize(js,list,factory_list,pth);

		// dimensions
		fnc2sc_ = js["fnc2sc"].asDouble();
		diameter_ = js["diameter"].asDouble();

		// layers
		set_sc(cmn::Node::deserialize_node<Conductor>(js["sc"], list, factory_list, pth));
		set_nc(cmn::Node::deserialize_node<Conductor>(js["nc"], list, factory_list, pth));

		// bisection tolerance
		if(js["tol_bisection"].isNumeric())
			set_tol_bisection(js["tol_bisection"].asDouble());

		// setup as parallel conductor
		setup();
	}

}}