/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "seriesconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	SeriesConductor::SeriesConductor(){

	}

	// factory
	ShSeriesConductorPr SeriesConductor::create(){
		return std::make_shared<SeriesConductor>();
	}

	// copy factory
	cmn::ShNodePr SeriesConductor::copy() const{
		// create new conductor
		ShSeriesConductorPr mycopy = SeriesConductor::create();

		// walk over conductors and copy one by one
		arma::uword i=0;
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++,i++){
			// get conductor
			const double fraction = (*it).first; assert(fraction>0);
			const ShConductorPr conductor = (*it).second;
			mycopy->add_conductor(fraction, conductor->copy_covariant<Conductor>());
		}
		
		// copy settings
		mycopy->tol_bisection_ = tol_bisection_;

		// return copy
		return mycopy;
	}

	// add material to list
	void SeriesConductor::add_conductor(const double fraction, ShConductorPr conductor){
		conductor_list_.push_back(FracShConPr(fraction,conductor));
	}

	// accumulate individual conductors in a list
	// if a series conductor is encountered it is added as whole
	void SeriesConductor::accu_series_conductors(
		FracConPrList &series_conductors) const{

		// this conductor is parallel so we take our children
		for(auto it = conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// allocate a new list
			FracConPrList series_conductors_child;

			// add conductors to the list
			conductor->accu_parallel_conductors(series_conductors_child);

			// multiply factions and add conductor to the general list
			for(auto it2 = series_conductors_child.begin();
				it2!=series_conductors_child.end();it2++)(*it2).first *= fraction;

			// combine lists
			series_conductors.insert(
				series_conductors.end(), 
				series_conductors_child.begin(), 
				series_conductors_child.end());
		}
	}

	// material properties for scalar input
	// calculate specific heat [J kg^-1 K^-1]
	double SeriesConductor::calc_specific_heat(
		const double temperature) const{
		
		// allocate
		double specific_heat = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional specific heat
			specific_heat += fraction*conductor->calc_specific_heat(temperature);
		}

		// return values
		return specific_heat;
	}

	// thermal conductivity [W m^-1 K^-1]
	double SeriesConductor::calc_thermal_conductivity(
		const double temperature,
		const double magnetic_field_magnitude) const{
		
		// allocate
		double thermal_resistivity = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// calculate thermal conductivity of this layer
			const double thermal_conductivity = conductor->calc_thermal_conductivity(
				temperature, magnetic_field_magnitude);

			// accumulate fractional thermal resistivities
			thermal_resistivity += fraction/thermal_conductivity;
		}

		// return values
		return 1.0/thermal_resistivity;
	}

	// electrical resistivity [Ohm m^2]
	double SeriesConductor::calc_electrical_resistivity(
		const double temperature,
		const double magnetic_field_magnitude) const{
		
		// allocate
		double electrical_resistivity = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional electrical conductivities
			electrical_resistivity += fraction*
				conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude);
		}

		// take inverse to get resistivity and return
		return electrical_resistivity;
	}

	// material density [kg m^-3]
	double SeriesConductor::calc_density(
		const double temperature) const{

		// allocate
		double density = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional densities
			density += fraction*conductor->calc_density(temperature);
		}

		// return values
		return density;
	}

	// critical current density [J m^-2]
	double SeriesConductor::calc_critical_current_density(
		const double /*temperature*/,
		const double /*magnetic_field_magnitude*/,
		const double /*magnetic_field_angle*/) const{

		// return values
		return 0;
	}

	// calculate power law nvalue
	double SeriesConductor::calc_nvalue(
		const double /*temperature*/,
		const double /*magnetic_field_magnitude*/,
		const double /*magnetic_field_angle*/) const{

		// return values
		return 0;
	}


	// electric field [V/m]
	double SeriesConductor::calc_electric_field(
		const double current_density,
		const double temperature,
		const double magnetic_field_magnitude,
		const double /*magnetic_field_angle*/) const{
		
		// calculate field using Ohms Law
		return current_density*calc_electrical_resistivity(
			temperature, magnetic_field_magnitude);
	}

	// electric field using pre-calculated electrical 
	// resistivity en critical current density [V/m]
	// this uses bisection method
	double SeriesConductor::calc_electric_field_fast(
		const double current_density,
		const arma::Row<double> &electrical_resistivity_vec,
		const arma::Row<double> &/*critical_current_density_vec*/,
		const arma::Row<double> &/*nvalue_vec*/) const{

		// convert to scalars
		const double electrical_resistivity = arma::as_scalar(electrical_resistivity_vec);
		
		// electric field based on resistivity
		return electrical_resistivity*current_density;
	}

	// electric field [V/m]
	double SeriesConductor::calc_current_density(
		const double electric_field,
		const double temperature,
		const double magnetic_field_magnitude,
		const double /*magnetic_field_angle*/) const{
		
		// calculate field using Ohms Law
		return electric_field/calc_electrical_resistivity(
			temperature, magnetic_field_magnitude);
	}

	// calculate current density [A m^-2]
	double SeriesConductor::calc_current_density_fast(
		const double electric_field,
		const arma::Row<double> &electrical_resistivity_vec,
		const arma::Row<double> &/*critical_current_density_vec*/,
		const arma::Row<double> &/*nvalue_vec*/) const{

		// convert to scalars
		const double electrical_resistivity = arma::as_scalar(electrical_resistivity_vec);
		
		// current density from Ohm's Law
		return electric_field/electrical_resistivity;
	}


	// material properties for vector input
	// calculate specific heat [J kg^-1 K^-1]
	arma::Col<double> SeriesConductor::calc_specific_heat(
		const arma::Col<double> &temperature) const{
		
		// allocate
		arma::Col<double> specific_heat(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional specific heat
			specific_heat += fraction*conductor->calc_specific_heat(temperature);
		}

		// return values
		return specific_heat;
	}

	// thermal conductivity [W m^-1 K^-1]
	arma::Col<double> SeriesConductor::calc_thermal_conductivity(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude) const{
		
		// allocate
		arma::Col<double> thermal_resistivity(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// calculate conductivity of this layer
			const arma::Col<double> thermal_conductivity = 
				conductor->calc_thermal_conductivity(
				temperature, magnetic_field_magnitude);

			// accumulate fractional thermal conductivities
			thermal_resistivity += fraction/thermal_conductivity;
		}

		// return values
		return 1.0/thermal_resistivity;
	}

	// electrical resistivity [Ohm m^2]
	arma::Col<double> SeriesConductor::calc_electrical_resistivity(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude) const{
		
		// allocate
		arma::Col<double> electrical_resistivity(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional electrical conductivities
			electrical_resistivity += fraction*
				conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude);
		}

		// take inverse to get resistivity and return
		return electrical_resistivity;
	}

	// material density [kg m^-3]
	arma::Col<double> SeriesConductor::calc_density(
		const arma::Col<double> &temperature) const{
		
		// allocate
		arma::Col<double> density(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first;
			const ShConductorPr &conductor = (*it).second;

			// accumulate fractional densities
			density += fraction*conductor->calc_density(temperature);
		}

		// return values
		return density;
	}

	// critical current density [J m^-2]
	arma::Col<double> SeriesConductor::calc_critical_current_density(
		const arma::Col<double> &temperature,
		const arma::Col<double> &/*magnetic_field_magnitude*/,
		const arma::Col<double> &/*magnetic_field_angle*/) const{

		// return values
		return arma::Col<double>(temperature.n_elem, arma::fill::zeros);
	}

	// critical current density [J m^-2]
	arma::Col<double> SeriesConductor::calc_nvalue(
		const arma::Col<double> &temperature,
		const arma::Col<double> &/*magnetic_field_magnitude*/,
		const arma::Col<double> &/*magnetic_field_angle*/) const{

		// return values
		return arma::Col<double>(temperature.n_elem, arma::fill::zeros);
	}


	// electric field calculation [V/m]
	arma::Col<double> SeriesConductor::calc_electric_field(
		const arma::Col<double> &current_density,
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &/*magnetic_field_angle*/) const{

		// calculate field using Ohms Law
		return current_density%calc_electrical_resistivity(
			temperature, magnetic_field_magnitude);
	}

	// electric field [V m^-1] section averaged
	arma::Col<double> SeriesConductor::calc_electric_field_av(
		const arma::Col<double> &current_density,
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &/*magnetic_field_angle*/,
		const arma::Mat<arma::uword> &nelements,
		const arma::Row<double> &cross_areas) const{

		// calculate cross section averaged values
		const arma::Mat<double> rho_section = 
			Conductor::nodes2section(calc_electrical_resistivity(
			temperature, magnetic_field_magnitude), nelements, cross_areas);
		const arma::Col<double> j_section = 
			Conductor::nodes2section(current_density, nelements, cross_areas);

		// calculate electric field for each section
		const arma::Col<double> electric_field_section = rho_section%j_section;

		// back to nodes
		return Conductor::section2nodes(
			electric_field_section, nelements, cross_areas);
	}

	// electric field using pre-calculated electrical 
	// resistivity en critical current density [V/m]
	arma::Col<double> SeriesConductor::calc_electric_field_fast(
		const arma::Col<double> &current_density,
		const arma::Mat<double> &electrical_resistivity,
		const arma::Mat<double> &/*critical_current_density*/, 
		const arma::Mat<double> &/*nvalue*/) const{

		// check if column vector supplied
		assert(electrical_resistivity.n_cols==1);

		// electric field based on resistivity
		return electrical_resistivity*current_density;
	}

	// calculate current density [A m^-2]
	arma::Col<double> SeriesConductor::calc_current_density(
		const arma::Col<double> &electric_field,
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &/*magnetic_field_angle*/) const{

		// calculate field using Ohms Law
		return electric_field/calc_electrical_resistivity(
			temperature, magnetic_field_magnitude);
	}

	// current density from electric field 
	// output in [A m^-2]
	arma::Col<double> SeriesConductor::calc_current_density_fast(
		const arma::Col<double> &electric_field,
		const arma::Mat<double> &electrical_resistivity,
		const arma::Mat<double> &/*critical_current_density*/, 
		const arma::Mat<double> &/*nvalue*/) const{

		// check if column vector supplied
		assert(electrical_resistivity.n_cols==1);

		// electric field based on resistivity
		return electric_field/electrical_resistivity;
	}


	// serialization type
	std::string SeriesConductor::get_type(){
		return "rat::mat::seriesconductor";
	}

	// serialization
	void SeriesConductor::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// type
		js["type"] = get_type();

		// parent
		cmn::Nameable::serialize(js,list);

		// serialize conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const double fraction = (*it).first; const ShConductorPr &conductor = (*it).second;

			// create sub object to hold both fraction and conductor
			Json::Value js_con;
			js_con["conductor"] = cmn::Node::serialize_node(conductor, list);
			js_con["fraction"] = fraction;

			// serialize nodes
			js["conductor_list"].append(js_con);
		}
	}

	// deserialization
	void SeriesConductor::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// parent
		cmn::Nameable::deserialize(js,list,factory_list,pth);

		// number of condutcors
		arma::uword num_conductors = js["conductor_list"].size();
		for(arma::uword i=0;i<num_conductors;i++){
			// get sub object
			Json::Value js_con = js["conductor_list"].get(i,0);

			// get fraction and conductor and add to self
			add_conductor(js_con["fraction"].asDouble(), 
				cmn::Node::deserialize_node<Conductor>(
				js_con["conductor"], list, factory_list, pth));
		}
	}

}}