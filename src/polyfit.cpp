/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "polyfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	PolyFit::PolyFit(){

	}

	// factory
	ShPolyFitPr PolyFit::create(){
		return std::make_shared<PolyFit>();
	}


	// create by copy
	cmn::ShNodePr PolyFit::copy() const{
		return std::make_shared<PolyFit>(*this);
	}


	// set fit parameters
	void PolyFit::set_fit_parameters(const arma::Col<double> &fit_parameters){
		if(fit_parameters.is_empty())rat_throw_line("parameter list is empty");
		fit_parameters_ = fit_parameters;
	}

	// set fit parameters
	void PolyFit::set_temperature_range(const double t1, const double t2){
		if(t2<t1)rat_throw_line("upper temperature must be larger than lower temperature");
		fit_temperature_range_(0) = t1; 
		fit_temperature_range_(1) = t2;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Col<double> PolyFit::calc_property(const arma::Col<double> &temperature)const{
		// limit lower temperature
		const arma::Col<double> T = arma::clamp(temperature,fit_temperature_range_(0),arma::datum::inf);

		// use armadillo polyval
		arma::Col<double> values = arma::polyval(fit_parameters_, T);

		// extrapolation beyond end
		const arma::Col<arma::uword> extrap = arma::find(T>fit_temperature_range_(1));
		if(!extrap.is_empty()){
			const double dt = (fit_temperature_range_(1)-fit_temperature_range_(0))/1000;
			const arma::Col<double>::fixed<2> Te = {fit_temperature_range_(1)-dt, fit_temperature_range_(1)-1e-10};
			const arma::Col<double>::fixed<2> ve = calc_property(Te);
			values(extrap) = ve(1) + (T(extrap)-Te(1))*((ve(1)-ve(0))/(Te(1)-Te(0)));
		}

		// return calculated values
		return values;
	}

	// specific heat output in [J m^-3 K^-1]
	double PolyFit::calc_property(const double temperature)const{
		// allocate
		double value = 0;

		// extrapolation beyond end
		if(temperature>fit_temperature_range_(1)){
			const double dt = (fit_temperature_range_(1)-fit_temperature_range_(0))/1000;
			const double t1 = fit_temperature_range_(1)-dt;
			const double t2 = fit_temperature_range_(1);
			const double s1 = calc_property(t1);
			const double s2 = calc_property(t2);
			value = s2 + (temperature - fit_temperature_range_(1))*((s2-s1)/(t2-t1));
		}

		// fix value when below range
		else if(temperature<fit_temperature_range_(0)){
			value = calc_property(fit_temperature_range_(0));
		}

		// calculate normally
		else{
			value = 0; double Tpow = 1.0;
			for(arma::uword i=0;i<fit_parameters_.n_elem;i++){
				value += fit_parameters_(fit_parameters_.n_elem-1-i)*Tpow; Tpow *= Tpow;
			}
		}

		// return answer
		return value;
	}

	// get type
	std::string PolyFit::get_type(){
		return "rat::mat::polyfit";
	}

	// method for serialization into json
	void PolyFit::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();

		// create alphabet
		const std::string alphabet("abcdefghijklmnopqrstuvwxyz");

		// fit parameters
		arma::uword cnt = 0;
		for(auto it=alphabet.begin();it!=alphabet.end() && cnt<fit_parameters_.n_elem;it++,cnt++)
			js[(*it)] = fit_parameters_(cnt);

		// temperature range
		js["t1"] = fit_temperature_range_(0);
		js["t2"] = fit_temperature_range_(1);
	}

	// method for deserialisation from json
	void PolyFit::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){

		// create alphabet
		const std::string alphabet("abcdefghijklmnopqrstuvwxyz");

		// count number of parameters
		arma::uword cnt = 0;
		for(auto it=alphabet.begin();it!=alphabet.end() && cnt<fit_parameters_.n_elem;it++,cnt++)
			if(!js[(*it)].isNumeric())break;

		// allocate
		fit_parameters_.set_size(cnt);

		// retreive values
		cnt = 0;
		for(auto it=alphabet.begin();it!=alphabet.end() && cnt<fit_parameters_.n_elem;it++,cnt++)
			fit_parameters_(cnt) = js[(*it)].asDouble();

		// temperature range
		fit_temperature_range_(0) = js["t1"].asDouble();
		fit_temperature_range_(1) = js["t2"].asDouble();
	}

}}