/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "wflfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	WFLFit::WFLFit(){

	}

	WFLFit::WFLFit(ShFitPr krho_fit){
		set_fit(krho_fit);
	}

	// factory
	ShWFLFitPr WFLFit::create(){
		return std::make_shared<WFLFit>();
	}

	// factory from file
	ShWFLFitPr WFLFit::create(ShFitPr krho_fit){
		return std::make_shared<WFLFit>(krho_fit);
	}

	// copy constructor
	cmn::ShNodePr WFLFit::copy() const{
		return WFLFit::create(krho_fit_->copy_covariant<Fit>());
	}

	// set thermal conductivity fit
	void WFLFit::set_fit(ShFitPr krho_fit){
		assert(krho_fit!=NULL);
		krho_fit_ = krho_fit;
	}

	// fit functions for vector
	double WFLFit::calc_property(
		const double temperature, 
		const double magnetic_field_magnitude) const{
		
		// limit temperature
		const double T = std::max(temperature,1.0);

		// calculate thermal conductivity
		const double krho = krho_fit_->calc_property(
			temperature, magnetic_field_magnitude);

		// Wiedemann Franz Law
		const double value = lorentz_number_*T/krho;

		// rurn resistivity
		return value;
	}

	// fit functions for vector
	arma::Col<double> WFLFit::calc_property(
		const arma::Col<double> &temperature, 
		const arma::Col<double> &magnetic_field_magnitude) const{

		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);
		assert(temperature.is_finite());
		assert(magnetic_field_magnitude.is_finite());
		
		// limit temperature
		const arma::Col<double> T = arma::clamp(temperature,1.0,arma::datum::inf);

		// calculate thermal conductivity
		const arma::Col<double> krho = krho_fit_->calc_property(
			temperature, magnetic_field_magnitude);

		// Wiedemann Franz Law
		const arma::Col<double> value = lorentz_number_*T/krho;

		// check output
		assert(value.n_elem==temperature.n_elem);
		assert(value.is_finite());
		assert(arma::all(value>0));

		// rurn resistivity
		return value;
	}

	// serialization type
	std::string WFLFit::get_type(){
		return "rat::mat::wflfit";
	}

	// serialization
	void WFLFit::serialize(Json::Value &js, cmn::SList &list) const{
		js["type"] = get_type();
		js["krho_fit"] = cmn::Node::serialize_node(krho_fit_, list);
	}

	// deserialization
	void WFLFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		set_fit(cmn::Node::deserialize_node<Fit>(js["krho_fit"], list, factory_list, pth));
	}

}}