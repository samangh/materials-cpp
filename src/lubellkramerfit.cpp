/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "lubellkramerfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	LubellKramerFit::LubellKramerFit(){

	}

	// factory
	ShLubellKramerFitPr LubellKramerFit::create(){
		return std::make_shared<LubellKramerFit>();
	}

	// copy constructor
	cmn::ShNodePr LubellKramerFit::copy() const{
		return std::make_shared<LubellKramerFit>(*this);
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	double LubellKramerFit::calc_property(
		const double temperature,
		const double magnetic_field_magnitude) const{

		// copy input and limit
		const double temperature_lim = std::max(0.0,temperature);
		const double mgn_field_lim = std::max(0.1,magnetic_field_magnitude);

		// calculate scaled values
		const double t = temperature_lim/Tc0_;
		const double Bc2 = Bc20_*(1.0-std::pow(t,n_)); // Lubell's equation
		const double b = mgn_field_lim/Bc2;

		// bottura's equation
		double critical_current_density = 
			1e6*Jref_*(C0_/mgn_field_lim)*std::pow(b,alpha_)*
			std::pow(1.0-b,beta_)*std::pow(1.0-std::pow(t,n_),gamma_);

		// do not allow critical currents lower than zero
		if(mgn_field_lim>Bc2)critical_current_density = 0;
		if(mgn_field_lim==0)critical_current_density = arma::datum::inf;

		// return critical current
		return critical_current_density; // A/m^2
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<double> LubellKramerFit::calc_property(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude) const{

		// copy B
		const arma::Col<double> mgn_field_lim = arma::clamp(
			magnetic_field_magnitude, 0.1, arma::datum::inf);
		const arma::Col<double> temperature_lim = arma::clamp(
			temperature,0,arma::datum::inf);

		// calculate scaled values
		const arma::Col<double> t = temperature_lim/Tc0_;
		const arma::Col<double> Bc2 = Bc20_*(1.0-arma::pow(t,n_)); // Lubell's equation
		const arma::Col<double> b = mgn_field_lim/Bc2;

		// bottura's equation
		arma::Col<double> critical_current_density = 
			1e6*Jref_*(C0_/mgn_field_lim)%arma::pow(b,alpha_)%
			arma::pow(1.0-b,beta_)%arma::pow(1.0-arma::pow(t,n_),gamma_);

		// do not allow critical currents lower than zero
		critical_current_density(arma::find(mgn_field_lim>Bc2)).fill(0);
		critical_current_density(arma::find(mgn_field_lim==0)).fill(arma::datum::inf);

		// return critical current
		return critical_current_density; // A/m^2
	}

	// get type
	std::string LubellKramerFit::get_type(){
		return "rat::mat::lubellkramerfit";
	}

	// method for serialization into json
	void LubellKramerFit::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		js["C0"] = C0_;
		js["alpha"] = alpha_;
		js["beta"] = beta_;
		js["gamma"] = gamma_;
		js["Bc20"] = Bc20_;
		js["Tc0"] = Tc0_;
		js["Jref"] = Jref_;
	}

	// method for deserialisation from json
	void LubellKramerFit::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){

		C0_ = js["C0"].asDouble();
		alpha_ = js["alpha"].asDouble();
		beta_ = js["beta"].asDouble();
		gamma_ = js["gamma"].asDouble();
		Bc20_ = js["Bc20"].asDouble();
		Tc0_ = js["Tc0"].asDouble();
		Jref_ = js["Jref"].asDouble();
	}

}}