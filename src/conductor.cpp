/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "conductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// volumetric specific heat [J m^-3 K^-1]
	double Conductor::calc_volumetric_specific_heat(
		const double temperature) const{

		// calculate density and specific heat
		const double d = calc_density(temperature); // kg m^-3
		const double sh = calc_specific_heat(temperature); // J kg^-1 K^-1
		
		// muliply to convert to volumetric
		return d*sh;
	}

	// volumetric specific heat [J m^-3 K^-1]
	arma::Col<double> Conductor::calc_volumetric_specific_heat(
		const arma::Col<double> &temperature) const{

		// calculate density and specific heat
		const arma::Col<double> d = calc_density(temperature); // kg m^-3
		const arma::Col<double> sh = calc_specific_heat(temperature); // J kg^-1 K^-1

		// muliply to convert to volumetric
		return d%sh;
	}

	// accumulation method
	void Conductor::accu_parallel_conductors(
		FracConPrList &parallel_conductors) const{
		parallel_conductors.push_back(FracConPr(1.0, this));
	}

	// accumulation method for series conductors
	void Conductor::accu_series_conductors(
		FracConPrList &series_conductors) const{
		series_conductors.push_back(FracConPr(1.0, this));
	}

	// calculate percentage on the load-line
	// assuming fixed magnetic field angle
	arma::Col<double> Conductor::calc_percent_load(
		const arma::Col<double> &current_density,
		const arma::Col<double> &temperature, 
		const arma::Col<double> &magnetic_field_magnitude, 
		const arma::Col<double> &magnetic_field_angle,
		const double current_density_increment,
		const double tol_bisection) const{

		// get number of elements
		const arma::uword num_elements = current_density.n_elem;

		// slope of load line [T/A]
		const arma::Col<double> load_line_slope = 
			magnetic_field_magnitude/arma::abs(current_density);

		// find upper critical field
		arma::Col<double> upper_current_density(num_elements, arma::fill::zeros);

		// search roughly the critical surface
		for(;;){
			// calculate critical current in [A/m^2]
			const arma::Col<double> critical_current_density = calc_critical_current_density(
				temperature, load_line_slope%upper_current_density, magnetic_field_angle);

			// find indexes of elements below Jc
			const arma::Col<arma::uword> idx = arma::find(
				critical_current_density>upper_current_density);
			
			// check if any left
			if(idx.is_empty())break;

			// increase critical temperature
			upper_current_density(idx) += current_density_increment;
		}

		// find current density below critical current
		arma::Col<double> lower_current_density = upper_current_density - current_density_increment;

		// bisection algorithm
		for(;;){
			// calculate average
			const arma::Col<double> intermediate_current_density = 
				(upper_current_density + lower_current_density)/2;

			// calculate critical current in [A/m^2]
			const arma::Col<double> crit_current_density = calc_critical_current_density(
				temperature, load_line_slope%intermediate_current_density, magnetic_field_angle);

			// find indexes for which to shift upper/lower temperature
			const arma::Col<arma::uword> idx1 = arma::find(
				crit_current_density<intermediate_current_density);
			const arma::Col<arma::uword> idx2 = arma::find(
				crit_current_density>=intermediate_current_density);

			// update temperatures
			upper_current_density(idx1) = intermediate_current_density(idx1);
			lower_current_density(idx2) = intermediate_current_density(idx2);

			// stop condiction
			if(arma::all(arma::abs(lower_current_density - 
				upper_current_density)<tol_bisection))break;
		}

		// calculate percentage on loadline
		return 100*arma::abs(current_density)/upper_current_density;
	}


	// default critical current is zero (not superconducting)
	double Conductor::calc_cs_temperature(
		const double current_density, 
		const double magnetic_field_magnitude, 
		const double magnetic_field_angle,
		const double temperature_increment,
		const double tol_bisection) const{

		// allocate critical temperature
		double upper_crit_temperature = temperature_increment;

		// clamp current density
		const double J = std::max(std::abs(current_density),1e-6);

		// check if it is a superconductor
		const double Jc0 = calc_critical_current_density(
			0, magnetic_field_magnitude, magnetic_field_angle);
		if(Jc0==0)return 0;

		// iterations
		for(;;){
			// calculate critical current in [A/m^2]
			const double critical_current_density = calc_critical_current_density(
				upper_crit_temperature, magnetic_field_magnitude, magnetic_field_angle);

			// increment temperature 
			if(critical_current_density>J)
				upper_crit_temperature += temperature_increment;

			// until no margin left
			else break;
		}

		// remove last increment
		double lower_crit_temperature = 
			upper_crit_temperature - temperature_increment;

		// bisection algorithm
		for(;;){
			// calculate average
			const double intermediate_temperature = 
				(upper_crit_temperature + lower_crit_temperature)/2;

			// calculate critical current in [A/m^2]
			const double crit_current_density = calc_critical_current_density(
				intermediate_temperature, magnetic_field_magnitude, magnetic_field_angle);

			// update temperatures
			if(crit_current_density<J)
				upper_crit_temperature = intermediate_temperature;
			else lower_crit_temperature = intermediate_temperature;

			// stop condiction
			if(std::abs(lower_crit_temperature - 
				upper_crit_temperature)<tol_bisection)break;
		}

		// return critical temp
		return upper_crit_temperature;
	}


	// calculate current sharing temperature
	arma::Col<double> Conductor::calc_cs_temperature(
		const arma::Col<double> &current_density, 
		const arma::Col<double> &magnetic_field_magnitude, 
		const arma::Col<double> &magnetic_field_angle,
		const double temperature_increment,
		const double tol_bisection) const{

		// clamp current density
		const arma::Col<double> J = arma::clamp(arma::abs(current_density),1e-6,arma::datum::inf);

		// allocate critical temperature
		arma::Col<double> upper_crit_temperature(J.n_elem); 
		upper_crit_temperature.fill(temperature_increment);

		// iterations
		for(;;){
			// calculate critical current in [A/m^2]
			const arma::Col<double> crit_current_density = calc_critical_current_density(
				upper_crit_temperature, magnetic_field_magnitude, magnetic_field_angle);

			// find indexes of elements below Jc
			const arma::Col<arma::uword> idx = arma::find(
				crit_current_density>arma::abs(J));
			
			// check if any left
			if(idx.is_empty())break;

			// increase critical temperature
			upper_crit_temperature(idx) += temperature_increment;
		}

		// remove last increment
		arma::Col<double> lower_crit_temperature = 
			upper_crit_temperature - temperature_increment;

		// bisection algorithm
		for(;;){
			// calculate average
			const arma::Col<double> intermediate_temperature = 
				(upper_crit_temperature + lower_crit_temperature)/2;

			// calculate critical current in [A/m^2]
			const arma::Col<double> crit_current_density = calc_critical_current_density(
				intermediate_temperature, magnetic_field_magnitude, magnetic_field_angle);

			// find indexes for which to shift upper/lower temperature
			const arma::Col<arma::uword> idx1 = arma::find(
				crit_current_density<arma::abs(J));
			const arma::Col<arma::uword> idx2 = arma::find(
				crit_current_density>=arma::abs(J));

			// update temperatures
			upper_crit_temperature(idx1) = intermediate_temperature(idx1);
			lower_crit_temperature(idx2) = intermediate_temperature(idx2);

			// stop condiction
			if(arma::all(arma::abs(lower_crit_temperature - 
				upper_crit_temperature)<tol_bisection))break;
		}

		// return critical temp
		return upper_crit_temperature;
	}



	// analysis tools for scalar input
	// calcultae temperature increase due to added heat
	double Conductor::calc_temperature_increase(
		const double initial_temperature, 
		const double volume, 
		const double added_heat,
		const double delta_temperature) const{

		// check no heat case
		if(added_heat==0)return 0;

		// set temperature
		double final_temperature = initial_temperature;
		double vol_heat_remain = added_heat/volume; // [J m^-3]

		// perform integration
		for(;;){
			// calculate specific heat
			const double vsh = calc_volumetric_specific_heat(
				final_temperature + delta_temperature/2);

			// normal temperature step
			if(vsh*delta_temperature < vol_heat_remain){
				final_temperature += delta_temperature;
				vol_heat_remain -= vsh*delta_temperature;
			}

			// final temperature step
			else{
				const double dT = vol_heat_remain/vsh;
				final_temperature += dT;
				vol_heat_remain -= vsh*dT;
				break;
			}
		}

		// return temperature
		return final_temperature - initial_temperature;
	}

	// calculate the energy density based on the temperature
	// energy density is given in [J m^-3]
	double Conductor::calc_thermal_energy_density(
		const double temperature, 
		const double delta_temperature) const{

		// calculate number of steps
		const arma::uword num_steps = std::min(std::max((int)(temperature/delta_temperature),2),1000);

		// create temperature array from zero to temperature
		const arma::Col<double> tt = arma::linspace<arma::Col<double> >(0.0,temperature,num_steps);
		
		// calculate specific heat [J m^-3 K^-1]
		const arma::Col<double> vsh = calc_volumetric_specific_heat(tt);

		// integrate over temperature to get energy
		return arma::as_scalar(arma::trapz(tt, vsh));
	}

	// calculate the energy density based on the temperature
	// energy density is given in [J m^-3]
	arma::Col<double> Conductor::calc_thermal_energy_density(
		const arma::Col<double> &temperature, 
		const double delta_temperature) const{

		// calculate number of steps
		const arma::uword num_steps = std::min(
			std::max((int)(temperature.max()/delta_temperature),2),1000);

		// create temperature array from zero to temperature
		const arma::Col<double> tt = arma::linspace<arma::Col<double> >(0,temperature.max(),num_steps);
		
		// calculate specific heat [J m^-3 K^-1]
		const arma::Col<double> vsh = calc_volumetric_specific_heat(tt);

		// cumtrapz
		const arma::Col<double> edi = arma::join_vert(arma::Col<double>{0},
			arma::cumsum(arma::diff(tt)%(vsh.head_rows(num_steps-1) + vsh.tail_rows(num_steps-1))/2));

		// interpolate
		arma::Col<double> energy_density;
		rat::cmn::Extra::lininterp1f(tt,edi,temperature,energy_density,true);

		// integrate over temperature to get energy
		return energy_density;
	}

	// calculate the length of the minimal propagation zone in [m]
	double Conductor::calc_theoretical_lmpz(
		const double current_density,
		const double operating_temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{
		
		// // calculate the critical current 
		// const double jc = calc_critical_current_density(
		// 	operating_temperature, magnetic_field_magnitude, magnetic_field_angle);

		// // check if it is a superconductor
		// if(jc==0)return 0.0;

		// current sharing temperature
		const double cs_temperature = calc_cs_temperature(
			current_density, magnetic_field_magnitude, magnetic_field_angle);

		// critical temperature (at zero current)
		const double crit_temperature = calc_cs_temperature(
			0, magnetic_field_magnitude, magnetic_field_angle);

		// transition temperature between Tc and Tcs
		const double transition_temperature = (cs_temperature + crit_temperature)/2;

		// check if it is a superconductor
		if(transition_temperature<operating_temperature)return 0.0;

		// material temperature
		const double nzp_temperature = (transition_temperature + operating_temperature)/2;


		// calculate resistivity
		const double rho = calc_electrical_resistivity(
			nzp_temperature, magnetic_field_magnitude);

		// calculate thermal conductivity
		const double k = calc_thermal_conductivity(
			nzp_temperature, magnetic_field_magnitude);

		// calculate length of the minimal propagation zone
		const double lmpz = std::sqrt(2*k*(transition_temperature - 
			operating_temperature)/(current_density*current_density*rho));

		// return length
		return lmpz;
	}

	// calculate minimal quench energy in [J]
	double Conductor::calc_theoretical_mqe(
		const double current_density,
		const double operating_temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle,
		const double cross_sectional_area) const{

		// // calculate the critical current 
		// const double jc = calc_critical_current_density(
		// 	operating_temperature, magnetic_field_magnitude, magnetic_field_angle);

		// // check if it is a superconductor
		// if(jc==0)return 0.0;

		// current sharing temperature
		const double cs_temperature = calc_cs_temperature(
			current_density, magnetic_field_magnitude, magnetic_field_angle);

		// critical temperature (at zero current)
		const double crit_temperature = calc_cs_temperature(
			0.0, magnetic_field_magnitude, magnetic_field_angle);

		// transition temperature between Tc and Tcs
		const double transition_temperature = (cs_temperature + crit_temperature)/2;

		// check if it is a superconductor
		if(transition_temperature<operating_temperature)return 0.0;

		// material temperature
		const double nzp_temperature = (transition_temperature + operating_temperature)/2;

		// calculate resistivity
		const double rho = calc_electrical_resistivity(
			nzp_temperature, magnetic_field_magnitude);

		// calculate thermal conductivity
		const double k = calc_thermal_conductivity(
			nzp_temperature, magnetic_field_magnitude);

		// calculate length of the minimal propagation zone
		const double lmpz = std::sqrt(2*k*(transition_temperature - 
			operating_temperature)/(current_density*current_density*rho));

		// create temperature range for integration
		const arma::Col<double>::fixed<2> temperature_range = {operating_temperature,transition_temperature};

		// calculate MQE
		const double mqe = lmpz*cross_sectional_area*arma::as_scalar(
			arma::diff(calc_thermal_energy_density(temperature_range)));

		// return length
		return mqe;
	}

	// calculate the normal zone propagation velocity in [m s^-1]
	double Conductor::calc_theoretical_vnzp(
		const double current_density,
		const double operating_temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{
		
		// calculate the critical current 
		// const double jc = calc_critical_current_density(
		// 	operating_temperature, magnetic_field_magnitude, magnetic_field_angle);

		// current sharing temperature
		const double cs_temperature = calc_cs_temperature(
			current_density, magnetic_field_magnitude, magnetic_field_angle);

		// critical temperature (at zero current)
		const double crit_temperature = calc_cs_temperature(
			0, magnetic_field_magnitude, magnetic_field_angle);

		// transition temperature between Tc and Tcs
		double ff = 0.5;
		const double transition_temperature = ff*cs_temperature + (1.0-ff)*crit_temperature;

		// check if it is a superconductor
		if(transition_temperature<operating_temperature)return 0.0;

		// material temperature
		const double nzp_temperature = (transition_temperature + operating_temperature)/2;

		// calculate resistivity [Ohm m] = [W A^-2 m]
		const double rho = calc_electrical_resistivity(
			nzp_temperature, magnetic_field_magnitude);

		// calculate thermal conductivity [J s^-1 m^-1 K^-1]
		const double k = calc_thermal_conductivity(
			nzp_temperature, magnetic_field_magnitude);

		// claculate heat capacity [J m^-3 K^-1]
		const double sh = arma::as_scalar(arma::mean(calc_volumetric_specific_heat(
			arma::linspace(operating_temperature, transition_temperature))));

		// normal zone velocity [m/s]
		const double vnzp = (std::abs(current_density)/sh)*
			std::sqrt((rho*k)/(transition_temperature-operating_temperature));

		// return velocity
		return vnzp;
	}

	// calculate time for adiabatic conductor 
	// to reach specified temperature
	// superconductors are assumed to start at Tc
	double Conductor::calc_adiabatic_time(
		const double current_density,
		const double operating_temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle,
		const double max_temperature,
		const double max_temperature_step) const{

		// check 
		if(max_temperature<operating_temperature)
			return 0;

		// critical temperature (at zero current)
		const double crit_temperature = calc_cs_temperature(
			0, magnetic_field_magnitude, magnetic_field_angle);

		// set start condition
		double temperature = std::max(operating_temperature, crit_temperature);
		double time = 0;

		// integrate
		while(temperature<max_temperature){
			// calculate electric field
			const double electric_field = calc_electric_field(
				current_density, temperature, 
				magnetic_field_magnitude, magnetic_field_angle); // [V/m]

			// calculate heat capacity
			const double volumetric_specific_heat = calc_volumetric_specific_heat(temperature); //[J/m^3/K]

			// calculate power density [J/s/m^3]
			const double power_density = electric_field*current_density; 

			// calculate change [K/s]
			const double dtemperature_dt = power_density/volumetric_specific_heat;

			// calculate required temperature step
			const double temperature_step = std::min(
				max_temperature_step, max_temperature-temperature);
			
			// take maximum temperature step
			const double dt = temperature_step/dtemperature_dt;
			time += dt; temperature += dt*dtemperature_dt;
		}

		// return time
		return time;
	}

	// calculate partial derivative of electric field 
	// with temperature using finite difference method
	arma::Col<double> Conductor::calc_electric_field_dT(
		const arma::Col<double> &current_density,
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle,
		const double delta_temperature) const{

		// calculate electric field
		const arma::Col<double> electric_field1 = calc_electric_field(
			current_density, 
			temperature-delta_temperature/2, 
			magnetic_field_magnitude,
			magnetic_field_angle); // [V m^-1]

		// calculate electric field
		const arma::Col<double> electric_field2 = calc_electric_field(
			current_density, 
			temperature+delta_temperature/2, 
			magnetic_field_magnitude,
			magnetic_field_angle); // [V m^-1]

		// finite difference
		return (electric_field2 - electric_field1)/delta_temperature;
	}

	// calculate partial derivative of electric field 
	// with temperature using finite difference method
	double Conductor::calc_electric_field_dT(
		const double current_density,
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle,
		const double delta_temperature) const{

		// calculate electric field
		const double electric_field1 = calc_electric_field(
			current_density, 
			temperature-delta_temperature/2, 
			magnetic_field_magnitude,
			magnetic_field_angle); // [V m^-1]

		// calculate electric field
		const double electric_field2 = calc_electric_field(
			current_density, 
			temperature+delta_temperature/2, 
			magnetic_field_magnitude,
			magnetic_field_angle); // [V m^-1]

		// finite difference
		return (electric_field2 - electric_field1)/delta_temperature;
	}


	// function for calculating section averaged properties
	arma::Mat<double> Conductor::nodes2section(
		const arma::Mat<double> &node_values,
		const arma::Mat<arma::uword> &nelements,
		const arma::Row<double> &cross_areas){

		// get counters
		const arma::uword num_nodes_total = node_values.n_rows;
		const arma::uword num_nodes_cross = nelements.max() + 1;
		const arma::uword num_elements = nelements.n_cols;

		// check if the number of elements matches that of the data
		if(num_nodes_total%num_nodes_cross!=0)rat_throw_line("nodes can not be divided over integer number of cross sections");
		const arma::uword num_sections = num_nodes_total/num_nodes_cross; // number of cross sections

		// calculate areas
		const double total_area = arma::accu(cross_areas);

		// allocate for cross sections
		arma::Mat<double> cross_values(num_sections, node_values.n_cols, arma::fill::zeros);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// extract section J at nodes
			const arma::Mat<double> cross_values_nodes = 
				node_values.rows(i*num_nodes_cross, (i+1)*num_nodes_cross-1);

			// walk over elements in section
			for(arma::uword j=0;j<num_elements;j++){
				cross_values.row(i) += (cross_areas(j)/total_area)*
					arma::mean(cross_values_nodes.rows(nelements.col(j)),0);
			}
		}

		// return section values
		return cross_values;
	}

	// function for interpolating sections to nodes
	// function for calculating section averaged properties
	arma::Mat<double> Conductor::section2nodes(
		const arma::Mat<double> &section_values,
		const arma::Mat<arma::uword> &nelements,
		const arma::Row<double> &/*cross_areas*/){
		
		// get counters
		const arma::uword num_nodes_cross = nelements.max() + 1;
		const arma::uword num_sections = section_values.n_rows;
		const arma::uword num_nodes_total = num_nodes_cross*num_sections;
		// const arma::uword num_elements = nelements.n_cols;

		// send to nodes
		arma::Mat<double> node_values(num_nodes_total, section_values.n_cols);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++)
			node_values.rows(i*num_nodes_cross, (i+1)*num_nodes_cross-1).fill(section_values(i));

		// return node values
		return node_values;
	}

	// critical current density averaged over cross section [J m^-2]
	arma::Col<double> Conductor::calc_critical_current_density_av(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle,
		const arma::Mat<arma::uword> &nelements,
		const arma::Row<double> &cross_areas) const{

		// calculate critical current normally
		const arma::Col<double> jc = calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle);

		// divide over cross section
		return section2nodes(
			nodes2section(jc, nelements, cross_areas), 
			nelements, cross_areas);
	}

	// critical temperature calculation in [A m^-2]
	// using averaged critical current over cross area
	arma::Col<double> Conductor::calc_cs_temperature_av(
		const arma::Col<double> &current_density, 
		const arma::Col<double> &magnetic_field_magnitude, 
		const arma::Col<double> &magnetic_field_angle,
		const arma::Mat<arma::uword> &nelements,
		const arma::Row<double> &cross_areas,
		const double temperature_increment,
		const double tol_bisection) const{

		// clamp current density
		const arma::Col<double> J = arma::clamp(arma::abs(current_density),1e-6,arma::datum::inf);

		// allocate critical temperature
		arma::Col<double> upper_crit_temperature(J.n_elem); 
		upper_crit_temperature.fill(temperature_increment);

		// iterations
		for(;;){
			// calculate critical current in [A/m^2]
			const arma::Col<double> crit_current_density = calc_critical_current_density_av(
				upper_crit_temperature, magnetic_field_magnitude,
				magnetic_field_angle, nelements, cross_areas);

			// find indexes of elements below Jc
			const arma::Row<arma::uword> idx = arma::find(
				crit_current_density>arma::abs(J)).t();
			
			// check if any left
			if(idx.is_empty())break;

			// increase critical temperature
			upper_crit_temperature(idx) += temperature_increment;
		}

		// remove last increment
		arma::Col<double> lower_crit_temperature = 
			upper_crit_temperature - temperature_increment;

		// bisection algorithm
		for(;;){
			// calculate average
			const arma::Col<double> intermediate_temperature = 
				(upper_crit_temperature + lower_crit_temperature)/2;

			// calculate critical current in [A/m^2]
			const arma::Col<double> crit_current_density = calc_critical_current_density_av(
				intermediate_temperature, magnetic_field_magnitude, 
				magnetic_field_angle, nelements, cross_areas);

			// find indexes for which to shift upper/lower temperature
			const arma::Col<arma::uword> idx1 = arma::find(
				crit_current_density<arma::abs(J));
			const arma::Col<arma::uword> idx2 = arma::find(
				crit_current_density>=arma::abs(J));

			// update temperatures
			upper_crit_temperature(idx1) = intermediate_temperature(idx1);
			lower_crit_temperature(idx2) = intermediate_temperature(idx2);

			// stop condiction
			if(arma::all(arma::abs(lower_crit_temperature - 
				upper_crit_temperature)<tol_bisection))break;
		}

		// return critical temp
		return upper_crit_temperature;
	}

	// matrix input wrappers
	// calculate specific heat [J kg^-1 K^-1]
	arma::Mat<double> Conductor::calc_specific_heat_mat(
		const arma::Mat<double> &temperature) const{
		const arma::Col<double> sh = calc_specific_heat(
			arma::vectorise(temperature));
		assert(sh.n_elem==temperature.n_elem);
		return arma::reshape(sh, temperature.n_rows,temperature.n_cols);
	}
	
	// thermal conductivity [W m^-1 K^-1]
	arma::Mat<double> Conductor::calc_thermal_conductivity_mat(
		const arma::Mat<double> &temperature,
		const arma::Mat<double> &magnetic_field_magnitude) const{
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		const arma::Col<double> k = calc_thermal_conductivity(
			arma::vectorise(temperature), 
			arma::vectorise(magnetic_field_magnitude));
		

		// convert to matrix and return
		assert(k.n_elem==temperature.n_elem);
		return arma::reshape(k,	temperature.n_rows, temperature.n_cols);
	} 
	
	// electrical resistivity [Ohm m]
	arma::Mat<double> Conductor::calc_electrical_resistivity_mat(
		const arma::Mat<double> &temperature,
		const arma::Mat<double> &magnetic_field_magnitude) const{
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		const arma::Col<double> rho = calc_electrical_resistivity(
			arma::vectorise(temperature), 
			arma::vectorise(magnetic_field_magnitude));
		

		// convert to matrix and return
		assert(rho.n_elem==temperature.n_elem);
		return arma::reshape(rho, temperature.n_rows,temperature.n_cols);
	}

	// critical current density [J m^-2]
	arma::Mat<double> Conductor::calc_critical_current_density_mat(
		const arma::Mat<double> &temperature,
		const arma::Mat<double> &magnetic_field_magnitude,
		const arma::Mat<double> &magnetic_field_angle) const{
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		assert(temperature.n_cols==magnetic_field_angle.n_cols);
		assert(temperature.n_rows==magnetic_field_angle.n_rows);
		const arma::Col<double> jc = calc_critical_current_density(
			arma::vectorise(temperature), 
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle));
		

		// convert to matrix and return
		assert(jc.n_elem == temperature.n_elem);
		return arma::reshape(jc, temperature.n_rows,temperature.n_cols);
	}

	// critical current density [J m^-2]
	arma::Mat<double> Conductor::calc_nvalue_mat(
		const arma::Mat<double> &temperature,
		const arma::Mat<double> &magnetic_field_magnitude,
		const arma::Mat<double> &magnetic_field_angle) const{
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		assert(temperature.n_cols==magnetic_field_angle.n_cols);
		assert(temperature.n_rows==magnetic_field_angle.n_rows);
		const arma::Col<double> N = calc_nvalue(
			arma::vectorise(temperature), 
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle));

		// convert to matrix and return
		assert(N.n_elem == temperature.n_elem);
		return arma::reshape(N, temperature.n_rows,temperature.n_cols);
	}

	// material density [kg m^-3]
	arma::Mat<double> Conductor::calc_density_mat(
		const arma::Mat<double> &temperature) const{
		const arma::Col<double> d = calc_density(
			arma::vectorise(temperature));
		assert(d.n_elem==temperature.n_elem);

		// convert to matrix and return
		return arma::reshape(d, temperature.n_rows,temperature.n_cols);
	}

	// volumetric specific heat [J m^-3 K^-1]
	arma::Mat<double> Conductor::calc_volumetric_specific_heat_mat(
		const arma::Mat<double> &temperature) const{
		const arma::Col<double> vsh = calc_volumetric_specific_heat(
			arma::vectorise(temperature));

		// convert to matrix and return
		assert(vsh.n_elem==temperature.n_elem);
		return arma::reshape(vsh, temperature.n_rows,temperature.n_cols);
	}

	// electric field [V m^-1]
	arma::Mat<double> Conductor::calc_electric_field_mat(
		const arma::Mat<double> &current_density,
		const arma::Mat<double> &temperature,
		const arma::Mat<double> &magnetic_field_magnitude,
		const arma::Mat<double> &magnetic_field_angle) const{
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		assert(temperature.n_cols==magnetic_field_angle.n_cols);
		assert(temperature.n_rows==magnetic_field_angle.n_rows);
		assert(temperature.n_cols==current_density.n_cols);
		assert(temperature.n_rows==current_density.n_rows);
		const arma::Col<double> E = calc_electric_field(
			arma::vectorise(current_density),
			arma::vectorise(temperature), 
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle));
		

		// convert to matrix and return
		assert(E.n_elem==temperature.n_elem);
		return arma::reshape(E, temperature.n_rows,temperature.n_cols);
	}

	// calculate current density [A m^-2]
	arma::Mat<double> Conductor::calc_current_density_mat(
		const arma::Mat<double> &electric_field,
		const arma::Mat<double> &temperature,
		const arma::Mat<double> &magnetic_field_magnitude,
		const arma::Mat<double> &magnetic_field_angle) const{
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		assert(temperature.n_cols==magnetic_field_angle.n_cols);
		assert(temperature.n_rows==magnetic_field_angle.n_rows);
		assert(temperature.n_cols==electric_field.n_cols);
		assert(temperature.n_rows==electric_field.n_rows);
		const arma::Col<double> J = calc_current_density(
			arma::vectorise(electric_field),
			arma::vectorise(temperature), 
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle));

		// convert to matrix and return
		assert(J.n_elem==temperature.n_elem);
		return arma::reshape(J, temperature.n_rows,temperature.n_cols);
	}

	// critical temperature calculation in [A m^-2]
	arma::Mat<double> Conductor::calc_cs_temperature_mat(
		const arma::Mat<double> &current_density, 
		const arma::Mat<double> &magnetic_field_magnitude, 
		const arma::Mat<double> &magnetic_field_angle,
		const double temperature_increment,
		const double tol_bisection) const{

		assert(current_density.n_cols==magnetic_field_magnitude.n_cols);
		assert(current_density.n_rows==magnetic_field_magnitude.n_rows);
		assert(current_density.n_cols==magnetic_field_angle.n_cols);
		assert(current_density.n_rows==magnetic_field_angle.n_rows);
		const arma::Col<double> Tcs = calc_cs_temperature(
			arma::vectorise(current_density),
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			temperature_increment, tol_bisection);

		// convert to matrix and return
		assert(Tcs.n_elem==current_density.n_elem);
		return arma::reshape(Tcs, current_density.n_rows,current_density.n_cols);
	}

	// critical current density averaged over cross section [J m^-2]
	arma::Mat<double> Conductor::calc_critical_current_density_av_mat(
		const arma::Mat<double> &temperature,
		const arma::Mat<double> &magnetic_field_magnitude,
		const arma::Mat<double> &magnetic_field_angle,
		const arma::Mat<arma::uword> &nelements,
		const arma::Row<double> &cross_areas) const{

		// check input
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		assert(temperature.n_cols==magnetic_field_angle.n_cols);
		assert(temperature.n_rows==magnetic_field_angle.n_rows);
		assert(nelements.n_cols==cross_areas.n_elem);

		// calculate averaged critical current density
		const arma::Col<double> Jc = calc_critical_current_density_av(
			arma::vectorise(temperature),
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle), 
			nelements, cross_areas);

		// convert to matrix and return
		assert(Jc.n_elem==temperature.n_elem);
		return arma::reshape(Jc, temperature.n_rows, temperature.n_cols);
	}

	// electric field [V m^-1] section averaged
	// required to supply the cross-sectional areas of the
	// elements making up a section in the same order as
	// the other properties
	arma::Mat<double> Conductor::calc_electric_field_av_mat(
		const arma::Mat<double> &current_density,
		const arma::Mat<double> &temperature,
		const arma::Mat<double> &magnetic_field_magnitude,
		const arma::Mat<double> &magnetic_field_angle,
		const arma::Mat<arma::uword> &nelements,
		const arma::Row<double> &cross_areas) const{

		// check input
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		assert(temperature.n_cols==magnetic_field_angle.n_cols);
		assert(temperature.n_rows==magnetic_field_angle.n_rows);
		assert(temperature.n_cols==current_density.n_cols);
		assert(temperature.n_rows==current_density.n_rows);
		
		// calculate electric field
		const arma::Col<double> E = calc_electric_field_av(
			arma::vectorise(current_density),
			arma::vectorise(temperature),
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			nelements, cross_areas);

		// convert to matrix and return
		assert(E.n_elem==temperature.n_elem);
		return arma::reshape(E, temperature.n_rows, temperature.n_cols);
	}
	
	// critical temperature calculation in [A m^-2]
	// using averaged critical current over cross area
	arma::Mat<double> Conductor::calc_cs_temperature_av_mat(
		const arma::Mat<double> &current_density, 
		const arma::Mat<double> &magnetic_field_magnitude, 
		const arma::Mat<double> &magnetic_field_angle,
		const arma::Mat<arma::uword> &nelements,
		const arma::Row<double> &cross_areas,
		const double temperature_increment,
		const double tol_bisection) const{
		
		// check input
		assert(current_density.n_cols==magnetic_field_magnitude.n_cols);
		assert(current_density.n_rows==magnetic_field_magnitude.n_rows);
		assert(current_density.n_cols==magnetic_field_angle.n_cols);
		assert(current_density.n_rows==magnetic_field_angle.n_rows);

		// calculate current sharing temperature
		const arma::Col<double> Tcs = calc_cs_temperature_av(
			arma::vectorise(current_density),
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			nelements, cross_areas,
			temperature_increment,
			tol_bisection);

		// convert to matrix and return
		assert(Tcs.n_elem==current_density.n_elem);
		return arma::reshape(Tcs, current_density.n_rows, current_density.n_cols);
	}

	// calculate percentage on the load-line
	// assuming fixed magnetic field angle
	arma::Mat<double> Conductor::calc_percent_load_mat(
		const arma::Mat<double> &current_density,
		const arma::Mat<double> &temperature, 
		const arma::Mat<double> &magnetic_field_magnitude, 
		const arma::Mat<double> &magnetic_field_angle,
		const double current_density_increment,
		const double tol_bisection) const{

		// check input
		assert(current_density.n_cols==magnetic_field_magnitude.n_cols);
		assert(current_density.n_rows==magnetic_field_magnitude.n_rows);
		assert(current_density.n_cols==magnetic_field_angle.n_cols);
		assert(current_density.n_rows==magnetic_field_angle.n_rows);
		assert(current_density.n_cols==temperature.n_cols);
		assert(current_density.n_rows==temperature.n_rows);

		// calculate current sharing temperature
		const arma::Col<double> pct_load = calc_percent_load(
			arma::vectorise(current_density),
			arma::vectorise(temperature),
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			current_density_increment, tol_bisection);

		// convert to matrix and return
		return arma::reshape(pct_load, current_density.n_rows, current_density.n_cols);
	}

	// display function
	void Conductor::display_property_table(
		cmn::ShLogPr lg,
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle) const{

		// create for loop
		lg->msg("Material Name: %s\n",get_name().c_str());
		lg->msg("Material Property Table\n");
		const arma::uword table_size = 93;
		lg->msg("%19s | %51s | %17s\n","parameters","fundamental","superconductor");
		lg->msg("%5s %6s %6s | %6s %8s %8s %8s %8s %8s | %8s %8s\n",
			"B","alpha","T","d","cp","delH","df","k","rho","Jc","Nv");
		lg->msg("%5s %6s %6s | %6s %8s %8s %8s %8s %8s | %8s %8s\n",
			"T","deg","K","kg/m^3","J/K/kg","J/kg","m^2/s","W/m/K",
			"Ohm.m","A/m^2","unitless");
		lg->msg("="); for(arma::uword i=0;i<table_size-1;i++)lg->msg(0,"="); lg->msg(0,"\n");
		for(arma::uword j=0;j<magnetic_field_magnitude.n_elem;j++){
			for(arma::uword k=0;k<magnetic_field_angle.n_elem;k++){
				for(arma::uword i=0;i<temperature.n_elem;i++){
					// get parameters
					const double T = temperature(i);
					const double B = magnetic_field_magnitude(j);
					const double alpha = magnetic_field_angle(k);

					// get fundamental properties
					const double d = calc_density(T);
					const double cp = calc_specific_heat(T);
					const double k = calc_thermal_conductivity(T,B);
					const double rho = calc_electrical_resistivity(T,B);
					const double E = calc_thermal_energy_density(T);
					const double df = k/(d*cp); // calculate diffusivity

					// superconductor properties
					const double jc = calc_critical_current_density(T,B,alpha);
					const double nv = calc_nvalue(T,B,alpha);

					// display properties
					lg->msg("%5.2f %6.2f %6.2f | %6.1f %8.2e %8.2e %8.2e %8.2e %8.2e | %8.2e %8.2e\n", 
						B, 360*alpha/(2*arma::datum::pi), T, d, cp, E, df, k, rho, jc, nv);
				}
			}
		}

		// table footer
		lg->msg("="); for(arma::uword i=0;i<table_size-1;i++)lg->msg(0,"="); lg->msg(0,"\n");
	}

}}