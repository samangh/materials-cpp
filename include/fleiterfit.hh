/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_FLEITER_FIT_HH
#define MAT_FLEITER_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class FleiterFit> ShFleiterFitPr;

	// fixed critical current density
	class FleiterFit: public Fit{
		private:
			// typical layer thickness
			double tsc_;

			// general parameters
			double Tc0_; // [K]
			double n_;
			double n1_;
			double n2_;

			// Parameters for ab-plane
			double pab_;
			double qab_;
			double Bi0ab_; // [T]
			double a_;
			double gammaab_;
			double alphaab_; // [AT/m^2]

			// Parameters for c-plane
			double pc_;
			double qc_;
			double Bi0c_; // [T]
			double gammac_;
			double alphac_; // [AT/m^2]

			// Parameters for anisotropy
			double g0_;
			double g1_;
			double g2_;
			double g3_;
			double nu_;
			double pkoff_;

			// type-0 pair flipping
			bool type0_flip_;
			bool ignore_pkoff_;

		public:
			// constructor
			FleiterFit();

			// factory
			static ShFleiterFitPr create();

			// copy function
			cmn::ShNodePr copy() const override;

			// function for calculating critical current density
			// using scalar input and output
			// output is in [A m^-2]
			double calc_property(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// function for calculating critical current density
			// using vector input and output
			// output is in [A m^-2]
			arma::Col<double> calc_property(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif