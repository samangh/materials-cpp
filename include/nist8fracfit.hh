/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_NIST8FRAC_FIT_HH
#define MAT_NIST8FRAC_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class Nist8FracFit> ShNist8FracFitPr;

	// template for materials
	class Nist8FracFit: public Fit{
		// properties
		private:
			// coefficients of fit
			arma::Col<double>::fixed<9> fit_parameters_; // abcdefghi

			// temperature range
			arma::Col<double>::fixed<2> fit_temperature_range_;

		// methods
		public:
			// virtual destructor
			Nist8FracFit();

			// factory
			static ShNist8FracFitPr create();

			// copy function
			cmn::ShNodePr copy() const override;

			// set fit parameters
			void set_fit_parameters(const arma::Col<double> &fit_parameters);
			void set_temperature_range(const double lower_temperature, const double upper_temperature);

			// fit functions
			double calc_property(const double temperature) const override;
			arma::Col<double> calc_property(const arma::Col<double> &temperature) const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif