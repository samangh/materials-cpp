/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_LUBKRA_FIT_HH
#define MAT_LUBKRA_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class LubellKramerFit> ShLubellKramerFitPr;

	// Lubell Kramer Jc Fit
	class LubellKramerFit: public Fit{
		// properties
		private:
			// fitting parameters
			double C0_ = 31.4; // [T]
			double alpha_ = 0.63;
			double beta_ = 1.0;
			double gamma_ = 2.3;
			double n_ = 1.7;

			// materialistic parameters
			double Bc20_ = 14.5; // [T] upper critical magnetic flux at 0K
			double Tc0_ = 9.2; // [K] critical temperature at zero magnetic flux
			double Jref_ = 3000; // [A/mm^2]critical current density at 4.2K and 5T

		public:
			// constructor
			LubellKramerFit();

			// factory
			static ShLubellKramerFitPr create();

			// copy function
			cmn::ShNodePr copy() const override;

			// function for calculating critical current density
			// using scalar input and output
			// output is in [A m^-2]
			double calc_property(
				const double temperature,
				const double magnetic_field_magnitude) const override;

			// function for calculating critical current density
			// using vector input and output
			// output is in [A m^-2]
			arma::Col<double> calc_property(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude) const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif