/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_SPECIFIC_HEAT_FIT_HH
#define MAT_SPECIFIC_HEAT_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class SpecificHeatFit> ShSpecificHeatFitPr;

	// specific heat fit class template
	class SpecificHeatFit: public cmn::Node{
		// methods
		public:
			// virtual destructor
			~SpecificHeatFit(){};

			// function for calculating specific heat
			// using scalar input and output
			// output is in [J kg^-1 K^-1]
			virtual double calc_specific_heat(
				const double temperature) const = 0;

			// function for calculating specific heat
			// using vector input and output
			// output is in [J kg^-1 K^-1]
			virtual arma::Col<double> calc_specific_heat(
				const arma::Col<double> &temperature) const = 0;

			// copy function
			virtual ShSpecificHeatFitPr copy() const = 0;
	};

}}

#endif