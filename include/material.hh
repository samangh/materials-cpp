/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_CONDUCTOR_HH
#define MAT_CONDUCTOR_HH

#include <armadillo>
#include <memory>
#include <cassert>

#include "rat/common/log.hh"
#include "rat/common/nameable.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class Conductor> ShConductorPr;
	typedef arma::field<ShConductorPr> ShConductorPrList;

	// typedef for list containing both fraction and conductor
	typedef std::pair<double, ShConductorPr> FracShConPr;
	typedef std::list<FracShConPr> FracShConPrList;
	typedef std::pair<double, const Conductor*> FracConPr;
	typedef std::list<FracConPr> FracConPrList;

	// template for materials
	class Conductor: public cmn::Nameable{
		// methods
		public:
			// virtual destructor
			~Conductor(){};

			// material properties for scalar input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			virtual double calc_specific_heat(
				const double temperature) const = 0;
			
			// thermal conductivity [W m^-1 K^-1]
			virtual double calc_thermal_conductivity(
				const double temperature,
				const double magnetic_field_magnitude) const = 0; 

			// material density [kg m^-3]
			virtual double calc_density(
				const double temperature) const = 0;

			// volumetric specific heat [J m^-3 K^-1]
			virtual double calc_volumetric_specific_heat(
				const double temperature) const;

			// material properties for vector input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			virtual arma::Col<double> calc_specific_heat(
				const arma::Col<double> &temperature) const = 0;
			
			// thermal conductivity [W m^-1 K^-1]
			virtual arma::Col<double> calc_thermal_conductivity(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude) const = 0; 

			// material density [kg m^-3]
			virtual arma::Col<double> calc_density(
				const arma::Col<double> &temperature) const = 0;

			// volumetric specific heat [J m^-3 K^-1]
			virtual arma::Col<double> calc_volumetric_specific_heat(
				const arma::Col<double> &temperature) const;


			// material properties for matrices 
			// these are essentially wrappers
			// for the vector material properties
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			arma::Mat<double> calc_specific_heat_mat(
				const arma::Mat<double> &temperature) const;
			
			// thermal conductivity [W m^-1 K^-1]
			arma::Mat<double> calc_thermal_conductivity_mat(
				const arma::Mat<double> &temperature,
				const arma::Mat<double> &magnetic_field_magnitude) const; 
			
			// material density [kg m^-3]
			arma::Mat<double> calc_density_mat(
				const arma::Mat<double> &temperature) const;

			// volumetric specific heat [J m^-3 K^-1]
			arma::Mat<double> calc_volumetric_specific_heat_mat(
				const arma::Mat<double> &temperature) const;


			// analysis tools for scalar input
			// ===
			// calcultae temperature increase due to added heat
			// output in [K]
			double calc_temperature_increase(
				const double initial_temperature, 
				const double volume, 
				const double added_heat,
				const double delta_temperature = 1.0) const;

			// calculate the energy density based on the temperature
			// output in [J m^-3]
			double calc_thermal_energy_density(
				const double temperature,
				const double delta_temperature = 1.0) const;

			// calculate the length of the minimal propagation zone
			// output in [m]
			double calc_theoretical_lmpz(
				const double current_density,
				const double operating_temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const;

			// calculate the normal zone propagation velocity
			// output in [m s^-1]
			double calc_theoretical_vnzp(
				const double current_density,
				const double operating_temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const;

			// calculate minimal quench energy in [J] or 
			// [J/m^2] if cross sectional area is not provided
			double calc_theoretical_mqe(
				const double current_density,
				const double operating_temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle,
				const double cross_sectional_area = 1.0) const;

			// calculate adiabatic normal zone temperature
			double calc_adiabatic_time(
				const double current_density,
				const double operating_temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle,
				const double max_temperature,
				const double max_temperature_step = 0.5) const;


			// analysis tools for vector input
			// ===
			// calcultae temperature increase due to added heat
			arma::Col<double> calc_temperature_increase(
				const arma::Col<double> &initial_temperature, 
				const arma::Col<double> &volume, 
				const arma::Col<double> &added_heat) const;

			// calculate the energy density based on the temperature
			arma::Col<double> calc_thermal_energy_density(
				const arma::Col<double> &temperature,
				const double delta_temperature = 1.0) const;


			// accumulation method
			virtual void accu_parallel_conductors(
				FracConPrList &parallel_conductors) const;


			// calculate normal zone propagation velocity

			// calculate minimal quench energy

			// run simple NZP model?

			// functions averaged over cross section of conductor
			// ===
			// critical current density averaged over cross section [J m^-2]
			virtual arma::Col<double> calc_critical_current_density_av(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas) const;

			// electric field [V m^-1] section averaged
			// required to supply the cross-sectional areas of the
			// elements making up a section in the same order as
			// the other properties
			virtual arma::Col<double> calc_electric_field_av(
				const arma::Col<double> &current_density,
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas) const = 0;

			// critical temperature calculation in [A m^-2]
			// using averaged critical current over cross area
			virtual arma::Col<double> calc_cs_temperature_av(
				const arma::Col<double> &current_density, 
				const arma::Col<double> &magnetic_field_magnitude, 
				const arma::Col<double> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas,
				const double temperature_increment = 10.0,
				const double tol_bisection = 0.1) const;



			// helper functions
			// ===
			// cross section averaging
			static arma::Mat<double> nodes2section(
				const arma::Mat<double> &node_values,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas);

			// cross section values to nodes
			static arma::Mat<double> section2nodes(
				const arma::Mat<double> &section_values,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas);

			// Display function
			// make ss a table in the supplied log
			void display_property_table(
				cmn::ShLogPr lg = cmn::Log::create(),
				const arma::Col<double> &temperature = {1.9,4.5,10.0,20.0,30.0,40.0,50.0,60.0,77.0},
				const arma::Col<double> &magnetic_field_magnitude = {5.0},
				const arma::Col<double> &magnetic_field_angle = {0.0}) const;
	};


}}

#endif