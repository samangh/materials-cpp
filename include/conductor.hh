/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_CONDUCTOR_HH
#define MAT_CONDUCTOR_HH

#include <armadillo>
#include <memory>
#include <cassert>

#include "rat/common/log.hh"
#include "rat/common/nameable.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class Conductor> ShConductorPr;
	typedef arma::field<ShConductorPr> ShConductorPrList;

	// typedef for list containing both fraction and conductor
	typedef std::pair<double, ShConductorPr> FracShConPr;
	typedef std::list<FracShConPr> FracShConPrList;
	typedef std::pair<double, const Conductor*> FracConPr;
	typedef std::list<FracConPr> FracConPrList;

	// template for materials
	class Conductor: public cmn::Nameable{
		// methods
		public:
			// virtual destructor
			~Conductor(){};

			// material properties for scalar input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			virtual double calc_specific_heat(
				const double temperature) const = 0;
			
			// thermal conductivity [W m^-1 K^-1]
			virtual double calc_thermal_conductivity(
				const double temperature,
				const double magnetic_field_magnitude) const = 0; 
			
			// electrical resistivity [Ohm m]
			virtual double calc_electrical_resistivity(
				const double temperature,
				const double magnetic_field_magnitude) const = 0;

			// critical current density [J m^-2]
			virtual double calc_critical_current_density(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const = 0;

			// critical current density [J m^-2]
			virtual double calc_nvalue(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const = 0;

			// material density [kg m^-3]
			virtual double calc_density(
				const double temperature) const = 0;

			// volumetric specific heat [J m^-3 K^-1]
			virtual double calc_volumetric_specific_heat(
				const double temperature) const;

			// electric field [V/m]
			virtual double calc_electric_field(
				const double current_density,
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const = 0;

			// electric field using pre-calculated electrical 
			// resistivity en critical current density [V/m]
			virtual double calc_electric_field_fast(
				const double current_density,
				const arma::Row<double> &electrical_resistivity_vec,
				const arma::Row<double> &critical_current_density_vec,
				const arma::Row<double> &nvalue_vec) const = 0;
			
			// calculate current density [A m^-2]
			virtual double calc_current_density(
				const double electric_field,
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const = 0;

			// calculate current density [A m^-2]
			virtual double calc_current_density_fast(
				const double electric_field,
				const arma::Row<double> &electrical_resistivity_vec,
				const arma::Row<double> &critical_current_density_vec,
				const arma::Row<double> &nvalue_vec) const = 0;

			// calculate the critical temperature 
			// output in [K]
			virtual double calc_cs_temperature(
				const double current_density, 
				const double magnetic_field_magnitude, 
				const double magnetic_field_angle,
				const double temperature_increment = 10.0,
				const double tol_bisection = 0.1) const;

			// calculate partial derivative of electric field 
			// with temperature using finite difference method
			double calc_electric_field_dT(
				const double current_density,
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle,
				const double delta_temperature = 0.1) const;


			// material properties for vector input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			virtual arma::Col<double> calc_specific_heat(
				const arma::Col<double> &temperature) const = 0;
			
			// thermal conductivity [W m^-1 K^-1]
			virtual arma::Col<double> calc_thermal_conductivity(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude) const = 0; 
			
			// electrical resistivity [Ohm m]
			virtual arma::Col<double> calc_electrical_resistivity(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude) const = 0;

			// critical current density [J m^-2]
			virtual arma::Col<double> calc_critical_current_density(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const = 0;

			// critical current density [J m^-2]
			virtual arma::Col<double> calc_nvalue(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const = 0;

			// material density [kg m^-3]
			virtual arma::Col<double> calc_density(
				const arma::Col<double> &temperature) const = 0;

			// volumetric specific heat [J m^-3 K^-1]
			virtual arma::Col<double> calc_volumetric_specific_heat(
				const arma::Col<double> &temperature) const;

			// electric field [V m^-1]
			virtual arma::Col<double> calc_electric_field(
				const arma::Col<double> &current_density,
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const = 0;

			// electric field using pre-calculated electrical 
			// resistivity en critical current density [V m^-1]
			virtual arma::Col<double> calc_electric_field_fast(
				const arma::Col<double> &current_density,
				const arma::Mat<double> &electrical_resistivity_mat,
				const arma::Mat<double> &critical_current_density_mat, 
				const arma::Mat<double> &nvalue_mat) const = 0;

			// calculate current density [A m^-2]
			virtual arma::Col<double> calc_current_density(
				const arma::Col<double> &electric_field,
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const = 0;

			// current density from electric field 
			// output in [A m^-2]
			virtual arma::Col<double> calc_current_density_fast(
				const arma::Col<double> &electric_field,
				const arma::Mat<double> &electrical_resistivity_mat,
				const arma::Mat<double> &critical_current_density_mat, 
				const arma::Mat<double> &nvalue_mat) const = 0;

			// critical temperature calculation in [A m^-2]
			virtual arma::Col<double> calc_cs_temperature(
				const arma::Col<double> &current_density, 
				const arma::Col<double> &magnetic_field_magnitude, 
				const arma::Col<double> &magnetic_field_angle,
				const double temperature_increment = 10.0,
				const double tol_bisection = 0.1) const;

			// calculate partial derivative of electric field [V m^-1 K^-1]
			// with temperature using finite difference method
			arma::Col<double> calc_electric_field_dT(
				const arma::Col<double> &current_density,
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle,
				const double dT = 1e-2) const;

			// calculate percentage on the load-line in [pct]
			// assuming fixed magnetic field angle
			arma::Col<double> calc_percent_load(
				const arma::Col<double> &current_density,
				const arma::Col<double> &temperature, 
				const arma::Col<double> &magnetic_field_magnitude, 
				const arma::Col<double> &magnetic_field_angle,
				const double current_density_increment = 100e6,
				const double tol_bisection = 0.1) const;


			// material properties for matrices 
			// these are essentially wrappers
			// for the vector material properties
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			arma::Mat<double> calc_specific_heat_mat(
				const arma::Mat<double> &temperature) const;
			
			// thermal conductivity [W m^-1 K^-1]
			arma::Mat<double> calc_thermal_conductivity_mat(
				const arma::Mat<double> &temperature,
				const arma::Mat<double> &magnetic_field_magnitude) const; 
			
			// electrical resistivity [Ohm m]
			arma::Mat<double> calc_electrical_resistivity_mat(
				const arma::Mat<double> &temperature,
				const arma::Mat<double> &magnetic_field_magnitude) const;

			// critical current density [J m^-2]
			arma::Mat<double> calc_critical_current_density_mat(
				const arma::Mat<double> &temperature,
				const arma::Mat<double> &magnetic_field_magnitude,
				const arma::Mat<double> &magnetic_field_angle) const;

			// critical current density [J m^-2]
			arma::Mat<double> calc_nvalue_mat(
				const arma::Mat<double> &temperature,
				const arma::Mat<double> &magnetic_field_magnitude,
				const arma::Mat<double> &magnetic_field_angle) const;

			// material density [kg m^-3]
			arma::Mat<double> calc_density_mat(
				const arma::Mat<double> &temperature) const;

			// volumetric specific heat [J m^-3 K^-1]
			arma::Mat<double> calc_volumetric_specific_heat_mat(
				const arma::Mat<double> &temperature) const;

			// electric field [V m^-1]
			arma::Mat<double> calc_electric_field_mat(
				const arma::Mat<double> &current_density,
				const arma::Mat<double> &temperature,
				const arma::Mat<double> &magnetic_field_magnitude,
				const arma::Mat<double> &magnetic_field_angle) const;

			// calculate current density [A m^-2]
			arma::Mat<double> calc_current_density_mat(
				const arma::Mat<double> &electric_field,
				const arma::Mat<double> &temperature,
				const arma::Mat<double> &magnetic_field_magnitude,
				const arma::Mat<double> &magnetic_field_angle) const;

			// critical temperature calculation in [A m^-2]
			arma::Mat<double> calc_cs_temperature_mat(
				const arma::Mat<double> &current_density, 
				const arma::Mat<double> &magnetic_field_magnitude, 
				const arma::Mat<double> &magnetic_field_angle,
				const double temperature_increment = 10.0,
				const double tol_bisection = 0.1) const;

			// critical current density averaged over cross section [J m^-2]
			arma::Mat<double> calc_critical_current_density_av_mat(
				const arma::Mat<double> &temperature,
				const arma::Mat<double> &magnetic_field_magnitude,
				const arma::Mat<double> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas) const;

			// electric field [V m^-1] section averaged
			// required to supply the cross-sectional areas of the
			// elements making up a section in the same order as
			// the other properties
			arma::Mat<double> calc_electric_field_av_mat(
				const arma::Mat<double> &current_density,
				const arma::Mat<double> &temperature,
				const arma::Mat<double> &magnetic_field_magnitude,
				const arma::Mat<double> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas) const;
			
			// critical temperature calculation in [A m^-2]
			// using averaged critical current over cross area
			arma::Mat<double> calc_cs_temperature_av_mat(
				const arma::Mat<double> &current_density, 
				const arma::Mat<double> &magnetic_field_magnitude, 
				const arma::Mat<double> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas,
				const double temperature_increment = 10.0,
				const double tol_bisection = 0.1) const;

			// calculate percentage on the load-line
			// assuming fixed magnetic field angle
			arma::Mat<double> calc_percent_load_mat(
				const arma::Mat<double> &current_density,
				const arma::Mat<double> &temperature, 
				const arma::Mat<double> &magnetic_field_magnitude, 
				const arma::Mat<double> &magnetic_field_angle,
				const double current_density_increment = 100e6,
				const double tol_bisection = 0.1) const;


			// analysis tools for scalar input
			// ===
			// calcultae temperature increase due to added heat
			// output in [K]
			double calc_temperature_increase(
				const double initial_temperature, 
				const double volume, 
				const double added_heat,
				const double delta_temperature = 1.0) const;

			// calculate the energy density based on the temperature
			// output in [J m^-3]
			double calc_thermal_energy_density(
				const double temperature,
				const double delta_temperature = 1.0) const;

			// calculate the length of the minimal propagation zone
			// output in [m]
			double calc_theoretical_lmpz(
				const double current_density,
				const double operating_temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const;

			// calculate the normal zone propagation velocity
			// output in [m s^-1]
			double calc_theoretical_vnzp(
				const double current_density,
				const double operating_temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const;

			// calculate minimal quench energy in [J] or 
			// [J/m^2] if cross sectional area is not provided
			double calc_theoretical_mqe(
				const double current_density,
				const double operating_temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle,
				const double cross_sectional_area = 1.0) const;

			// calculate adiabatic normal zone temperature
			double calc_adiabatic_time(
				const double current_density,
				const double operating_temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle,
				const double max_temperature,
				const double max_temperature_step = 0.5) const;


			// analysis tools for vector input
			// ===
			// calcultae temperature increase due to added heat
			arma::Col<double> calc_temperature_increase(
				const arma::Col<double> &initial_temperature, 
				const arma::Col<double> &volume, 
				const arma::Col<double> &added_heat) const;

			// calculate the energy density based on the temperature
			arma::Col<double> calc_thermal_energy_density(
				const arma::Col<double> &temperature,
				const double delta_temperature = 1.0) const;


			// accumulation method for parallel conductors
			virtual void accu_parallel_conductors(
				FracConPrList &parallel_conductors) const;

			// accumulation method for series conductors
			virtual void accu_series_conductors(
				FracConPrList &series_conductors) const;


			// functions averaged over cross section of conductor
			// ===
			// critical current density averaged over cross section [J m^-2]
			virtual arma::Col<double> calc_critical_current_density_av(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas) const;

			// electric field [V m^-1] section averaged
			// required to supply the cross-sectional areas of the
			// elements making up a section in the same order as
			// the other properties
			virtual arma::Col<double> calc_electric_field_av(
				const arma::Col<double> &current_density,
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas) const = 0;
			
			// critical temperature calculation in [A m^-2]
			// using averaged critical current over cross area
			virtual arma::Col<double> calc_cs_temperature_av(
				const arma::Col<double> &current_density, 
				const arma::Col<double> &magnetic_field_magnitude, 
				const arma::Col<double> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas,
				const double temperature_increment = 10.0,
				const double tol_bisection = 0.1) const;



			// helper functions
			// ===
			// cross section averaging
			static arma::Mat<double> nodes2section(
				const arma::Mat<double> &node_values,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas);

			// cross section values to nodes
			static arma::Mat<double> section2nodes(
				const arma::Mat<double> &section_values,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas);

			// Display function
			// make ss a table in the supplied log
			void display_property_table(
				cmn::ShLogPr lg = cmn::Log::create(),
				const arma::Col<double> &temperature = {1.9,4.5,10.0,20.0,30.0,40.0,50.0,60.0,77.0},
				const arma::Col<double> &magnetic_field_magnitude = {5.0},
				const arma::Col<double> &magnetic_field_angle = {0.0}) const;
	};


}}

#endif