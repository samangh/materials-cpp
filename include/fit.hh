/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_FIT_HH
#define MAT_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class Fit> ShFitPr;

	// template for materials
	class Fit: public cmn::Node{
		// methods
		public:
			// virtual destructor
			~Fit(){};

			// calculate property as function of scalar temperature
			virtual double calc_property(
				const double temperature) const;

			// calculate property as function of scalar
			// temperature and magnetic field magnitude
			virtual double calc_property(
				const double temperature,
				const double magnetic_field_magnitude) const;

			// calculate property as function of scalar
			// temperature and magnetic field magnitude
			virtual double calc_property(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const;

			// calculate property as function of vector temperature
			virtual arma::Col<double> calc_property(
				const arma::Col<double> &temperature) const;

			// calculate property as function of vector
			// temperature and magnetic field magnitude
			virtual arma::Col<double> calc_property(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude) const;

			// calculate property as function of vector
			// temperature, magnetic field magnitude and angle
			virtual arma::Col<double> calc_property(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const;
	};

}}

#endif