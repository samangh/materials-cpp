/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_HTS_TAPE_HH
#define MAT_HTS_TAPE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "parallelconductor.hh"
#include "htscore.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class HTSTape> ShHTSTapePr;

	// wrapper for representing HTS tape 
	// consisting of core and stabilizer
	class HTSTape: public ParallelConductor{
		private: 
			// dimensions
			double width_;
			double thickness_;

			// core material
			ShHTSCorePr core_;

			// substrate material
			ShConductorPr stab_;

		public:
			// constructor
			HTSTape();
			HTSTape(const double width, const double thickness, ShHTSCorePr core, ShConductorPr stab);

			// factory
			static ShHTSTapePr create();
			static ShHTSTapePr create(const double width, const double thickness, ShHTSCorePr core, ShConductorPr stab);

			// copy function
			cmn::ShNodePr copy() const override;

			// set properties
			void set_width(const double width);
			void set_thickness(const double thickness);

			// set core dimensions
			double get_width() const;
			double get_thickness() const;
			double get_area() const;

			// set layer thicknesses
			double get_tcore() const;
			double get_tstab() const;
			
			// get layer conductors
			ShHTSCorePr get_core() const;
			ShConductorPr get_stab() const;

			// set layer conductors
			void set_core(ShHTSCorePr core);
			void set_stab(ShConductorPr stab);

			// setup function
			void setup();

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif