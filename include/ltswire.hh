/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_LTS_WIRE_HH
#define MAT_LTS_WIRE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "parallelconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class LTSWire> ShLTSWirePr;

	// wrapper for representing LTS wire
	class LTSWire: public ParallelConductor{
		private: 
			// dimensions
			double fnc2sc_;
			double diameter_;

			// superconducting material
			ShConductorPr sc_;

			// normal conducting part such as stabilizer and/or matrix
			ShConductorPr nc_;

		public:
			// constructor
			LTSWire();

			// factory
			static ShLTSWirePr create();

			// copy function
			cmn::ShNodePr copy() const override;

			// set layer conductors
			void set_sc(ShConductorPr sc);
			void set_nc(ShConductorPr nc);

			// get conductors
			ShConductorPr get_sc() const;
			ShConductorPr get_nc() const;

			// set dimensions
			void set_diameter(const double diameter);
			void set_fnc2sc(const double fnc2sc);

			// get dimensions
			double get_diameter() const;
			double get_fnc2sc() const;

			// setup function
			void setup();

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif