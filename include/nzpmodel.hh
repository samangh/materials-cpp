/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_NZPMODEL_HH
#define MAT_NZPMODEL_HH

#include <armadillo>
#include <memory>
#include <cassert>

#include "rat/common/log.hh"
#include "rat/common/extra.hh"
#include "rat/common/parfor.hh"

#include "conductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class NZPModel> ShNZPModelPr;
	typedef arma::field<ShNZPModelPr> ShNZPModelPrList;

	// toy normal zone propagation model
	class NZPModel{
		private:
			// the to-be analysed conductor
			ShConductorPr conductor_;

			// operating conditions
			double current_density_ = 900e6; // [A m^-2]
			double magnetic_field_magnitude_ = 5; // [T]
			double magnetic_field_angle_ = 0; // [rad]
			double operating_temperature_ = 20; // [K]

			// geometry
			double tape_length_ = 0.15; // [m]
			double element_size_ = 1e-3; // [m]
			
			// time stepping 
			double max_temperature_step_ = 10.0; // [K]
			double max_time_step_ = 0.1; // [s]
			double timestep_ini_ = 1e-3; // [s]
			double abs_tol_ = 1e-4; // [K]
			arma::uword max_iter_ = 50;

			// stop conditions
			double max_temperature_ = 500.0;
			double max_time_ = 4.0;

			// parallel option
			bool use_parallel_ = true;

			// temperature profile data
			arma::Col<double> position_vec_;
			std::list<std::pair<double,arma::Col<double> > > nzp_data_;


		// methods
		public:
			// constructor
			NZPModel();
			NZPModel(ShConductorPr conductor);

			// factory
			static ShNZPModelPr create();
			static ShNZPModelPr create(ShConductorPr conductor);

			// setters
			void set_conductor(ShConductorPr conductor);

			// set conditions
			void set_current_density(const double current_density);
			void set_magnetic_field_magnitude(const double magnetic_field_magnitude);
			void set_magnetic_field_angle(const double magnetic_field_angle);
			void set_operating_temperature(const double operating_temperature);
			void set_element_size(const double element_size);
			void set_tape_length(const double tape_length);

			// solve system
			void solve(cmn::ShLogPr lg = cmn::NullLog::create());

			// system function to solve
			arma::Col<double> bdf_system_fun(
				const arma::Col<double> &temperature_prev,
				const arma::Col<double> &temperature_next,
				const double dt) const;

			// calculate normal zone propagation velocity
			arma::Col<double> calc_velocity() const;
			arma::Col<double> calc_voltage() const;
			arma::Col<double> calc_resistance() const;
			arma::Col<double> calc_peak_temperature() const;
			arma::Col<double> get_times() const;
			double calc_resistance(const double peak_temperature) const;
			double calc_burn_time(const double voltage_treshold, const double temperature_limit) const;

	};

}}

#endif