/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_RUTHERFORD_CABLE_HH
#define MAT_RUTHERFORD_CABLE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "parallelconductor.hh"
#include "ltswire.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class RutherfordCable> ShRutherfordCablePr;

	// wrapper for representing LTS wire
	class RutherfordCable: public ParallelConductor{
		private: 
			// cabling parameters
			arma::uword num_strands_; // total number of strands in cable [#]

			// cable dimensions
			double width_; // bare cable width [m]
			double thickness_; // bare cable thickness [m]
			double keystone_; // keystone angle [rad]
			double dinsu_; // insulation thickness [m]

			// degradation due to cabling
			double fcabling_;

			// cabling pitch
			double pitch_; // twist pitch [m]

			// strand conductor
			ShLTSWirePr strand_; // material used in strands
			ShConductorPr insu_; // material used in insulation layer
			ShConductorPr voids_; // material used in voids between strands

		public:
			// constructor
			RutherfordCable();

			// factory
			static ShRutherfordCablePr create();

			// copy function
			cmn::ShNodePr copy() const override;

			// set layer conductors
			void set_strand(ShLTSWirePr strand);
			void set_insu(ShConductorPr insu);
			void set_voids(ShConductorPr voids);
			
			// get conductors
			ShLTSWirePr get_strand() const;

			// set dimensions
			void set_num_strands(const arma::uword num_strands);
			void set_width(const double width);
			void set_thickness(const double thickness);
			void set_keystone(const double keystone);
			void set_pitch(const double pitch);
			void set_dinsu(const double dinsu);
			void set_fcabling(const double fcabling);

			// setup function
			void setup();

			// critical current density [J m^-2]
			double calc_critical_current_density(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// critical current density [J m^-2]
			arma::Col<double> calc_critical_current_density(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif