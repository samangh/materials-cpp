/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_HTS_CORE_HH
#define MAT_HTS_CORE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "parallelconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class HTSCore> ShHTSCorePr;

	// wrapper for representing HTS tape core
	// this consists of substrate and superconducting layer
	class HTSCore: public ParallelConductor{
		private: 
			// dimensions
			double width_;
			double thickness_;
			double tsc_;
			double tsubstr_;

			// core material
			ShConductorPr sc_;

			// substrate material
			ShConductorPr substr_;

		public:
			// constructor
			HTSCore();
			HTSCore(const double width, const double thickness, const double tsc, ShConductorPr sc, const double tsubstr, ShConductorPr substr);

			// factory
			static ShHTSCorePr create();
			static ShHTSCorePr create(const double width, const double thickness, const double tsc, ShConductorPr sc, const double tsubstr, ShConductorPr substr);

			// copy function
			cmn::ShNodePr copy() const override;

			// set properties
			void set_width(const double width);
			void set_thickness(const double thickness);
			
			// set core dimensions
			double get_width() const;
			double get_thickness() const;
			double get_area() const;

			// set layer thicknesses
			double get_tsc() const;
			double get_tsubstr() const;
			
			// surface 2D critical current density 
			// given per unit width [A m^-1]
			double calc_critical_current_per_width(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const;

			// get layer conductors
			ShConductorPr get_sc() const;
			ShConductorPr get_substr() const;

			// set layer conductors
			void set_sc(ShConductorPr sc);
			void set_substr(ShConductorPr substr);

			// setup function
			void setup();

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif