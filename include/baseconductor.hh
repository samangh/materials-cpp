/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_NORMAL_CONDUCTOR_HH
#define MAT_NORMAL_CONDUCTOR_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/extra.hh"

#include "conductor.hh"

#include "fit.hh"

// for making copper
#include "nist8sumfit.hh"
#include "cudifit.hh"
#include "wflfit.hh"
#include "constfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class BaseConductor> ShBaseConductorPr;
	typedef arma::field<ShBaseConductorPr> ShBaseConductorPrList;

	// template for materials
	class BaseConductor: public Conductor{
		private:
			// specific heat
			ShFitPr cp_fit_;

			// thermal conductivity
			ShFitPr k_fit_;

			// electrical resistivity
			ShFitPr rho_fit_;

			// density fit
			ShFitPr d_fit_;

			// N-value fit
			ShFitPr nv_fit_;

			// critical current density fit
			ShFitPr jc_fit_;

			// electric field criterion
			double electric_field_criterion_ = 0;

		public:
			// base condutcor
			BaseConductor();
			BaseConductor(ShFitPr cp_fit, ShFitPr k_fit, ShFitPr rho_fit, ShFitPr d_fit); // normal conductor
			BaseConductor(ShFitPr jc_fit, ShFitPr nv_fit, ShFitPr cp_fit, ShFitPr k_fit, ShFitPr rho_fit, ShFitPr d_fit); // superconductor

			// factories
			static ShBaseConductorPr create();
			static ShBaseConductorPr create(ShFitPr cp_fit, ShFitPr k_fit, ShFitPr rho_fit, ShFitPr d_fit); // normal conductor
			static ShBaseConductorPr create(ShFitPr jc_fit, ShFitPr nv_fit, ShFitPr cp_fit, ShFitPr k_fit, ShFitPr rho_fit, ShFitPr d_fit); // superconductor

			// standard implementations
			static ShBaseConductorPr copper(const double RRR = 100);

			// copy method
			cmn::ShNodePr copy() const override;

			// setting the fit fit functions
			void set_electric_field_criterion(const double electric_field_criterion);
			void set_thermal_conductivity_fit(ShFitPr k_fit);
			void set_electrical_resistivity_fit(ShFitPr rho_fit);
			void set_specific_heat_fit(ShFitPr cp_fit);
			void set_density_fit(ShFitPr d_fit);
			void set_critical_current_density_fit(ShFitPr jc_fit);
			void set_nvalue_fit(ShFitPr nv_fit);

			// material property calculation for scalars
			// calculate specific heat [J kg^-1 K^-1]
			double calc_specific_heat(
				const double temperature) const override;

			// thermal conductivity [W m^-1 K^-1]
			double calc_thermal_conductivity(
				const double temperature,
				const double magnetic_field_magnitude) const override; 

			// electrical resistivity [Ohm m^2]
			double calc_electrical_resistivity(
				const double temperature,
				const double magnetic_field_magnitude) const override;

			// material density [kg m^-3]
			double calc_density(
				const double temperature) const override;
			
			// critical current density [J m^-2]
			double calc_critical_current_density(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;
			
			// power law nvalue calculation
			double calc_nvalue(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// electric field [V/m]
			double calc_electric_field(
				const double current_density,
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// electric field using pre-calculated electrical 
			// resistivity en critical current density [V/m]
			double calc_electric_field_fast(
				const double current_density,
				const arma::Row<double> &electrical_resistivity_vec,
				const arma::Row<double> &critical_current_density_vec,
				const arma::Row<double> &nvalue_vec) const override;

			// calculate current density [A m^-2]
			double calc_current_density(
				const double electric_field,
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// calculate current density [A m^-2]
			double calc_current_density_fast(
				const double electric_field,
				const arma::Row<double> &electrical_resistivity_vec,
				const arma::Row<double> &critical_current_density_vec,
				const arma::Row<double> &nvalue_vec) const override;


			// material property calculation for vectors
			// calculate specific heat [J kg^-1 K^-1]
			arma::Col<double> calc_specific_heat(
				const arma::Col<double> &temperature) const override;

			// thermal conductivity [W m^-1 K^-1]
			arma::Col<double> calc_thermal_conductivity(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude) const override; 

			// electrical resistivity [Ohm m^2]
			arma::Col<double> calc_electrical_resistivity(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude) const override;

			// material density [kg m^-3]
			arma::Col<double> calc_density(
				const arma::Col<double> &temperature) const override;
			
			// critical current density [J m^-2]
			arma::Col<double> calc_critical_current_density(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// power law nvalue calculation
			arma::Col<double> calc_nvalue(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// electric field [V/m]
			arma::Col<double> calc_electric_field(
				const arma::Col<double> &current_density,
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// electric field [V m^-1] section averaged
			// required to supply the cross-sectional areas of the
			// elements making up a section in the same order as
			// the other properties
			arma::Col<double> calc_electric_field_av(
				const arma::Col<double> &current_density,
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas) const override;

			// electric field using pre-calculated electrical 
			// resistivity en critical current density [V/m]
			arma::Col<double> calc_electric_field_fast(
				const arma::Col<double> &current_density,
				const arma::Mat<double> &electrical_resistivity_mat, // each conductor is a column
				const arma::Mat<double> &critical_current_density_mat, // each conductor is a column
				const arma::Mat<double> &nvalue_mat) const override; // each conductor is a column

			// calculate current density [A m^-2]
			arma::Col<double> calc_current_density(
				const arma::Col<double> &electric_field,
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// current density from electric field 
			// output in [A m^-2]
			arma::Col<double> calc_current_density_fast(
				const arma::Col<double> &electric_field,
				const arma::Mat<double> &electrical_resistivity_mat, // each conductor is a column
				const arma::Mat<double> &critical_current_density_mat, // each conductor is a column
				const arma::Mat<double> &nvalue_mat) const override; // each conductor is a column

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif