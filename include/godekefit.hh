/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// Description:
// Calculates the critical current density of Nb3Sn using the A. God
// formula. The initial parameters can be set using two standard
// variants: Powder in Tube (PIT) and Rod-Restack Process (RRP). It must
// be noted that the formulas are not accurate in the low field region.

// references:
// A. Godeke, Msc. Thesis: Performance Boundaries in Nb3Sn Superconductors,
// Twente University 2005, p168

#ifndef MAT_GODEKE_FIT_HH
#define MAT_GODEKE_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class GodekeFit> ShGodekeFitPr;

	// Lubell/Kramer scaling relation
	class GodekeFit: public Fit{
		private:
			// deformation related parameters
			double Ca1_;
			double Ca2_;
			double eps_0a_; // normalized
			double C1_; // [kAT mm^-2]

			// superconducting parameters
			double Bc2m_; // [T]
			double Tcm_; // [K]

			// magnetic field dependence parameters
			double p_;
			double q_;

			// environmental parameters
			double eps_ax_ = 0; // axial strain

		public:
			// constructor
			GodekeFit();

			// factory
			static ShGodekeFitPr create();

			// copy function
			cmn::ShNodePr copy() const override;

			// function for calculating critical current density
			// using scalar input and output
			// output is in [A m^-2]
			double calc_property(
				const double temperature,
				const double magnetic_field_magnitude) const override;

			// function for calculating critical current density
			// using vector input and output
			// output is in [A m^-2]
			arma::Col<double> calc_property(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude) const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif