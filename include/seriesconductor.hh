/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_SERIES_CONDUCTOR_HH
#define MAT_SERIES_CONDUCTOR_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/extra.hh"

#include "conductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class SeriesConductor> ShSeriesConductorPr;

	// template for materials
	class SeriesConductor: public Conductor{
		// properties
		protected:
			// tolerance for bisection method
			double tol_bisection_ = 1e-7;

			// list of conductors <fraction, pointer>
			FracShConPrList conductor_list_;

		// methods
		public:
			// constructor
			SeriesConductor();

			// factory
			static ShSeriesConductorPr create();

			// copy function
			cmn::ShNodePr copy() const override;

			// add material to list
			void add_conductor(const double fraction, ShConductorPr conductor);

			// material properties for scalar input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			double calc_specific_heat(
				const double temperature) const override;
			
			// thermal conductivity [W m^-1 K^-1]
			double calc_thermal_conductivity(
				const double temperature,
				const double magnetic_field_magnitude) const override; 
			
			// electrical resistivity [Ohm m^2]
			double calc_electrical_resistivity(
				const double temperature,
				const double magnetic_field_magnitude) const override;

			// critical current density [J m^-2]
			virtual double calc_critical_current_density(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// critical current density [J m^-2]
			double calc_nvalue(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// material density [kg m^-3]
			double calc_density(
				const double temperature) const override;

			// electric field [V m^-1]
			double calc_electric_field(
				const double current_density,
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// electric field using pre-calculated electrical 
			// resistivity en critical current density [V m^-1]
			double calc_electric_field_fast(
				const double current_density,
				const arma::Row<double> &electrical_resistivity,
				const arma::Row<double> &critical_current_density,
				const arma::Row<double> &nvalue) const override;

			// calculate current density [A m^-2]
			double calc_current_density(
				const double electric_field,
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// calculate current density [A m^-2]
			double calc_current_density_fast(
				const double electric_field,
				const arma::Row<double> &electrical_resistivity,
				const arma::Row<double> &critical_current_density,
				const arma::Row<double> &nvalue) const override;



			// material properties for vector input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			arma::Col<double> calc_specific_heat(
				const arma::Col<double> &temperature) const override;
			
			// thermal conductivity [W m^-1 K^-1]
			arma::Col<double> calc_thermal_conductivity(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude) const override; 
			
			// electrical resistivity [Ohm m^2]
			arma::Col<double> calc_electrical_resistivity(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude) const override;

			// critical current density [J m^-2]
			virtual arma::Col<double> calc_critical_current_density(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// critical current density [J m^-2]
			arma::Col<double> calc_nvalue(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// material density [kg m^-3]
			arma::Col<double> calc_density(
				const arma::Col<double> &temperature) const override;

			// electric field [V/m]
			arma::Col<double> calc_electric_field(
				const arma::Col<double> &current_density,
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// electric field [V m^-1] section averaged
			// required to supply the cross-sectional areas of the
			// elements making up a section in the same order as
			// the other properties
			arma::Col<double> calc_electric_field_av(
				const arma::Col<double> &current_density,
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<double> &cross_areas) const override;

			// electric field using pre-calculated electrical 
			// resistivity en critical current density [V/m]
			arma::Col<double> calc_electric_field_fast(
				const arma::Col<double> &current_density,
				const arma::Mat<double> &electrical_resistivity,
				const arma::Mat<double> &critical_current_density, 
				const arma::Mat<double> &nvalue) const override;

			// calculate current density [A m^-2]
			arma::Col<double> calc_current_density(
				const arma::Col<double> &electric_field,
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// current density from electric field 
			// output in [A m^-2]
			arma::Col<double> calc_current_density_fast(
				const arma::Col<double> &electric_field,
				const arma::Mat<double> &electrical_resistivity,
				const arma::Mat<double> &critical_current_density, 
				const arma::Mat<double> &nvalue) const override;

			// get parallel conductors
			void accu_series_conductors(FracConPrList &series_conductors) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif