/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// DESCRIPTION
// Minimalist example of a solenoid model

#include "rat/common/node.hh"

#include "serializer.hh"
#include "constfit.hh"
#include "parallelconductor.hh"
#include "baseconductor.hh"
#include "htscore.hh"
#include "htstape.hh"

// main function
int main(){

	// Settings
	const double nvalue = 20;
	const double wcore = 12.00e-3;
	const double wtape = 12.04e-3;
	const double tsubstr = 50e-6;
	const double tsc = 2e-6;
	const double tcore = 60e-6;
	const double ttape = 100e-6;

	// construct individual layers
	rat::mat::ShSerializerPr slzr = rat::mat::Serializer::create();
	rat::mat::ShBaseConductorPr sc = 
		slzr->json2tree<rat::mat::BaseConductor>(
		"json/materials/superconductors/REBCO_Fujikura_CERN.json");
	rat::mat::ShBaseConductorPr stab = 
		slzr->json2tree<rat::mat::BaseConductor>(
		"json/materials/metals/CopperOFHC_RRR100.json");
	rat::mat::ShBaseConductorPr substr = 
		slzr->json2tree<rat::mat::BaseConductor>(
		"json/materials/stainless/Stainless316.json");
	
	// construct core and tape
	rat::mat::ShHTSCorePr core = rat::mat::HTSCore::create(
		wcore,tcore,tsc,sc,tsubstr,substr);
	rat::mat::ShHTSTapePr tape = rat::mat::HTSTape::create(
		wtape,ttape,core,stab);

	// override Nvalue
	sc->set_nvalue_fit(rat::mat::ConstFit::create(nvalue));

	rat::mat::ShConductorPr matcpy = tape->copy_covariant<rat::mat::Conductor>();

	// calculate electric field
	arma::uword num_step = 20;
	const arma::Col<double> J = arma::linspace<arma::Col<double> >(400,400,num_step);
	const arma::Col<double> T = arma::linspace<arma::Col<double> >(4.5,93,num_step);
	const arma::Col<double> B = arma::linspace<arma::Col<double> >(5,5,num_step);
	const arma::Col<double> a = arma::linspace<arma::Col<double> >(0,0,num_step);
	const arma::Col<double> E = matcpy->calc_electric_field(1e6*J,T,B,a);
	const double Esc = matcpy->calc_electric_field(1e6*J(10),T(10),B(10),a(10));

	std::cout<<E<<Esc<<std::endl;

	// calculate current density in each layer
	const arma::Col<double> Isc = wcore*tsc*sc->calc_current_density(E,T,B,a);
	const arma::Col<double> Inc = (wtape*ttape - wcore*tcore)*stab->calc_current_density(E,T,B,a) + tsubstr*wcore*substr->calc_current_density(E,T,B,a);
	
	std::cout<<arma::join_horiz(Isc,Inc,Isc+Inc)<<std::endl;

	return 0;
}