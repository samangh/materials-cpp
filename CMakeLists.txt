## PROJECT SETUP
cmake_minimum_required(VERSION 3.9 FATAL_ERROR)
project(Rat-Materials VERSION 1.000.0 LANGUAGES CXX)

# switchboard
option(ENABLE_TESTING "Build unit/system tests" ON)
option(ENABLE_EXAMPLES "Build example applications" ON)
option(ENABLE_CLIAPP "Build commandline applications" ON)

# set build type
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

# compiler options
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wpedantic -Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native -O3")
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# ensure out of core build
if(PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
  message(FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there.\n")
endif()

## EXTERNAL LIBRARIES
# where are the custom CMake modules located
list(INSERT CMAKE_MODULE_PATH 0 ${CMAKE_SOURCE_DIR}/cmake)

# find the rat common library
find_package(RatCommon 1.0 REQUIRED)

## LIBRARY
# add the library
add_library(ratmat SHARED
	src/conductor.cpp
	src/baseconductor.cpp
	src/parallelconductor.cpp
	src/htscore.cpp
	src/htstape.cpp
	src/ltswire.cpp
	src/nzpmodel.cpp
	src/fit.cpp
	src/nist8sumfit.cpp
	src/nist8fracfit.cpp
	src/polyfit.cpp
	src/lubellkramerfit.cpp
	src/constfit.cpp
	src/multifit.cpp
	src/dualfit.cpp
	src/godekefit.cpp
	src/fleiterfit.cpp
	src/cudifit.cpp
	src/wflfit.cpp
	src/rutherfordcable.cpp
	src/serializer.cpp
	src/seriesconductor.cpp
)

# Add an alias so that library can be used inside the build tree, e.g. when testing
add_library(Rat::Materials ALIAS ratmat)

# properties
set_target_properties(ratmat PROPERTIES VERSION ${PROJECT_VERSION})
set_target_properties(ratmat PROPERTIES SOVERSION 1)

# add include directory
target_include_directories(ratmat
	PUBLIC 
		$<INSTALL_INTERFACE:include>    
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/src
)

# link libraries
target_link_libraries(ratmat PUBLIC Rat::Common)

## UNIT/SYSTEM TESTS
# check switchboard
if(ENABLE_TESTING)
	# use cmake's platform for unit testing
	enable_testing()

	# add test directory
	add_subdirectory(test)
endif()

## Applications
# check switchboard
if(ENABLE_EXAMPLES)
	add_subdirectory(examples)
endif()

# check switchboard
if(ENABLE_CLIAPP)
	add_subdirectory(cliapp)
endif()


## copy database files to build directory
file(COPY json DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

## INSTALLATION
# get install directories
include(GNUInstallDirs)
set(INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/ratmat)
set(INSTALL_DATABASEDIR ${CMAKE_INSTALL_LIBDIR})

install(TARGETS ratmat
	EXPORT ratmat-targets
	LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}/rat
	ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}/rat
)

# This is required so that the exported target has the name RatCmn and not ratcmn
set_target_properties(ratmat PROPERTIES EXPORT_NAME Mat)

install(DIRECTORY include/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/rat/mat)

# Export the targets to a script
install(EXPORT ratmat-targets
	FILE
		RatMatTargets.cmake
	NAMESPACE
		Rat::
	DESTINATION
		${INSTALL_CONFIGDIR}
)

# Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
	${CMAKE_CURRENT_BINARY_DIR}/RatMatConfigVersion.cmake
	VERSION ${PROJECT_VERSION}
	COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(${CMAKE_CURRENT_LIST_DIR}/cmake/RatMatConfig.cmake.in
	${CMAKE_CURRENT_BINARY_DIR}/RatMatConfig.cmake
	INSTALL_DESTINATION ${INSTALL_CONFIGDIR}
)

# Install the config, configversion and custom find modules
install(FILES
	${CMAKE_CURRENT_BINARY_DIR}/RatMatConfig.cmake
	${CMAKE_CURRENT_BINARY_DIR}/RatMatConfigVersion.cmake
	DESTINATION ${INSTALL_CONFIGDIR}
)

# Install the database directory
install(DIRECTORY 
	${CMAKE_CURRENT_BINARY_DIR}/json
	DESTINATION ${INSTALL_DATABASEDIR})

# export the cmake files
export(EXPORT ratmat-targets
	FILE ${CMAKE_CURRENT_BINARY_DIR}/RatMatTargets.cmake
	NAMESPACE Rat::)

# export the data inspector
if(ENABLE_CLIAPP)
	install(FILES ${CMAKE_CURRENT_BINARY_DIR}/cliapp/matprops
		${CMAKE_CURRENT_BINARY_DIR}/cliapp/matnzp
		DESTINATION ${CMAKE_INSTALL_PREFIX}/bin
		PERMISSIONS WORLD_EXECUTE)
endif()

# Register package in user's package registry
# export(PACKAGE RatMat)
