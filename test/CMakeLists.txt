# make a list of unit test files
set(test_list

)

# walk over source files
foreach(srcfile ${test_list})
	string(REPLACE ".cpp" "" name ${srcfile})
	add_executable(${name} ${srcfile})
	target_link_libraries(${name} Rat::Common)
	add_test(${name} ${name})
endforeach()