/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "lubellkramer.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	LubellKramer::LubellKramer(){

	}

	// factory
	ShLubellKramerPr LubellKramer::create(){
		return std::make_shared<LubellKramer>();
	}

	// get function for electric field criterion
	double LubellKramer::get_electric_field_criterion() const{
		return electric_field_criterion_;
	}

	// set function for critical currnet
	void LubellKramer::set_nvalue(const double nvalue){
		if(nvalue<0)rat_throw_line("N-value can not be negative");
		nvalue_ = nvalue;
	}

	// set function for critical currnet
	void LubellKramer::set_electric_field_criterion(const double electric_field_criterion){
		if(electric_field_criterion<0)rat_throw_line("electric field criterion can not be negative");
		electric_field_criterion_ = electric_field_criterion;
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	double LubellKramer::calc_critical_current_density(
		const double temperature,
		const double magnetic_field_magnitude,
		const double /*magnetic_field_angle*/) const{

		// copy input and limit
		const double mgn_field_lim = std::max(0.1,magnetic_field_magnitude);
		
		// calculate scaled values
		const double t = temperature/Tc0_;
		const double Bc2 = Bc20_*(1.0-std::pow(t,n_)); // Lubell's equation
		const double b = mgn_field_lim/Bc2;

		// bottura's equation
		double critical_current_density = 
			1e6*Jref_*(C0_/mgn_field_lim)*std::pow(b,alpha_)*
			std::pow(1.0-b,beta_)*std::pow(1.0-std::pow(t,n_),gamma_);

		// do not allow critical currents lower than zero
		if(mgn_field_lim>Bc2)critical_current_density = 0;
		if(mgn_field_lim==0)critical_current_density = arma::datum::inf;

		// return critical current
		return critical_current_density; // A/m^2
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<double> LubellKramer::calc_critical_current_density(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &/*magnetic_field_angle*/) const{
		
		// copy B
		const arma::Col<double> mgn_field_lim = arma::clamp(
			magnetic_field_magnitude, 0.1, arma::datum::inf);
		
		// calculate scaled values
		const arma::Col<double> t = temperature/Tc0_;
		const arma::Col<double> Bc2 = Bc20_*(1.0-arma::pow(t,n_)); // Lubell's equation
		const arma::Col<double> b = mgn_field_lim/Bc2;

		// bottura's equation
		arma::Col<double> critical_current_density = 
			1e6*Jref_*(C0_/mgn_field_lim)%arma::pow(b,alpha_)%
			arma::pow(1.0-b,beta_)%arma::pow(1.0-arma::pow(t,n_),gamma_);

		// do not allow critical currents lower than zero
		critical_current_density(arma::find(mgn_field_lim>Bc2)).fill(0);
		critical_current_density(arma::find(mgn_field_lim==0)).fill(arma::datum::inf);

		// return critical current
		return critical_current_density; // A/m^2
	}

	// function for calculating the power law n-value
	// using scalar input and output
	double LubellKramer::calc_nvalue(
		const double /*temperature*/,
		const double /*magnetic_field_magnitude*/,
		const double /*magnetic_field_angle*/) const{
		return nvalue_;
	}

	// function for calculating the power law n-value
	// using vector input and output
	arma::Col<double> LubellKramer::calc_nvalue(
		const arma::Col<double> &temperature,
		const arma::Col<double> &/*magnetic_field_magnitude*/,
		const arma::Col<double> &/*magnetic_field_angle*/) const{
		return arma::Col<double>(temperature.n_elem,arma::fill::ones)*nvalue_;
	}

	// copy constructor
	ShJcFitPr LubellKramer::copy() const{
		return std::make_shared<LubellKramer>(*this);
	}

	// get type
	std::string LubellKramer::get_type(){
		return "rat::mat::lubellkramer";
	}

	// method for serialization into json
	void LubellKramer::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		js["E0"] = electric_field_criterion_;
		js["N"] = nvalue_;
		js["C0"] = C0_;
		js["alpha"] = alpha_;
		js["beta"] = beta_;
		js["gamma"] = gamma_;
		js["n"] = n_;
		js["Bc20"] = Bc20_;
		js["Tc0"] = Tc0_;
		js["Jref"] = Jref_;
	}

	// method for deserialisation from json
	void LubellKramer::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		electric_field_criterion_ = js["E0"].asDouble();
		nvalue_ = js["N"].asDouble();
		C0_ = js["C0"].asDouble();
		alpha_ = js["alpha"].asDouble();
		beta_ = js["beta"].asDouble();
		gamma_ = js["gamma"].asDouble();
		n_ = js["n"].asDouble();
		Bc20_ = js["Bc20"].asDouble();
		Tc0_ = js["Tc0"].asDouble();
		Jref_ = js["Jref"].asDouble();
	}

}}