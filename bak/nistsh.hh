/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_NIST_SH_HH
#define MAT_NIST_SH_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "specificheatfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class NistSH> ShNistSHPr;

	// NIST specific heat fit
	// source NIST: https://trc.nist.gov/cryogenics/materials/OFHC%20Copper/OFHC_Copper_rev1.htm
	class NistSH: public SpecificHeatFit{
		private: 
			// coefficients of fit
			arma::Col<double>::fixed<9> fit_parameters_; // abcdefghi

			// temperature range
			double lower_temperature_;
			double upper_temperature_;

		public:
			// constructor
			NistSH();
			NistSH(const std::string &fname);
			NistSH(const double a, const double b, const double c, const double d, const double e, const double f, const double g, const double h, const double i, const double lower_temperature, const double upper_temperature);

			// factory
			static ShNistSHPr create();
			static ShNistSHPr create(const std::string &fname);
			static ShNistSHPr create(const double a, const double b, const double c, const double d, const double e, const double f, const double g, const double h, const double i, const double lower_temperature, const double upper_temperature);

			// get temperature range
			double get_lower_temperature() const;
			double get_upper_temperature() const;

			// set fit parameters
			void set_fit_parameters(const arma::Col<double> &fit_parameters);
			void set_temperature_range(const double lower_temperature, const double upper_temperature);

			// fit functions
			double calc_specific_heat(const double temperature) const override;
			arma::Col<double> calc_specific_heat(const arma::Col<double> &temperature) const override;

			// copy function
			ShSpecificHeatFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;

			// read data from json value
			void import_json(const Json::Value &js);
	};

}}

#endif