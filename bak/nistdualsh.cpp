/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "nistdualsh.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	NistDualSH::NistDualSH(){

	}

	// factory
	ShNistDualSHPr NistDualSH::create(){
		return std::make_shared<NistDualSH>();
	}

	// fit functions
	double NistDualSH::calc_specific_heat(const double temperature) const{
		// allocate
		double specific_heat = 0;

		// split calculation
		if(temperature<sh_fit1_->get_upper_temperature())
			specific_heat = sh_fit1_->calc_specific_heat(temperature);
		if(temperature>=sh_fit1_->get_upper_temperature())
			specific_heat = sh_fit2_->calc_specific_heat(temperature);

		// return specific heat
		return specific_heat;
	}
	
	// specific heat calculation
	arma::Col<double> NistDualSH::calc_specific_heat(const arma::Col<double> &temperature) const{
		// check input
		assert(temperature.is_finite());

		// allocate
		arma::Col<double> specific_heat(temperature.n_elem,arma::fill::zeros);

		// split calculation
		const arma::Col<arma::uword> idx1 = arma::find(temperature<sh_fit1_->get_upper_temperature());
		const arma::Col<arma::uword> idx2 = arma::find(temperature>=sh_fit1_->get_upper_temperature());
		if(!idx1.is_empty())specific_heat(idx1) = sh_fit1_->calc_specific_heat(temperature(idx1));
		if(!idx2.is_empty())specific_heat(idx2) = sh_fit2_->calc_specific_heat(temperature(idx2));

		// check output
		assert(specific_heat.is_finite());
		assert(arma::all(specific_heat>0));

		// return specific heat
		return specific_heat;
	}

	// copy constructor
	ShSpecificHeatFitPr NistDualSH::copy() const{
		ShNistDualSHPr dual = NistDualSH::create();
		dual->sh_fit1_ = std::dynamic_pointer_cast<NistSH>(sh_fit1_->copy());
		dual->sh_fit2_ = std::dynamic_pointer_cast<NistSH>(sh_fit2_->copy());
		return dual;
	}

	// get type
	std::string NistDualSH::get_type(){
		return "rat::mat::nistdualsh";
	}

	// method for serialization into json
	void NistDualSH::serialize(Json::Value &js, cmn::SList &list) const{
		js["type"] = get_type();
		js["sh_fit1"] = cmn::Node::serialize_node(sh_fit1_, list);
		js["sh_fit2"] = cmn::Node::serialize_node(sh_fit2_, list);
	}

	// method for deserialisation from json
	void NistDualSH::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		sh_fit1_ = cmn::Node::deserialize_node<NistSH>(js["sh_fit1"], list, factory_list);
		sh_fit2_ = cmn::Node::deserialize_node<NistSH>(js["sh_fit2"], list, factory_list);
	}

}}