/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_CUDI_COPPER_HH
#define MAT_CUDI_COPPER_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "resistivityfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class CudiCopper> ShCudiCopperPr;

	// calculate rho from CUDI
	class CudiCopper: public ResistivityFit{
		// properties
		private:
			// fit coefficients
			double C0_;
			double C1_; 
			double C2_; 
			double C3_;

			// triple-R
			double RRR_;

			// temperature range
			double lower_temperature_;
			double upper_temperature_;

		public:
			// constructor
			CudiCopper();
			CudiCopper(const std::string &fname);
			CudiCopper(const double C0, const double C1, const double C2, const double C3, const double RRR, const double lower_temperature, const double upper_temperature);

			// factory
			static ShCudiCopperPr create();
			static ShCudiCopperPr create(const std::string &fname);
			static ShCudiCopperPr create(const double C0, const double C1, const double C2, const double C3, const double RRR, const double lower_temperature, const double upper_temperature);

			// fit functions
			double calc_electrical_resistivity(const double temperature, const double magnetic_field_magnitude = 0) const override;
			arma::Col<double> calc_electrical_resistivity(const arma::Col<double> &temperature, const arma::Col<double> &magnetic_field_magnitude) const override;

			// copy function
			ShResistivityFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;

			// import json
			void import_json(const Json::Value &js);
	};

}}

#endif