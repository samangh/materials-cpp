/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_JC_FIT_HH
#define MAT_JC_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class JcFit> ShJcFitPr;

	// template for materials
	class JcFit: public cmn::Node{
		// methods
		public:
			// virtual destructor
			~JcFit(){};

			// get function for electric field criterion
			virtual double get_electric_field_criterion() const = 0;

			// function for calculating critical current density
			// using scalar input and output
			// output is in [A m^-2]
			virtual double calc_critical_current_density(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const = 0;

			// function for calculating critical current density
			// using vector input and output
			// output is in [A m^-2]
			virtual arma::Col<double> calc_critical_current_density(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const = 0;

			// function for calculating the power law n-value
			// using scalar input and output
			virtual double calc_nvalue(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const = 0;

			// function for calculating the power law n-value
			// using vector input and output
			virtual arma::Col<double> calc_nvalue(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const = 0;

			// copy function
			virtual ShJcFitPr copy() const = 0;
	};

}}

#endif