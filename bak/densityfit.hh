/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_DENSITY_FIT_HH
#define MAT_DENSITY_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class DensityFit> ShDensityFitPr;

	// template for materials
	class DensityFit: public cmn::Node{
		// methods
		public:
			// virtual destructor
			~DensityFit(){};

			// function for calculating the density
			// using scalar input and output
			// output is in [kg m^-3]
			virtual double calc_density(
				const double temperature) const = 0;

			// function for calculating the density
			// using scalar input and output
			// output is in [kg m^-3]
			virtual arma::Col<double> calc_density(
				const arma::Col<double> &temperature) const = 0;

			// copy function
			virtual ShDensityFitPr copy() const = 0;
	};

}}

#endif