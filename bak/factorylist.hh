/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_FACTORY_LIST_HH
#define MAT_FACTORY_LIST_HH

#include "rat/common/node.hh"
#include "rat/common/serializer.hh"

#include "fixedjc.hh"
#include "rebcocern.hh"
#include "lubellkramer.hh"
#include "densityfit.hh"
#include "baseconductor.hh"
#include "parallelconductor.hh"
#include "cudicopper.hh"
#include "nistconductivity.hh"
#include "nistsh.hh"
#include "nistdualsh.hh"
#include "wfrho2k.hh"
#include "wfk2rho.hh"
#include "fixeddensity.hh"

// code specific to Rat
namespace rat{namespace mat{

	// use a class for storage
	class FactoryList{
		public:
			static void register_constructors(rat::cmn::ShSerializerPr slzr){
				// conductors
				slzr->register_factory<BaseConductor>();
				slzr->register_factory<ParallelConductor>();
				
				// jc fits
				slzr->register_factory<FixedJc>();
				slzr->register_factory<LubellKramer>();
				slzr->register_factory<ReBCOCERN>();

				// electrical resistivity fits
				slzr->register_factory<CudiCopper>();
				slzr->register_factory<WFK2Rho>();

				// thermal conductivity fits
				slzr->register_factory<WFRho2K>();
				slzr->register_factory<NistConductivity>();

				// specific heat fits
				slzr->register_factory<NistSH>();
				slzr->register_factory<NistDualSH>();

				// density fits
				slzr->register_factory<FixedDensity>();
			}
	};

}}

#endif