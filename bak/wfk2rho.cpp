/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "wfk2rho.hh"
#include "conductivityfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	WFK2Rho::WFK2Rho(){

	}

	WFK2Rho::WFK2Rho(ShConductivityFitPr k_fit){
		set_thermal_conductivity_fit(k_fit);
	}

	// factory
	ShWFK2RhoPr WFK2Rho::create(){
		return std::make_shared<WFK2Rho>();
	}

	// factory from file
	ShWFK2RhoPr WFK2Rho::create(ShConductivityFitPr k_fit){
		return std::make_shared<WFK2Rho>(k_fit);
	}

	// set thermal conductivity fit
	void WFK2Rho::set_thermal_conductivity_fit(ShConductivityFitPr k_fit){
		assert(k_fit!=NULL);
		k_fit_ = k_fit;
	}

	// fit functions for vector
	double WFK2Rho::calc_electrical_resistivity(
		const double temperature, 
		const double magnetic_field_magnitude) const{
		
		// limit temperature
		const double T = std::max(temperature,1.0);

		// calculate thermal conductivity
		const double k = k_fit_->calc_thermal_conductivity(
			temperature, magnetic_field_magnitude);

		// Wiedemann Franz Law
		const double rho = lorentz_number_*T/k;

		// rurn resistivity
		return rho;
	}

	// fit functions for vector
	arma::Col<double> WFK2Rho::calc_electrical_resistivity(
		const arma::Col<double> &temperature, 
		const arma::Col<double> &magnetic_field_magnitude) const{

		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);
		assert(temperature.is_finite());
		assert(magnetic_field_magnitude.is_finite());
		
		// limit temperature
		const arma::Col<double> T = arma::clamp(temperature,1.0,arma::datum::inf);

		// calculate thermal conductivity
		const arma::Col<double> k = k_fit_->calc_thermal_conductivity(
			temperature, magnetic_field_magnitude);

		// Wiedemann Franz Law
		const arma::Col<double> rho = lorentz_number_*T/k;

		// check output
		assert(rho.n_elem==temperature.n_elem);
		assert(rho.is_finite());
		assert(arma::all(rho>0));

		// rurn resistivity
		return rho;
	}

	// copy constructor
	ShResistivityFitPr WFK2Rho::copy() const{
		return WFK2Rho::create(k_fit_->copy());
	}

	// serialization type
	std::string WFK2Rho::get_type(){
		return "rat::mat::wfk2rho";
	}

	// serialization
	void WFK2Rho::serialize(Json::Value &js, cmn::SList &list) const{
		js["type"] = get_type();
		js["k_fit"] = cmn::Node::serialize_node(k_fit_, list);
	}

	// deserialization
	void WFK2Rho::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		set_thermal_conductivity_fit(cmn::Node::deserialize_node<ConductivityFit>(js["k_fit"], list, factory_list));
	}

	

}}