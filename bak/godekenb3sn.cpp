/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "godekenb3sn.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	GodekeNb3Sn::GodekeNb3Sn(){

	}

	// factory
	ShGodekeNb3SnPr GodekeNb3Sn::create(){
		return std::make_shared<GodekeNb3Sn>();
	}

	// get function for electric field criterion
	double GodekeNb3Sn::get_electric_field_criterion() const{
		return electric_field_criterion_;
	}

	// set function for critical currnet
	void GodekeNb3Sn::set_nvalue(const double nvalue){
		if(nvalue<0)rat_throw_line("N-value can not be negative");
		nvalue_ = nvalue;
	}

	// set function for critical currnet
	void GodekeNb3Sn::set_electric_field_criterion(const double electric_field_criterion){
		if(electric_field_criterion<0)rat_throw_line("electric field criterion can not be negative");
		electric_field_criterion_ = electric_field_criterion;
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	double GodekeNb3Sn::calc_critical_current_density(
		const double temperature,
		const double magnetic_field_magnitude,
		const double /*magnetic_field_angle*/) const{

		// strain dependence
		const double eps_shift = Ca2_*eps_0a_/std::sqrt(Ca1_*Ca1_ - Ca2_*Ca2_);
		const double S = (Ca1_*(std::sqrt(eps_shift*eps_shift + eps_0a_*eps_0a_) - 
			std::sqrt(std::pow(eps_ax_ - eps_shift,2) + eps_0a_*eps_0a_)) - 
			Ca2_*eps_ax_)/(1.0 - Ca1_*eps_0a_) + 1.0;

		// scaled values
		const double Tc = Tcm_*std::pow(S,(1.0/3));
		const double t = temperature/Tc;
		const double Bc2 = Bc2m_*S*(1.0-std::pow(t,1.52));
		const double h = magnetic_field_magnitude/Bc2;

		// Godeke Equation
		double critical_current = 
			C1_*S*(1.0-std::pow(t,1.52))*
			(1.0-std::pow(t,2))*std::pow(h,p_)*
			std::pow(1.0-h,q_)/magnetic_field_magnitude;

		// prevent formula from rising again after reaching Bc
		if(magnetic_field_magnitude>Bc2 || t>1.0)critical_current = 0;
		if(magnetic_field_magnitude==0)critical_current = arma::datum::inf;

		// return critical current
		return 1e6*critical_current;
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<double> GodekeNb3Sn::calc_critical_current_density(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &/*magnetic_field_angle*/) const{

		// strain dependence
		const double eps_shift = Ca2_*eps_0a_/std::sqrt(Ca1_*Ca1_ - Ca2_*Ca2_);
		const double S = (Ca1_*(std::sqrt(eps_shift*eps_shift + eps_0a_*eps_0a_) - 
			std::sqrt(std::pow(eps_ax_ - eps_shift,2) + eps_0a_*eps_0a_)) - 
			Ca2_*eps_ax_)/(1.0 - Ca1_*eps_0a_) + 1.0;

		// scaled values
		const double Tc = Tcm_*std::pow(S,(1.0/3));
		const arma::Col<double> t = temperature/Tc;
		const arma::Col<double> Bc2 = Bc2m_*S*(1.0 - arma::pow(t,1.52));
		const arma::Col<double> h = magnetic_field_magnitude/Bc2;

		// Godeke Equation
		arma::Col<double> critical_current = 
			C1_*S*(1.0-arma::pow(t,1.52))%
			(1.0-arma::pow(t,2))%arma::pow(h,p_)%
			arma::pow(1.0-h,q_)/magnetic_field_magnitude;

		// prevent formula from rising again after reaching Bc
		critical_current(arma::find(magnetic_field_magnitude>Bc2)).fill(0);
		critical_current(arma::find(magnetic_field_magnitude==0)).fill(arma::datum::inf);

		// return critical current
		return 1e6*critical_current;
	}

	// function for calculating the power law n-value
	// using scalar input and output
	double GodekeNb3Sn::calc_nvalue(
		const double /*temperature*/,
		const double /*magnetic_field_magnitude*/,
		const double /*magnetic_field_angle*/) const{
		return nvalue_;
	}

	// function for calculating the power law n-value
	// using vector input and output
	arma::Col<double> GodekeNb3Sn::calc_nvalue(
		const arma::Col<double> &temperature,
		const arma::Col<double> &/*magnetic_field_magnitude*/,
		const arma::Col<double> &/*magnetic_field_angle*/) const{
		return arma::Col<double>(temperature.n_elem,arma::fill::ones)*nvalue_;
	}

	// copy constructor
	ShJcFitPr GodekeNb3Sn::copy() const{
		return std::make_shared<GodekeNb3Sn>(*this);
	}

	// get type
	std::string GodekeNb3Sn::get_type(){
		return "rat::mat::godekenb3sn";
	}

	// method for serialization into json
	void GodekeNb3Sn::serialize(Json::Value &js, cmn::SList &) const{
		// serialization type
		js["type"] = get_type();
		
		// fit parameters
		js["Ca1"] = Ca1_;
		js["Ca2"] = Ca2_;
		js["eps_0a"] = eps_0a_;
		js["C1"] = C1_;

		// superconducting parameters
		js["Bc2m"] = Bc2m_;
		js["Tcm"] = Tcm_;

		// magnetic field dependence parameters
		js["p"] = p_;
		js["q"] = q_;

		// environmental parameters
		js["eps_ax"] = eps_ax_; // axial strain

		// power law parameters
		js["E0"] = electric_field_criterion_;
		js["N"] = nvalue_;
	}

	// method for deserialisation from json
	void GodekeNb3Sn::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		// fit parameters
		Ca1_ = js["Ca1"].asDouble();
		Ca2_ = js["Ca2"].asDouble();
		eps_0a_ = js["eps_0a"].asDouble(); // normalized
		C1_ = js["C1"].asDouble(); // [kAT mm^-2]

		// superconducting parameters
		Bc2m_ = js["Bc2m"].asDouble(); // [T]
		Tcm_ = js["Tcm"].asDouble(); // [K]

		// magnetic field dependence parameters
		p_ = js["p"].asDouble();
		q_ = js["q"].asDouble();

		// environmental parameters
		eps_ax_ = js["eps_ax"].asDouble();

		// power law parameters
		electric_field_criterion_ = js["E0"].asDouble();
		nvalue_ = js["N"].asDouble();
	}

}}