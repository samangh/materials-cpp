/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "nistsh.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	NistSH::NistSH(){

	}

	// constructor from file
	NistSH::NistSH(const std::string &fname){
		// allocate json
		Json::Value js = cmn::Node::parse_json(fname);

		// import data
		import_json(js);
	}

	// constructor from file
	NistSH::NistSH(
		const double a, const double b, const double c,
		const double d, const double e, const double f,
		const double g, const double h, const double i, 
		const double lower_temperature, 
		const double upper_temperature){
		fit_parameters_ = arma::Col<double>::fixed<9>{a,b,c,d,e,f,g,h,i};
		lower_temperature_ = lower_temperature;
		upper_temperature_ = upper_temperature;
	}

	// factory
	ShNistSHPr NistSH::create(){
		return std::make_shared<NistSH>();
	}

	// factory from file
	ShNistSHPr NistSH::create(const std::string &fname){
		return std::make_shared<NistSH>(fname);
	}

	// factory from file
	ShNistSHPr NistSH::create(
		const double a, const double b, const double c,
		const double d, const double e, const double f,
		const double g, const double h, const double i, 
		const double lower_temperature, const double upper_temperature){
		return std::make_shared<NistSH>(
			a,b,c,d,e,f,g,h,i, lower_temperature, upper_temperature);
	}

	// set fit parameters
	void NistSH::set_fit_parameters(const arma::Col<double> &fit_parameters){
		if(fit_parameters.is_empty())rat_throw_line("parameter list is empty");
		fit_parameters_ = fit_parameters;
	}

	// set fit parameters
	void NistSH::set_temperature_range(const double lower_temperature, const double upper_temperature){
		if(upper_temperature<lower_temperature)rat_throw_line("upper temperature must be larger than lower temperature");
		lower_temperature_ = lower_temperature;
		upper_temperature_ = upper_temperature;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Col<double> NistSH::calc_specific_heat(const arma::Col<double> &temperature)const{

		// check input
		assert(temperature.is_finite());

		// check if fit parameters set
		if(fit_parameters_.is_empty())rat_throw_line("parameter list is not set");

		// calculate nist fit using for loop to avoid power
		arma::Col<double> A(temperature.n_elem,arma::fill::zeros);
		arma::Col<double> log10T = arma::log10(temperature);
		arma::Col<double> plt(temperature); plt.fill(1.0);
		for(arma::uword i=0;i<fit_parameters_.n_elem;i++){
			A += fit_parameters_(i)*plt; plt%=log10T;
		}

		// 10^A and multiply with density
		arma::Col<double> specific_heat = arma::exp10(A);

		// extrapolation beyond end
		const arma::Col<arma::uword> extrap = arma::find(temperature>upper_temperature_);
		if(!extrap.is_empty()){
			const double dt = (upper_temperature_-lower_temperature_)/1000;
			const arma::Col<double>::fixed<2> Te = {upper_temperature_-dt, upper_temperature_-1e-10};
			const arma::Col<double>::fixed<2> Cpe = calc_specific_heat(Te);
			specific_heat(extrap) = Cpe(1) + (temperature(extrap)-Te(1))*((Cpe(1)-Cpe(0))/(Te(1)-Te(0)));
		}

		// fix value when below range
		const arma::Col<arma::uword> idx = arma::find(temperature<lower_temperature_);
		if(!idx.is_empty()){
			// create temperature array
			arma::Col<double> Text(idx.n_elem); Text.fill(lower_temperature_+1e-10);
			specific_heat(idx) = calc_specific_heat(Text);
		}

		// check output
		assert(specific_heat.n_elem==temperature.n_elem);
		assert(specific_heat.is_finite());
		
		// check with scalar version
		#ifndef NDEBUG
		for(arma::uword i=0;i<temperature.n_elem;i++)
			assert(std::abs(specific_heat(i) - calc_specific_heat(temperature(i)))/specific_heat(i)<1e-6);
		#endif

		// return answer
		return specific_heat;
	}

	// specific heat output in [J m^-3 K^-1]
	double NistSH::calc_specific_heat(const double temperature)const{
		// check input
		if(fit_parameters_.is_empty())rat_throw_line("parameter list is not set");

		// allocate
		double specific_heat = 0;

		// extrapolation beyond end
		if(temperature>upper_temperature_){
			const double dt = (upper_temperature_-lower_temperature_)/1000;
			const double t1 = upper_temperature_-dt;
			const double t2 = upper_temperature_;
			const double s1 = calc_specific_heat(t1);
			const double s2 = calc_specific_heat(t2);
			specific_heat = s2 + (temperature - upper_temperature_)*((s2-s1)/(t2-t1));
		}

		// fix value when below range
		else if(temperature<lower_temperature_){
			specific_heat = calc_specific_heat(lower_temperature_);
		}

		// calculate normally
		else{
			double A = 0, plt = 1.0;
			double log10T = std::log10(temperature);
			for(arma::uword i=0;i<fit_parameters_.n_elem;i++){
				A += fit_parameters_(i)*plt; plt*=log10T;
			}

			// 10^A and multiply with density
			specific_heat = std::pow(10.0,A);
		}

		// return answer
		return specific_heat;
	}

	// get lower temperature
	double NistSH::get_lower_temperature() const{
		return lower_temperature_;
	}

	// get upper temperature
	double NistSH::get_upper_temperature() const{
		return upper_temperature_;
	}

	// copy constructor
	ShSpecificHeatFitPr NistSH::copy() const{
		return std::make_shared<NistSH>(*this);
	}

	// get type
	std::string NistSH::get_type(){
		return "rat::mat::nistsh";
	}

	// method for serialization into json
	void NistSH::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();

		// fit parameters
		js["a"] = fit_parameters_(0);
		js["b"] = fit_parameters_(1);
		js["c"] = fit_parameters_(2);
		js["d"] = fit_parameters_(3);
		js["e"] = fit_parameters_(4);
		js["f"] = fit_parameters_(5);
		js["g"] = fit_parameters_(6);
		js["h"] = fit_parameters_(7);
		js["i"] = fit_parameters_(8);

		// temperature range
		js["lower_temperature"] = lower_temperature_;
		js["upper_temperature"] = upper_temperature_;
	}

	// method for deserialisation from json
	void NistSH::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/){
		import_json(js);
	}

	// create reader
	void NistSH::import_json(const Json::Value &js){
		// fit parameters
		fit_parameters_(0) = js["a"].asDouble();
		fit_parameters_(1) = js["b"].asDouble();
		fit_parameters_(2) = js["c"].asDouble();
		fit_parameters_(3) = js["d"].asDouble();
		fit_parameters_(4) = js["e"].asDouble();
		fit_parameters_(5) = js["f"].asDouble();
		fit_parameters_(6) = js["g"].asDouble();
		fit_parameters_(7) = js["h"].asDouble();
		fit_parameters_(8) = js["i"].asDouble();

		// temperature range
		lower_temperature_ = js["lower_temperature"].asDouble();
		upper_temperature_ = js["upper_temperature"].asDouble();
	}

}}