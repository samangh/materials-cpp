/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "fixedjc.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	FixedJc::FixedJc(){

	}

	// constructor with critical current input
	FixedJc::FixedJc(const double critical_current_density, const double nvalue){
		set_critical_current_density(critical_current_density);
		set_nvalue(nvalue);
	}

	// factory
	ShFixedJcPr FixedJc::create(){
		return std::make_shared<FixedJc>();
	}

	// factory from file
	ShFixedJcPr FixedJc::create(const double critical_current_density, const double nvalue){
		return std::make_shared<FixedJc>(critical_current_density, nvalue);
	}

	// get function for electric field criterion
	double FixedJc::get_electric_field_criterion() const{
		return electric_field_criterion_;
	}

	// set function for critical currnet
	void FixedJc::set_critical_current_density(const double critical_current_density){
		if(critical_current_density<0)rat_throw_line("critical current can not be negative");
		critical_current_density_ = critical_current_density;
	}

	// set function for critical currnet
	void FixedJc::set_nvalue(const double nvalue){
		if(nvalue<0)rat_throw_line("N-value can not be negative");
		nvalue_ = nvalue;
	}

	// set function for critical currnet
	void FixedJc::set_electric_field_criterion(const double electric_field_criterion){
		if(electric_field_criterion<0)rat_throw_line("electric field criterion can not be negative");
		electric_field_criterion_ = electric_field_criterion;
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	double FixedJc::calc_critical_current_density(
		const double /*temperature*/,
		const double /*magnetic_field_magnitude*/,
		const double /*magnetic_field_angle*/) const{
		return critical_current_density_;
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<double> FixedJc::calc_critical_current_density(
		const arma::Col<double> &temperature,
		const arma::Col<double> &/*magnetic_field_magnitude*/,
		const arma::Col<double> &/*magnetic_field_angle*/) const{
		return arma::Col<double>(temperature.n_elem,arma::fill::ones)*critical_current_density_;
	}

	// function for calculating the power law n-value
	// using scalar input and output
	double FixedJc::calc_nvalue(
		const double /*temperature*/,
		const double /*magnetic_field_magnitude*/,
		const double /*magnetic_field_angle*/) const{
		return nvalue_;
	}

	// function for calculating the power law n-value
	// using vector input and output
	arma::Col<double> FixedJc::calc_nvalue(
		const arma::Col<double> &temperature,
		const arma::Col<double> &/*magnetic_field_magnitude*/,
		const arma::Col<double> &/*magnetic_field_angle*/) const{
		return arma::Col<double>(temperature.n_elem,arma::fill::ones)*nvalue_;
	}

	// copy constructor
	ShJcFitPr FixedJc::copy() const{
		return std::make_shared<FixedJc>(*this);
	}

	// serialization
	// get type
	std::string FixedJc::get_type(){
		return "rat::mat::fixedjc";
	}

	// method for serialization into json
	void FixedJc::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();
		js["critical_current_density"] = critical_current_density_;
		js["electric_field_criterion"] = electric_field_criterion_;
		js["nvalue"] = nvalue_;
	}

	// method for deserialisation from json
	void FixedJc::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/){
		set_critical_current_density(js["critical_current_density"].asDouble());
		set_electric_field_criterion(js["electric_field_criterion"].asDouble());
		set_nvalue(js["nvalue"].asDouble());
	}

}}