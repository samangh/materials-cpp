/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_REBCO_CERN_HH
#define MAT_REBCO_CERN_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "jcfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class ReBCOCERN> ShReBCOCERNPr;

	// fixed critical current density
	class ReBCOCERN: public JcFit{
		private:
			// typical layer thickness
			double tsc_;

			// general parameters
			double Tc0_; // [K]
			double n_;
			double n1_;
			double n2_;

			// Parameters for ab-plane
			double pab_;
			double qab_;
			double Bi0ab_; // [T]
			double a_;
			double gammaab_;
			double alphaab_; // [AT/m^2]

			// Parameters for c-plane
			double pc_;
			double qc_;
			double Bi0c_; // [T]
			double gammac_;
			double alphac_; // [AT/m^2]

			// Parameters for anisotropy
			double g0_;
			double g1_;
			double g2_;
			double g3_;
			double nu_;
			double pkoff_;

			// fixed power law N-value
			double nvalue_;

			// power law electric field criterion
			double electric_field_criterion_;

			// type-0 pair flipping
			bool type0_flip_;
			bool ignore_pkoff_;

		public:
			// constructor
			ReBCOCERN();
			ReBCOCERN(const std::string &fname);

			// factory
			static ShReBCOCERNPr create();
			static ShReBCOCERNPr create(const std::string &fname);

			// get function for electric field criterion
			double get_electric_field_criterion() const override;

			// function for calculating critical current density
			// using scalar input and output
			// output is in [A m^-2]
			double calc_critical_current_density(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// function for calculating critical current density
			// using vector input and output
			// output is in [A m^-2]
			arma::Col<double> calc_critical_current_density(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;
			
			// function for calculating the power law n-value
			// using scalar input and output
			double calc_nvalue(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// function for calculating the power law n-value
			// using vector input and output
			arma::Col<double> calc_nvalue(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// copy function
			ShJcFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;

			// read data from json value
			void import_json(const Json::Value &js);
	};

}}

#endif