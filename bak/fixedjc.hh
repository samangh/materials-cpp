/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_FIX_JC_FIT_HH
#define MAT_FIX_JC_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "jcfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class FixedJc> ShFixedJcPr;

	// fixed critical current density
	class FixedJc: public JcFit{
		// critical current
		private:
			// fixed critical current density
			double critical_current_density_ = 0;
			
			// fixed power law N-value
			double nvalue_ = 0;

			// power law electric field criterion
			double electric_field_criterion_ = 100e-6;

		// methods
		public:
			// virtual destructor
			FixedJc();
			FixedJc(const double critical_current_density, const double nvalue = 0);

			// factory
			static ShFixedJcPr create();
			static ShFixedJcPr create(const double critical_current_density, const double nvalue = 0);

			// get function for electric field criterion
			double get_electric_field_criterion() const override;

			// set function for critical currnet
			void set_critical_current_density(const double critical_current_density);
			void set_nvalue(const double nvalue);
			void set_electric_field_criterion(const double electric_field_criterion);

			// function for calculating critical current density
			// using scalar input and output
			// output is in [A m^-2]
			double calc_critical_current_density(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// function for calculating critical current density
			// using vector input and output
			// output is in [A m^-2]
			arma::Col<double> calc_critical_current_density(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;
			
			// function for calculating the power law n-value
			// using scalar input and output
			double calc_nvalue(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// function for calculating the power law n-value
			// using vector input and output
			arma::Col<double> calc_nvalue(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// copy function
			ShJcFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;
	};

}}

#endif