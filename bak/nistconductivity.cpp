/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "nistconductivity.hh"

// code specific to Rat
namespace rat{namespace mat{


	// default constructor
	NistConductivity::NistConductivity(){

	}

	// constructor from file
	NistConductivity::NistConductivity(const std::string &fname){
		// allocate json
		Json::Value js = cmn::Node::parse_json(fname);

		// import data
		import_json(js);
	}

	// factory
	ShNistConductivityPr NistConductivity::create(){
		return std::make_shared<NistConductivity>();
	}

	// factory from file
	ShNistConductivityPr NistConductivity::create(const std::string &fname){
		return std::make_shared<NistConductivity>(fname);
	}

	// set fit parameters
	void NistConductivity::set_fit_parameters(const arma::Col<double> &fit_parameters){
		if(fit_parameters.is_empty())rat_throw_line("parameter list is empty");
		fit_parameters_ = fit_parameters;
	}

	// set fit parameters
	void NistConductivity::set_temperature_range(const double lower_temperature, const double upper_temperature){
		if(upper_temperature<lower_temperature)rat_throw_line("upper temperature must be larger than lower temperature");
		lower_temperature_ = lower_temperature;
		upper_temperature_ = upper_temperature;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Col<double> NistConductivity::calc_thermal_conductivity(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude)const{

		// calculate fit
		arma::Col<double> k1(temperature.n_elem,arma::fill::zeros); 
		arma::Col<double> k2(temperature.n_elem,arma::fill::ones);
		for(arma::uword i=0;i<5;i++){
			const arma::Col<double> tp = arma::pow(temperature,i*0.5);
			const double f1 = fit_parameters_(2*i);
			k1 += f1*tp; 
			if(i!=0){
				const double f2 = fit_parameters_(2*i-1);
				k2 += f2*tp;
			}
		}

		// take exp10
		arma::Col<double> k = arma::exp10(k1/k2);

		// find indexes below range
		const arma::Col<arma::uword> idx = arma::find(temperature<lower_temperature_);
		if(!idx.is_empty()){
			arma::Col<double> Text(idx.n_elem); Text.fill(lower_temperature_);
			k.rows(idx) = calc_thermal_conductivity(Text, magnetic_field_magnitude.rows(idx));
		}

		// check output
		assert(k.is_finite()); assert(arma::all(k>0));

		// return thermal conductivity
		return k;
	}

	// specific heat output in [J m^-3 K^-1]
	double NistConductivity::calc_thermal_conductivity(
		const double temperature,
		const double magnetic_field_magnitude)const{
		
		// catch below range
		if(temperature<lower_temperature_)
			return calc_thermal_conductivity(lower_temperature_, magnetic_field_magnitude);

		// calculate fit
		double k1 = 0, k2 = 1.0;
		for(arma::uword i=0;i<5;i++){
			const double tp = std::pow(temperature,i*0.5);
			const double f1 = fit_parameters_(2*i);
			k1 += f1*tp; 
			if(i!=0){
				const double f2 = fit_parameters_(2*i-1);
				if(i!=0)k2 += f2*tp;
			}
		}

		// take exp10
		const double k = std::pow(10.0,k1/k2);

		// return thermal conductivity
		return k;
	}

	// copy constructor
	ShConductivityFitPr NistConductivity::copy() const{
		return std::make_shared<NistConductivity>(*this);
	}

	// get type
	std::string NistConductivity::get_type(){
		return "rat::mat::nistconductivity";
	}

	// method for serialization into json
	void NistConductivity::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();

		// fit parameters
		js["a"] = fit_parameters_(0);
		js["b"] = fit_parameters_(1);
		js["c"] = fit_parameters_(2);
		js["d"] = fit_parameters_(3);
		js["e"] = fit_parameters_(4);
		js["f"] = fit_parameters_(5);
		js["g"] = fit_parameters_(6);
		js["h"] = fit_parameters_(7);
		js["i"] = fit_parameters_(8);

		// temperature range
		js["lower_temperature"] = lower_temperature_;
		js["upper_temperature"] = upper_temperature_;
	}

	// method for deserialisation from json
	void NistConductivity::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/){
		import_json(js);
	}

	// create reader
	void NistConductivity::import_json(const Json::Value &js){
		// fit parameters
		fit_parameters_(0) = js["a"].asDouble();
		fit_parameters_(1) = js["b"].asDouble();
		fit_parameters_(2) = js["c"].asDouble();
		fit_parameters_(3) = js["d"].asDouble();
		fit_parameters_(4) = js["e"].asDouble();
		fit_parameters_(5) = js["f"].asDouble();
		fit_parameters_(6) = js["g"].asDouble();
		fit_parameters_(7) = js["h"].asDouble();
		fit_parameters_(8) = js["i"].asDouble();

		// temperature range
		lower_temperature_ = js["lower_temperature"].asDouble();
		upper_temperature_ = js["upper_temperature"].asDouble();
	}

}}