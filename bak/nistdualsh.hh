/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_NIST_DUAL_SH_HH
#define MAT_NIST_DUAL_SH_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "specificheatfit.hh"
#include "nistsh.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class NistDualSH> ShNistDualSHPr;

	// specific heat fits consisting of different ranges
	class NistDualSH: public SpecificHeatFit{
		// create fit
		private:
			// first fit
			ShNistSHPr sh_fit1_;
			ShNistSHPr sh_fit2_;

		// methods
		public:
			// constructor
			NistDualSH();

			// factory
			static ShNistDualSHPr create();

			// fit functions
			double calc_specific_heat(const double temperature) const override;
			arma::Col<double> calc_specific_heat(const arma::Col<double> &temperature) const override;

			// copy function
			ShSpecificHeatFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;
	};

}}

#endif