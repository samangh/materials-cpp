/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_WF_K2RHO_HH
#define MAT_WF_K2RHO_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "resistivityfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class WFK2Rho> ShWFK2RhoPr;

	// fit rho with wiedemann franz law
	class WFK2Rho: public ResistivityFit{
		private: 
			// lorentz number
			const double lorentz_number_ = 2.44e-8; // [W Ohm K-2]

			// thermal conductivity fit
			std::shared_ptr<class ConductivityFit> k_fit_;

		public:
			// constructor
			WFK2Rho();
			WFK2Rho(std::shared_ptr<ConductivityFit> kfit);

			// factory
			static ShWFK2RhoPr create();
			static ShWFK2RhoPr create(std::shared_ptr<ConductivityFit> k_fit);

			// setting
			void set_thermal_conductivity_fit(std::shared_ptr<ConductivityFit> k_fit);

			// fit functions
			double calc_electrical_resistivity(const double temperature, const double magnetic_field_magnitude) const override;
			arma::Col<double> calc_electrical_resistivity(const arma::Col<double> &temperature, const arma::Col<double> &magnetic_field_magnitude) const override;

			// copy function
			ShResistivityFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;
	};

}}

#endif