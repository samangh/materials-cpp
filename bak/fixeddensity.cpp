/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "fixeddensity.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	FixedDensity::FixedDensity(){

	}

	// constructor from file
	FixedDensity::FixedDensity(const std::string &fname){
		// allocate json
		Json::Value js = cmn::Node::parse_json(fname);

		// import data
		import_json(js);
	}

	// constructor from file
	FixedDensity::FixedDensity(const double density){
		set_density(density);
	}

	// factory
	ShFixedDensityPr FixedDensity::create(){
		return std::make_shared<FixedDensity>();
	}

	// factory from file
	ShFixedDensityPr FixedDensity::create(const std::string &fname){
		return std::make_shared<FixedDensity>(fname);
	}

	// factory from file
	ShFixedDensityPr FixedDensity::create(const double density){
		return std::make_shared<FixedDensity>(density);
	}


	// set fit parameters
	void FixedDensity::set_density(const double density){
		density_ = density;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Col<double> FixedDensity::calc_density(const arma::Col<double> &temperature)const{
		return arma::Col<double>(temperature.n_elem,arma::fill::ones)*density_;
	}

	// specific heat output in [J m^-3 K^-1]
	double FixedDensity::calc_density(const double /*temperature*/)const{
		return density_;
	}

	// copy constructor
	ShDensityFitPr FixedDensity::copy() const{
		return std::make_shared<FixedDensity>(*this);
	}

	// get type
	std::string FixedDensity::get_type(){
		return "rat::mat::fixeddensity";
	}

	// method for serialization into json
	void FixedDensity::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();

		// fit parameters
		js["density"] = density_;
	}

	// method for deserialisation from json
	void FixedDensity::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/){
		import_json(js);
	}

	// create reader
	void FixedDensity::import_json(const Json::Value &js){
		density_ = js["density"].asDouble();
	}

}}