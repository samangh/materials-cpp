/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MAT_WF_RHO2K_HH
#define MAT_WF_RHO2K_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "resistivityfit.hh"
#include "conductivityfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class WFRho2K> ShWFRho2KPr;

	// fit k with wiedemann franz law
	class WFRho2K: public ConductivityFit{
		private: 
			// lorentz number
			const double lorentz_number_ = 2.44e-8; // [W Ohm K-2]

			// electrical resistivity fit
			std::shared_ptr<class ResistivityFit> rho_fit_;

		public:
			// constructor
			WFRho2K();
			WFRho2K(std::shared_ptr<ResistivityFit> rho_fit);

			// factory
			static ShWFRho2KPr create();
			static ShWFRho2KPr create(std::shared_ptr<ResistivityFit> rho_fit);

			// setting
			void set_electrical_resistivity_fit(std::shared_ptr<ResistivityFit> rho_fit);

			// fit functions
			double calc_thermal_conductivity(const double temperature, const double magnetic_field_magnitude) const override;
			arma::Col<double> calc_thermal_conductivity(const arma::Col<double> &temperature, const arma::Col<double> &magnetic_field_magnitude) const override;

			// copy function
			ShConductivityFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;
	};

}}

#endif