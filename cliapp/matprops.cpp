/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// command line parser
#include <tclap/CmdLine.h>
#include <filesystem>

// rat common headers
#include "rat/common/log.hh"

// rat material headers
#include "serializer.hh"
#include "baseconductor.hh"
#include "parallelconductor.hh"

// string parse function
arma::Col<double> parse_string(const std::string &str, const std::string delimiter){
	// keep track of position
	size_t pos = 0;
	
	// split string into tokens
	std::string s = str;
	std::list<std::string> token_list;
	while ((pos = s.find(delimiter)) != std::string::npos) {
		token_list.push_back(s.substr(0, pos));
		s.erase(0, pos + delimiter.length());
	}
	token_list.push_back(s);

	// convert tokens to values
	arma::Col<double> values(token_list.size()); arma::uword i=0;
	for(auto it = token_list.begin();it!=token_list.end();it++,i++){
		values(i) = std::stof((*it));
	}

	// return values
	return values;
}

// main function
int main(int argc, char * argv[]){
	// create tclap object
	TCLAP::CmdLine cmd("Rat Materials Library",' ',"1.0");

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> input_filename_argument(
		"ifname","Input filename",true,
		"json/matprops/metals/CopperOFHC_RRR050.json",
		"string", cmd);

	// temperature input argument
	TCLAP::ValueArg<std::string> temperature_argument(
		"t","temperature","Input temperatures in [K]",
		false,"4.5,10,20,40,60,77", "string", cmd);

	// magnetic field input argument
	TCLAP::ValueArg<std::string> field_argument(
		"b","fluxdensity","Input magnetic flux densities in [T]",
		false,"5,10,20,30", "string", cmd);

	// magnetic field angle input argument
	TCLAP::ValueArg<std::string> field_angle_argument(
		"a","fieldangle","Magnetic field angle in [deg]",
		false,"0,90", "string", cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);

	// get temperature
	arma::Col<double> temperature;
	if(temperature_argument.isSet()){
		temperature = parse_string(temperature_argument.getValue(), ",");
	}else{
		temperature = arma::Col<double>{1.9,4.5,10,20,30,40,50,60,77};
	}
	
	// get magnetic field
	arma::Col<double> magnetic_field_magnitude;
	if(field_argument.isSet()){
		magnetic_field_magnitude = parse_string(field_argument.getValue(), ",");
	}else{
		magnetic_field_magnitude = arma::Col<double>{5};
	}

	// get magnetic field angle
	arma::Col<double> magnetic_field_angle;
	if(field_angle_argument.isSet()){
		magnetic_field_angle = 2*arma::datum::pi*parse_string(field_angle_argument.getValue(), ",")/360;
	}else{
		magnetic_field_angle = arma::Col<double>{0};
	}

	// get file name
	boost::filesystem::path ifname = input_filename_argument.getValue();
	

	// create a log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// deserializer file
	rat::mat::ShConductorPr mat = rat::mat::Serializer::create<rat::mat::Conductor>(ifname);

	// display table
	mat->display_property_table(lg, temperature, magnetic_field_magnitude, magnetic_field_angle);
}